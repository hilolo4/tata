import { CalculTva } from "app/Models/Model/calcul-tva";
import { ResultatCalculModel } from "app/Models/Model/ResultatCalculModel";
import { TypeValue } from "app/Enums/typeValue.Enums";

export interface ICalcule {

    totalHTArticle(prix: number, quantite: number, remise: number): number

    totalTTCArticle(articleTotalHT: number, tva: number): number;

    totalHt(articles: any[]): number;

    totalHtRemise(globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): number

    calculVentilationRemise(articles: any[], globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): CalculTva[]
    calculVentilationRemiseDepense(articles: any[], globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue , id: string): CalculTva[]
    totalTTC(globalTotalHT: number, globalTotalHTRemise: number, calculTvas: CalculTva[], remiseGloabl: number): number

    margeBrut(articles: any[], globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): number

    calculGenerale(articles: any[], remiseGloabl: number, typeRemiseGloabl: TypeValue): ResultatCalculModel
}