import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Client } from 'app/Models/Entities/Contacts/Client';


declare var jQuery: any;
declare var toastr: any;

@Component({
	selector: 'app-export-factures',
	templateUrl: './export-factures.html',
	styleUrls: ['./export-factures.scss']
})
export class exportFacturesComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('startExport') startExport = new EventEmitter();
	// tslint:disable-next-line:no-input-rename
	@Input('clients') clients: Client[] = [];

	dateLang;
	dataTablesLang;
	typeExport: number;
	DateDebut: Date;
	DateFin: Date;
	IdChantier;
	IdClient;
	ExporterFacturesChantier = true;

	constructor(
		private translate: TranslateService) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getDataTablesTrans();
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	getDataTablesTrans() {
		this.translate.get('dataTables').subscribe((text: string) => {
			this.dataTablesLang = text;
		});
	}

	export() {

		if (+this.typeExport === 1) {
			const params = {
				IdClient: this.IdClient === undefined ? '' : this.IdClient,
				DateDebut: this.DateDebut,
				DateFin: this.DateFin,
				ExporterFacturesChantier: this.ExporterFacturesChantier
			};
			this.startExport.emit({
				type: this.typeExport,
				params
			});
		}

		if (+this.typeExport === 2) {

			const params = {
				IdClient: this.IdClient === undefined ? '' : this.IdClient,
				DateDebut: this.DateDebut,
				DateFin: this.DateFin,
				ExporterFacturesChantier: this.ExporterFacturesChantier
			};
			this.startExport.emit({
				type: this.typeExport,
				params
			});
		}

		jQuery('#exportFactures').modal('hide');
		this.typeExport = null;
		this.DateDebut = null;
		this.DateFin = null;
		this.IdClient = null;
	}

	validate() {
		if (this.typeExport === 0 || this.typeExport === undefined) {
			return true;
		}
		const DateDebut = new Date(this.DateDebut);
		const DateFin = new Date(this.DateFin);

		if (this.typeExport === 0 && (DateDebut > DateFin)) {
			return true;
		}
		if (this.typeExport === 1 && ((this.DateDebut === undefined || this.DateFin === undefined))) {
			return true;
		}
		return false;
	}
}
