import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter,
	ViewChild,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PagedResult } from 'app/Models/Model/ListModel';
import { IFilterOption, SortDirection } from 'app/Models/Model/filter-option';
import { AppSettings } from 'app/app-settings/app-settings';
import { FormControl } from '@angular/forms';
import { StringHelper } from 'app/libraries/string';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DataTableRowActions, DataTableEntry, ManageDataTable } from 'app/libraries/manage-data-table';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { MatDialog, MatInput } from '@angular/material';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { AgendaTypeFilter } from 'app/Enums/AgendaTypeFIlter';
import { MissionKind } from 'app/Enums/missionKinds';
import { DialogHelper } from 'app/libraries/dialog';
import { PaiementGroupeComponent } from 'app/pages/paiements/index/paiement-groupe/paiement-groupe.component';
import { GlobalInstances } from '../../../app.module';

declare var toastr: any;

export enum ColumnType {
	Date,
	DateTime,
	Currency,
	Translate,
	Number,
	Tags,
	TagType,
	List,
	colorNews,
	Status,
	Html,
	any,
	Progressbar,
	span,
	image,
	tache,
}

@Component({
	selector: 'data-table',
	templateUrl: './data-table.component.html',
	styleUrls: [
		'./data-table.component.scss'
	]
})

export class DataTableComponent implements OnInit {
	// #region Propertie
	allowPeriodeFilter = false;

	selected: any;
	processing = true;
	/**
 * Whether or not to allow searching
 */
	@Input() allowSearch: boolean = true;

	/**
	 * Whether or not to allow tagging
	 */
	@Input() allowTags: boolean = false;

	/**
	 * Whether or not to allow filtering
	 */
	@Input() allowFilter: boolean = true;

	@Input() filterDate: boolean = false;

	@Input() isAdd: boolean = true;

	@Output() changeEvent = new EventEmitter<IFilterOption>();

	@Input() title: string;
	@Input() idChantier;
	@Input() actions: DataTableRowActions[];
	@Input() columns: DataTableEntry[];
	@Input() profils = [];
	@Input() status = [];
	@Input() listMois = [];
	@Input() listChantiers = [];
	@Input() listClient = [];
	@Input() listSupplier = [];
	@Input() periodes = [];
	@Input() listTechnicien = [];
	@Input() comptes = [];
	@Input() name: string;
	@Input() isAction = true;
	@Input() isVisitM = false;
	@Input() Isfilter = false;
	@Input() isMission = false;

	@Input() placeholderSearch = 'LIST.SEARCH';
	@Input() set data(val: PagedResult) {
		this.dataitems = val;
		if (val == null) {
			this.items = [];
			this.rowCount = 0;
		} else {
			this.items = val.value;
			this.rowCount = val.rowsCount;
			this.processing = false;
		}
	}

	@Input() set defaultColumnOrder(value: string) {
		if (!StringHelper.isEmptyOrNull(value)) {
			this.state = { ...this.state, OrderBy: value };
		}
	}

	@ViewChild('fromInput', {
		read: MatInput
	}) fromInput: MatInput;

	@ViewChild('toInput', {
		read: MatInput
	}) toInput: MatInput;
	year = '';
	items: any[];
	dataitems: any;
	rowCount: number;
	checkedColumns: boolean[] = [];
	state: IFilterOption = {
		Page: 1,
		PageSize: AppSettings.DEFAULT_PAGE_SIZE,
		OrderBy: 'reference',
		SortDirection: SortDirection.Descending,
		SearchQuery: ''
	};
	pageSizeOptions = AppSettings.PAGE_SIZE_OPTIONS;
	searchFormControl = new FormControl();
	columnType: typeof ColumnType = ColumnType;
	sortDirection: typeof SortDirection = SortDirection;
	periodType: typeof PeriodType = PeriodType;
	filtersOpened = false;
	filtersOpenedDate = false;
	profile;
	stat;
	chantier;
	periode;
	client;
	fournisseur;
	moi;
	compte;
	technicien;
	minDate = null;
	maxDate = null;
	isPlanification;
	doctype: string;
	helper = ManageDataTable;
	kinds = null;
	missionKind: typeof MissionKind = MissionKind;
	typeMession = null;
	constructor(
		public translate: TranslateService,
		private router: Router,
		private route: ActivatedRoute,
		private dialog: MatDialog,

	) { }

	ngOnInit(): void {
		this.isPlanification = this.route.snapshot.data['isPlanification'];
		ManageDataTable.isPlanification = this.isPlanification;
		const isAgendaGlobal = this.route.snapshot.data['isAgendaGlobal'];
		ManageDataTable.isAgendaGlobal = isAgendaGlobal;
		this.retrieveState();
		this.getCheckedColumns();
		this.subscribeSearchQuery();
	}
	addPaiementGroupe() {
		ManageDataTable.addPaiementGroupe()
	}

	mouvementCompte() {
		ManageDataTable.MouvementCompte()
	}
	resetForm() {
		if (this.periodes.length === 0 || this.periode === PeriodType.Interval) {
			this.fromInput.value = '';
			this.toInput.value = '';
		}

		this.state['dateStart'] = null;
		this.state['dateEnd'] = null;

		if (ManageDataTable.docType === ApiUrl.journal) {
			this.state['periodType'] = PeriodType.None;
		}

		this.saveState();
	}

	initFilter() {
		this.profile = '';
		this.stat = '';
		this.chantier = '';
		this.periode = PeriodType.None;
		this.client = '';
		this.fournisseur = '';
		this.moi = '';
		this.compte = '';
		this.technicien = '';
		this.year = '';
		this.kinds = [];
	}

	subscribeSearchQuery() {
		this.searchFormControl.valueChanges.pipe(
			debounceTime(1000),
			distinctUntilChanged()
		).subscribe(searchQuery => {
			this.state.Page = 1;
			this.state.SearchQuery = searchQuery;
			this.saveState();
		});
	}

	sortChange(direction, event: any) {
		if (!StringHelper.isEmptyOrNull(event)) {
			this.state.OrderBy = event;
			this.state.SortDirection = direction;
			this.saveState();
		}
	}
	depenseGroupe() { return ManageDataTable.docType === ApiUrl.Depense }
	exportInvoice() { return ManageDataTable.docType === ApiUrl.Invoice && this.idChantier === null }
	isPaiement() { return ManageDataTable.docType === ApiUrl.Payment }

	initState() {
		this.state = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			OrderBy: 'reference',
			SortDirection: SortDirection.Descending,
			SearchQuery: ''
		}
	}

	chagePageNumber(event: any) {
		this.state.Page = event;
		this.saveState();
	}

	changePageSize(event: any) {
		this.state.PageSize = event;
		this.saveState();
	}

	onMenuOpened(element: any): void {
		this.selected = element;
	}

	getCheckedColumns() {
		// const checkedColumns = localStorage.getItem(`item_column_${this.name}`);
		const checkedColumns = localStorage.getItem(`item_column`);
		const data = JSON.parse(checkedColumns) as boolean[];
		this.checkedColumns = (data == null ? this.getDefaultCheckedColumns() : data);
	}

	setCheckedColumns(value: boolean[]) {
		localStorage.setItem(`item_column`, JSON.stringify(value));
	}

	getDefaultCheckedColumns() {
		const checkedColumns = [];
		for (const _ of this.columns) { checkedColumns.push(false); }
		if (this.isAction) { checkedColumns.push(false); }
		return checkedColumns;
	}

	doubleClick(element) {
		ManageDataTable.getDetails(element);
	}

	// #endregion

	// #region helpers

	getColumnsName() {
		const columns = this.columns.map(e => e.name);
		if (this.isAction) {
			columns.push('actions');
		}
		return columns;
	}

	getColumnsNameTranslated() {
		const columns = this.columns.map(e => e.nameTranslate);
		if (this.isAction) {
			columns.push('LABELS.ACTIONS');
		}
		return columns;
	}

	emitChange() {
		this.changeEvent.emit(this.state);
	}

	// #endregion

	// #region save and retrieve state

	saveState() {
		localStorage.setItem(`state`, JSON.stringify(this.state));
		this.emitChange();
	}


	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.columns.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}

	retrieveState() {
		const oldState = localStorage.getItem(`state`);
		if (oldState != null && oldState !== '') {
			this.state = JSON.parse(oldState);
			this.searchFormControl.setValue(this.state.SearchQuery);
		}
		this.emitChange();
	}

	getPlaceholder(): string {
		const searchTerms: any = ManageDataTable.getSearchTerms();

		if (searchTerms && Array.isArray(searchTerms)) {
			return this.translate.instant('search.by', {
				searchTerms: searchTerms.map(s => this.translate.instant(`search.${s}`).toLowerCase()).join(', ')
			});
		} else {
			return this.translate.instant('search.title');
		}
	}

	addClick() {
		if (ApiUrl.Groupe !== ManageDataTable.docType && ApiUrl.configurationLabel !== ManageDataTable.docType
			&& ApiUrl.Lot !== ManageDataTable.docType && ApiUrl.Mission !== ManageDataTable.docType && ApiUrl.typesMission !== ManageDataTable.docType
			&& ApiUrl.configDocumentType !== ManageDataTable.docType && ApiUrl.configModeRegelemnt !== ManageDataTable.docType) {
			this.router.navigate([ManageDataTable.getCreateRoute()]);
		} else {
			ManageDataTable.getCreateRoute();
		}
	}

	selectProfil(e) {
		this.state.Page = 1;
		this.state['RoleIds'] = e.value !== null && e.value !== '' ? [e.value] : [];
		this.saveState();
	}

	selectStatus(e) {
		this.state.Page = 1;
		if (ApiUrl.VisitMaintenance === ManageDataTable.docType) {
			this.state['status'] = e.value !== null && e.value !== '' ? e.value : '';
		} else {
			this.state['status'] = e.value !== null && e.value !== '' ? [e.value] : [];
		}
		this.saveState();
	}

	selectChantier(e) {
		this.state.Page = 1;
		this.state['workshopId'] = e.value !== null && e.value !== '' ? e.value : null;
		this.saveState();
	}

	selectClient(e) {
		this.state.Page = 1;
		if (ApiUrl.MaintenanceContrat === ManageDataTable.docType || ApiUrl.VisitMaintenance === ManageDataTable.docType) {
			this.state['externalPartnerId'] = e.value !== null && e.value !== '' ? [e.value] : [];
		} else if (ApiUrl.Chantier === ManageDataTable.docType) {
			this.state['clientId'] = e.value !== null && e.value !== '' ? e.value : null;
		} else {
			this.state['externalPartnerId'] = e.value !== null && e.value !== '' ? e.value : null;
		}
		this.saveState();
	}

	selectMinDate(e) {
		this.state.Page = 1;
		this.minDate = AppSettings.formaterDatetime(e.value);
		if (this.periodes.length !== 0 || !this.periode) {
			this.state['dateStart'] = this.minDate && this.minDate !== null && this.minDate !== '' ? this.minDate : null;
			this.saveState();
		}

	}

	filter() {
		this.state['periodType'] = PeriodType.Interval;
		this.state['dateStart'] = this.minDate;
		this.state['dateEnd'] = this.maxDate;
		this.saveState();
	}

	selectMaxDate(e) {
		this.state.Page = 1;
		this.maxDate = AppSettings.formaterDatetime(e.value);
		if ((this.periodes.length !== 0 || !this.periode) && new Date(this.maxDate.split('T')[0]) > new Date(this.minDate.split('T')[0])) {
			this.state['dateEnd'] = this.maxDate && this.maxDate !== null && this.maxDate !== '' ? this.maxDate : null;
			this.saveState();
		} else {
			toastr.warning('Date fin doit être supérieur ou égale à la date de début ', '',
				{ positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}
	}

	selectSupplier(e) {
		this.state.Page = 1;
		this.state['externalPartnerId'] = e.value !== null && e.value !== '' ? e.value : null;
		this.saveState();
	}

	selectCompte(e) {
		this.state.Page = 1;
		this.state['accountId'] = e.value !== null && e.value !== '' ? e.value : null;
		this.saveState();
	}

	selectMois(e) {
		this.state.Page = 1;
		this.state['month'] = e.value !== null && e.value !== '' ? e.value : [];
		this.saveState();
	}

	selectYear() {
		this.state.Page = 1;
		this.state['year'] = this.year;
		this.saveState();
	}
	selectMisionKind(e) {
		this.state.Page = 1;
		this.state['kinds'] = e.value !== null && e.value !== '' ? e.value : [];
		this.saveState();
	}
	selectPeriode(e) {
		this.state.Page = 1;
		if (this.periode !== PeriodType.Interval) {
			this.state['periodType'] = e.value !== null && e.value !== '' ? e.value : PeriodType.None;
			this.saveState();
			this.filtersOpenedDate = false;
		}
	}

	selectTechnicie(e) {
		this.state.Page = 1;
		this.state['techniciansId'] = e.value !== null && e.value !== '' ? e.value : [];
		this.saveState();
	}

	// clearStatutFilter() {
	// 	this.statut = null;
	// }

	onFiltersTriggered(): void {
		this.filtersOpened = !this.filtersOpened;
	}

	onFiltersTriggeredDate(): void {
		this.filtersOpenedDate = !this.filtersOpenedDate;
	}

	onBackdropClicked(): void {
		ManageDataTable.initFilters();
		this.initFilter();
		this.initState();
		this.saveState();
	}

	onBackdropClickedDate(): void {
		this.resetForm();
		this.periode = PeriodType.None;

	}

	getTextFiltrePeriod(period) {
		if (period === PeriodType.Interval) {

			return `de ${GlobalInstances.datePipe.transform(new Date(
				this.state['dateStart']
			), 'dd/MM/yyyy')} à ${GlobalInstances.datePipe.transform(new Date(
				this.state['dateEnd']
			), 'dd/MM/yyyy')}`;
		}
		return this.periodes.find(x => x.value === period).label;

	}

}
