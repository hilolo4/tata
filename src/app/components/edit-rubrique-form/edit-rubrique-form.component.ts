import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	selector: 'app-edit-rubrique-form',
	templateUrl: './edit-rubrique-form.component.html',
	styleUrls: ['./edit-rubrique-form.component.scss']
})
export class EditRubriqueFormComponent implements OnInit {
	form
	constructor(
		private translate: TranslateService,
		public dialogRef: MatDialogRef<EditRubriqueFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { type: number, data: any },
		private fb: FormBuilder,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {
		this.form = this.fb.group({
			Value: ['', Validators.required/*, this.checkUniqueNom.bind(this)*/]
		});
		if (this.data.data != null) {
			this.form.setValue({
				Value: this.data.data.value
			});
		}
	}
	getTitle() {
		//  {{ 'labels.ajouter' | translate }} {{'labels.rubrique' | translate}}
		if (this.data.data == null) {
			return this.translate.instant('labels.addRubrique')
		}
		if (this.data.data != null) {
			return this.translate.instant('labels.editRubrique')
		}
	}
	save() {
		this.dialogRef.close(this.form.value);
	}

	get f() { return this.form.controls; }


}
