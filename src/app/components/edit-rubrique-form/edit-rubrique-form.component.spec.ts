import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRubriqueFormComponent } from './edit-rubrique-form.component';

describe('EditRubriqueFormComponent', () => {
  let component: EditRubriqueFormComponent;
  let fixture: ComponentFixture<EditRubriqueFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRubriqueFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRubriqueFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
