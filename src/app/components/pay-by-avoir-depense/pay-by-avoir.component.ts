import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { Expense } from 'app/Models/Entities/Documents/Expense';
import { Credit } from 'app/Models/Entities/Documents/Credit';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

declare var jQuery: any;
@Component({
	// tslint:disable-next-line:component-selector
	selector: 'pay-by-avoir-depense',
	templateUrl: './pay-by-avoir.component.html',
	styleUrls: ['./pay-by-avoir.component.scss']
})
export class PayByAvoirDepenseComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('refresh') refresh = new EventEmitter()
	// tslint:disable-next-line:no-input-rename
	@Input('restPayer') restPay: number
	// tslint:disable-next-line:no-input-rename
	@Input('idChantier') idChantier;
	// tslint:disable-next-line:no-input-rename
	@Input('depense') depense: Expense;

	restPayCached
	idChantierCached;
	avoirs: Credit[] = [];
	search = ''
	page = 1;
	totalPages = 0
	finished = true;

	constructor(@Inject('IGenericRepository') private service: IGenericRepository<any>
	) { }

	ngOnInit() {
		this.GetAvoirs();
		this.restPayCached = this.restPay;
		this.idChantierCached = this.idChantier;
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngOnChanges() {
		if (this.idChantierCached !== this.idChantier || this.restPayCached !== this.restPay) {
			this.searchAvoir()
		}
	}

	/**
	 * Selectioné Avoir
	 */
	selectAvoir(index) {
		this.avoirs.map(x => x['checked'] = false);
		this.avoirs[index]['checked'] = true;
	}

	GetAvoirs() {
		if (this.idChantier !== null) {
			this.service
				.getAllPagination(ApiUrl.avoir, this.getFilters())
				.subscribe((res) => {
					this.totalPages = res.rowsCount;
					const newDepenses = res.value.filter(value => this.avoirs.filter(x => x.id === value.id).length === 0 &&
						value.totalTTC * (-1) < this.restPay);
					this.avoirs = [...this.avoirs, ...newDepenses]
				});
		}
	}


	getFilters() {
		return {
			'externalPartnerId': '',
			'workshopId': this.idChantier,
			'status': [StatutAvoir.Encours],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 5,
			'sortDirection': 'Ascending',
			'orderBy': 'reference'
		};
	}


	searchAvoir() {
		this.avoirs = []
		this.page = 1;
		this.totalPages = 0;
		this.GetAvoirs()
	}

	onScroll() {
		this.finished = false;
		if (this.totalPages <= this.page) {
			this.finished = true;
		} else {
			this.page++;
			this.GetAvoirs();
		}
	}


	savePaiement() {
		const selectAvoir = this.avoirs.filter(x => x['checked'] === true)
		if (this.avoirs.filter(x => x['checked'] === true).length > 0) {
			this.refresh.emit(selectAvoir[0])
			jQuery('#choixAvoir').modal('hide');
			this.avoirs = this.avoirs.filter(x => x['checked'] === true)
		}
	}

}
