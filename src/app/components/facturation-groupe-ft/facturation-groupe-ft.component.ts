import { Component, Input, Inject } from '@angular/core';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { CreateFacture } from 'app/Enums/CreateFacture.Enum';
import { TranslateService } from '@ngx-translate/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { Router } from '@angular/router';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { OperationSheet } from 'app/Models/Entities/Documents/OperationSheet';
import { ClientMinimalInfo } from 'app/Models/Entities/Contacts/Client';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var jQuery: any;
declare var toastr: any;
@Component({
	selector: 'app-facturation-groupe-ft',
	templateUrl: './facturation-groupe-ft.component.html',
	styleUrls: ['./facturation-groupe-ft.component.scss']
})
export class FacturationGroupeFTComponent {

	ficheInterventionUnselected: OperationSheet[] = [];
	// tslint:disable-next-line:no-input-rename
	@Input('chantiers') chantiers: Workshop[] = [];
	fichesIntervention: OperationSheet[] = []
	idChantier;
	finished = true;
	search = ''
	page = 1;
	totalPage = 0;

	chantierSelected: Workshop[] = []


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private translate: TranslateService,
	) { }

	choisirChantier(info: { idChantier: number, idFournisseur: number }): void {
		this.idChantier = info.idChantier;
		this.ficheInterventionUnselected = []
		this.fichesIntervention = []
		jQuery('#factureGroupee').modal('show');
		this.ficheinterventionlist();
	}

	// Récupérure la listes des fiches intervention
	ficheinterventionlist() {

		this.finished = false;

		this.service.getAllPagination(ApiUrl.FicheIntervention, this.getFilters())
			.subscribe((res) => {

				const listFichesIntervention = res.value.filter(value => this.fichesIntervention.filter(x => x.id === value.id).length === 0);
				this.totalPage = res.rowsCount;
				listFichesIntervention.forEach(fiche => {
					this.ficheInterventionUnselected.push(fiche)
				})
				this.finished = true;
			});

	}

	// this.technicien
	getFilters() {
		return {
			'externalPartnerId': '',
			'workshopId': this.idChantier,
			'status': [StatutFicheIntervention.Realisee],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference'
		};
	}

	// On cas de scroll dans popup des ft
	onScroll() {

		this.page++;
		this.ficheinterventionlist();
		if (this.totalPage === this.page) {
			this.finished = true;
		}
	}

	// Recherche des ft dans popup
	searchFT() {
		this.page = 1;
		this.ficheInterventionUnselected = [];
		this.ficheinterventionlist();
	}

	// selectioné fiche intervention

	addFicheIntervention(index) {
		const ficheIntervention = this.ficheInterventionUnselected[index]
		this.fichesIntervention.push(ficheIntervention)
		this.ficheInterventionUnselected.splice(index, 1)
	}
	// désélectioné fiche intervention
	removeFicheIntervention(index) {
		const ficheIntervention = this.fichesIntervention[index]
		this.ficheInterventionUnselected.push(ficheIntervention)
		this.fichesIntervention.splice(index, 1)
	}

	savefiche() {
		if (this.fichesIntervention.length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.SelectinneFicheRequired, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			})
		}
		if (this.fichesIntervention.length > 0) {
			const facture = new Invoice();
			let articles = [];
			const idsFicheIntervention = [];
			this.fichesIntervention.forEach(ficheIntervention => {
				idsFicheIntervention.push(ficheIntervention.id);
				articles = [...articles, ...ficheIntervention['orderDetails']['lineItems']];
				const chantier = new Workshop();
				chantier.id = ficheIntervention['workshopId'];
				facture.workshop = chantier;

				const cl = new ClientMinimalInfo();
				cl.id = ficheIntervention['clientId'].toString();
				facture.client = cl;
				facture['client']['addresses'] = ficheIntervention['adresses'] as Adresse[];

				const adresseFacturation = ficheIntervention['adresses'] as Adresse[];
				facture['addressIntervention'] = adresseFacturation.filter(A => A.isDefault === true)[0];
			});

			const orderDetails = {
				'globalDiscount': {
					'value': 0,
					'type': TypeValue.Amount
				},
				'holdbackDetails': {
					'warrantyPeriod': 12,
					'holdback': 0.,
					'warrantyExpirationDate': new Date(),
				},
				'globalVAT_Value': 0,
				'puc': 0,
				'proportion': 0,
				'productsDetailsType': OrderProductsDetailsType.List,
				'lineItems': articles,
				'productsPricingDetails': {
					'totalHours': 0,
					'salesPrice': 0
				},
				'totalHT': 0,
				'totalTTC': 0
			};
			facture.orderDetails = orderDetails;
			const res = {
				'facture' : facture,
				'idsFicheIntervention' : idsFicheIntervention
			}
			localStorage.setItem(LocalElements.docType , ApiUrl.Invoice);
			localStorage.setItem(LocalElements.docTypeGenerer , 'FactureGroupe');
			localStorage.setItem('FactureGroupe' , JSON.stringify(res) );
			this.router.navigate(['/factures/generer-create', '']);
			jQuery('#factureGroupee').modal('hide');
		}
	}

}
