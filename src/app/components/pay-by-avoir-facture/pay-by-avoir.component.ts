import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Credit } from 'app/Models/Entities/Documents/Credit';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'pay-by-avoir',
	templateUrl: './pay-by-avoir.component.html',
	styleUrls: ['./pay-by-avoir.component.scss']
})
export class PayByAvoirComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('refresh') refresh = new EventEmitter()
	// tslint:disable-next-line:no-input-rename
	@Input('restPayer') restPay: number
	// tslint:disable-next-line:no-input-rename
	@Input('idClient') idClient;
	// tslint:disable-next-line:no-input-rename
	@Input('facture') facture: Invoice;
	// tslint:disable-next-line:no-input-rename
	@Input('initAvoir') initAvoir = false;

	restPayCached;
	idClientCached;
	avoirs: Credit[] = []
	search = ''
	page = 1;
	totalPages = 0
	finished = true;
	AvoirIsInit = false;
	statutAvoir: typeof StatutAvoir = StatutAvoir;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
	) { }

	ngOnInit() {
		this.restPayCached = this.restPay;
		this.idClientCached = this.idClient;
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngOnChanges() {
		if (this.initAvoir === true) {
			this.GetAvoirs();
		}
		if ((this.idClientCached !== this.idClient || this.restPayCached !== this.restPay) && this.initAvoir === true) {
			this.searchAvoir()
		}
	}

	// selectioné avoir
	selectAvoir(index) {
		this.avoirs.map(x => x['checked'] = false);
		this.avoirs[index]['checked'] = true;
	}

	// Get avoirs
	GetAvoirs() {

		if (this.AvoirIsInit === true) {
			return
		}
		if (this.idClient !== null) {
			this.service.getAllPagination(ApiUrl.avoir, this.getFilters())
				.subscribe(res => {
					const newFactures = res.value.filter(value => this.avoirs.filter(x => x.id === value.id).length === 0
						&& value.totalTTC * (-1) < this.restPay
					);
					this.avoirs = [...this.avoirs, ...newFactures];
					this.AvoirIsInit = true
				})
		}
	}

	getFilters() {
		return {
			'externalPartnerId': this.idClient,
			'workshopId': '',
			'status': [this.statutAvoir.Encours],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': '',
			'page': this.page,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference'
		};
	}

	// Recherche dans les avoirs
	searchAvoir() {
		this.avoirs = []
		this.page = 1;
		this.totalPages = 0;
		this.GetAvoirs()
	}

	// Event scroll dans la liste des factures
	onScroll() {
		this.finished = false;
		if (this.totalPages <= this.page) {
			this.finished = true;
		} else {
			this.page++;
			this.GetAvoirs();
		}
	}

	savePaiement() {
		const selectedAvoir = this.avoirs.filter(x => x['checked'])
		if (this.avoirs.filter(x => x['checked']).length > 0) {
			this.refresh.emit(selectedAvoir[0])
			jQuery('#choixAvoir').modal('hide');
			this.avoirs = this.avoirs.filter(x => !x['checked'])
		}
	}

}
