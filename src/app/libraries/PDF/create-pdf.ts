
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { AppSettings } from '../../app-settings/app-settings';
import { GlobalInstances } from '../../app.module';
import { ApiUrl } from '../../Enums/Configuration/api-url.enum';
import { ActionPdf } from '../../Enums/typeValue.Enums';
import { Attachments, SendMailParams } from '../../Models/Entities/Commun/SendMailParams';
import { HelperPdf } from './helper-pdf';
import { UtilsPDf } from './utils-pdf';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


export class CreatePdf {

	static parametragePdf;


	public static async generatePdf(document, docType, action, formSendEmail?) {
		await GlobalInstances.genericService.getAll(ApiUrl.configParametragePDF).subscribe(res => {
			let margingTopTable = 35
			this.parametragePdf = JSON.parse(res);
			const content = [];
			const contentPdf = [
				this.getInfoSociete(),
				HelperPdf.generateColRefAndClient(document, docType)
			]

			content.push(contentPdf);
			if (AppSettings.API_ENDPOINT.includes('ets-borel')) {
				content.push(HelperPdf.certifBorel())
			}

			if (document.purpose) {
				content.push(HelperPdf.getObjet(document.purpose));
			}
			switch (docType) {
				case ApiUrl.MaintenanceOperationSheet:
					content.push(HelperPdf.generateTableGME(document, docType))

					if (document.report) { margingTopTable = 25; content.push(HelperPdf.rapportContratMaintenance(document.report)) }
					content.push(HelperPdf.siganturesContratMaintenance(document, margingTopTable));
					break;
				case ApiUrl.FicheIntervention:
					content.push(HelperPdf.getTable(document, docType));
					if (document.report) { margingTopTable = 25; content.push(HelperPdf.rapportContratMaintenance(document.report)) }
					content.push(HelperPdf.siganturesContratMaintenance(document, margingTopTable));
					break;

				case ApiUrl.Devis:
				case ApiUrl.Invoice:
				case ApiUrl.avoir:
					content.push(HelperPdf.getTable(document, docType));
					content.push(HelperPdf.ConditioRegAndTableTotal(document));
					if (document.note) { content.push(HelperPdf.Note(document.note)) }
					content.push(HelperPdf.CachetEtSiganture(document, docType, this.parametragePdf));
					// if ([ApiUrl.Devis, ApiUrl.Invoice].includes(docType)) {// && AppSettings.API_ENDPOINT.includes('ets-borel') ){
					// 	content.push(HelperPdf.RemerciementBorel());
					// }
					break;
				default:
					break;
			}


			try {
				const pdf = pdfMake.createPdf({
					pageSize: 'A4',
					pageMargins: [40, 40, 40, 55],

					header: this.getHeader(),
					footer: this.getFooter(),
					content: [
						content
					],

				});
				switch (action) {
					case ActionPdf.print:
						const win = window.open('', '_blank');
						pdf.print({}, win);
						break;
					case ActionPdf.download:
						// pdf.download(`${HelperPdf.getNamedocType(docType)} ${document.reference}.pdf`);
						pdf.open();
						break;
					case ActionPdf.base64:
						pdf.getBase64(async (base) => {
							const attachement: Attachments = {
								FileId: '',
								content: base,
								FileName: `${HelperPdf.getNamedocType(docType)} ${document.reference}.pdf`,
								FileType: 'application/pdf'
							}
							const mailParams: SendMailParams = {
								To: [formSendEmail.controls.emailTo.value],
								Subject: formSendEmail.controls.object.value,
								body: formSendEmail.controls.contents.value,
								path: '',
								Bcc: [],
								Cc: [],
								attachments: [attachement],
							};
							GlobalInstances.genericService.sendMail(docType, document.id, mailParams).subscribe(res => {
								// this.close();
								// this.processing = false;
							}, reject => console.error(reject));
						});
						break;

					default:
						break;
				}
			} catch (error) {
				console.log(error);
			}

		});
	}

	public static getHeader() {
		return {
			table: {
				dontBreakRows: true,
				widths: ['*'],
				body: [
					[
						{ text: '\n', fillColor: '#47A2C1' }
					]
				]
			},
			layout: 'noBorders'
		};
	}

	public static getFooter() {
		return {
			table: {
				dontBreakRows: true,
				widths: ['*'],
				body: [
					[
						{
							text: '\n\n', fillColor: '#EFEFEF', margin: [20, 0, 0, 0]
						}
					],
					[
						{
							text: this.parametragePdf.footer.line_1 + '\n' +
								this.parametragePdf.footer.line_2 + '\n' +
								this.parametragePdf.footer.line_3 + '\n',
							fontSize: 8,
							alignment: 'center',
							color: 'black',
							fillColor: '#EFEFEF',
							absolutePosition: { x: 0, y: 17 }, margin: [0, 0, 0, 20]
						}
					]
				]
			},
			layout: 'noBorders',
			margin: [0, 0, 0, 0]
		};
	}

	public static getInfoSociete() {
		const header = this.parametragePdf.header;
		const tel = header.phoneNumber ? `\nTél: ${header.phoneNumber}` : '';
		const email = header.email ? `\nEmail: ${header.email}` : '';
		return {
			table: {
				widths: ['64%', '36%'],
				body: [
					[
						{
							width: '64%',
							stack: [{
								image: this.parametragePdf.images.logo,
								width: 180, margin: [0, 0, 0, 15]
							}]
						},
						{
							width: '36%',
							table: {
								body: [
									[{ text: header.companyName, bold: true, fontSize: 12 }],
									[{
										text: UtilsPDf.removeNullText(`${header.address.street}
									${header.address.postalCode} ${header.address.city}${tel}${email}`),
										fontSize: 10
									}],
								]
							},
							layout: 'noBorders'
						}
					]

				]
			}, margin: [-20, 0, 0, 10],
			layout: AppSettings.API_ENDPOINT.includes('ets-borel') ? {
				vLineWidth: (i, node) => {
					return (i === 1 || i === node.table.body.length) ? 0 : 1;
				}
			} : 'noBorders',

			// columns: [
			// 	{
			// 		width: '64%',
			// 		stack: [{
			// 			image: this.parametragePdf.images.logo,
			// 			width: 180, margin: [0, 0, 0, 15]
			// 		}]
			// 	},
			// 	{
			// 		width: '36%',
			// 		table: {
			// 			body: [
			// 				[{ text: header.companyName, bold: true, fontSize: 12 }],
			// 				[{
			// 					text: UtilsPDf.removeNullText(`${header.address.street}
			// 				${header.address.postalCode} ${header.address.city}${tel}${email}`),
			// 					fontSize: 10
			// 				}],
			// 			]
			// 		},
			// 		layout: 'noBorders'
			// 	}
			// ],
			// margin: [-20, 0, 0, 10],
		}
	}


}
