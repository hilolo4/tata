import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { DialogOptions } from 'app/Models/Model/alert-options';
import { ConfigDialogComponent } from 'app/shared/config-dialog/config-dialog.component';

interface IConfirmDialogMessages {
	header: string;
	message: string;
	cancel: string;
	confirm: string;
}

export class DialogHelper {

	/**
	 * small size of dialog
	 */
	static SIZE_SMALL = '350px';


	/**
	 * small size of dialog
	 */
	static SIZE_SMALLTache = '420px';

	/**
	 * medium size of dialog
	 */
	static SIZE_MEDIUM = '550px';
	/**
	 * large size
	 */
	static SIZE_LARGE = '900px';

	/**
	 * open alert dialog
	 * @param dialog Service to open Material Design modal dialogs.
	 * @param options the alert dialog options
	 * @param dialogComponent the dialog that you want to open
	 */
	static openAlertDialog(dialog: MatDialog, options: DialogOptions, dialogComponent: any): MatDialogRef<any> {
		return dialog.open(dialogComponent, {
			hasBackdrop: true,
			disableClose: true,
			data: options,
			width: '425px'
		});
	}

	/**
	 * open confirm dialog
	 * @param dialog the dialog service
	 * @param translatedMessages the messages translated
	 * @param callbackSuccess the callback click confirm
	 */
	static openConfirmDialog(dialog: MatDialog, translatedMessages: IConfirmDialogMessages, callbackSuccess) {
		const options: DialogOptions = {
			header: translatedMessages.header,
			message: translatedMessages.message,
			buttons: [
				{
					text: translatedMessages.cancel,
					handler: () => { },
					cssClass: 'warn',
				},
				{
					text: translatedMessages.confirm,
					cssClass: 'primary',
					handler: () => {
						callbackSuccess();
					}
				}
			],
		};
		DialogHelper.openAlertDialog(dialog, options, ConfigDialogComponent);
	}

	/**
	 * open dialog
	 */
	static openDialog(dialog: MatDialog, dialogComponent: any, width: string, data: any): Observable<any> {
		const dialogRef = dialog.open(dialogComponent, { width, data, hasBackdrop: true });
		return dialogRef.afterClosed();
	}

}
