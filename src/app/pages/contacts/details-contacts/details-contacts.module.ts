import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsContactsComponent } from './details-contacts.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MultiLoader } from '../contacts.module';
import { MatTabsModule, MatMenuModule, MatIconModule, MatDialogModule, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FicheContactsComponent } from './fiche-contacts/fiche-contacts.component';
import { CommonModules } from 'app/common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { ClientAuthComponent } from 'app/components/clientAuth/clientAuth.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MaterielComponent } from 'app/components/add-material/materiel.component';



@NgModule({
  declarations: [
    DetailsContactsComponent,
    FicheContactsComponent,
    ClientAuthComponent,
    MaterielComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    ShowHidePasswordModule,
    CommonModules,
    NgbTooltipModule,
    SplitButtonModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: MultiLoader,
        deps: [HttpClient],
      },
      isolate: true,
    }),
    RouterModule.forChild([
      {
        path: '',
        component: DetailsContactsComponent,
      }
    ])
  ],
  providers: [{ provide: MAT_DIALOG_DATA, useValue: {} },
  { provide: MatDialogRef, useValue: {} }],
  entryComponents: [ClientAuthComponent, MaterielComponent]
})
export class DetailsContactsModule { }
