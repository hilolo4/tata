import { Component, OnInit, Inject } from '@angular/core';
import { StatutContratEntretien } from 'app/Enums/Statut/StatutContratEntretien.Enum';
import { ActionHistorique } from 'app/Enums/ActionHistorique.Enum';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';;
import * as _ from 'lodash';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { ContratEntretienPut, MaintenanceEquipement } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { GammeMaintenanceEquipement, OperationMaintenance } from 'app/Models/Entities/Maintenance/GammeMaintenanceEquipement';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { Attachement } from 'app/Models/Entities/Commun/DocumentAttacher';
import { ListeGammeMaintenanceEquipementComponent } from 'app/common/liste-gamme-maintenance-equipement/liste-gamme-maintenance-equipement.component';
import { PeriodType } from '../../../Enums/Maintenance/typePeriode.Enum';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { ReccurenteModel, Reccurente } from 'app/Models/Model/ReccurenteModel';
import { PersonaliseReccurenteComponent } from 'app/common/personalise-reccurente/personalise-reccurente.component';
import { PeriodicityRecurringType, PeriodicityType, PeriodicityEndingType } from 'app/Enums/TypeRepetition.Enum';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { OrderDetails } from 'app/Models/Entities/Commun/OrderDetails';
import { TypeDocument } from 'app/Enums/TypeDocument.enums';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { GlobalInstances } from 'app/app.module';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { Action } from 'app/Enums/action';
import { LocalElements } from 'app/shared/utils/local-elements';
import { filter, debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { VariableAst } from '@angular/compiler';
import { Color } from 'app/Enums/color';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { DialogHelper } from 'app/libraries/dialog';

declare var toastr: any;
declare var swal: any;
@Component({
	selector: 'app-add',
	templateUrl: './add.component.html',
	styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

	statutContratEntretien: typeof StatutContratEntretien = StatutContratEntretien;
	actionHistorique: ActionHistorique = new ActionHistorique();
	dateLang: any;
	idClient: number = null;
	listClients: Client[] = null;
	clients: Client[] = null;
	clientSelected: Client;
	processing = true;
	creationForm
	adresses: Adresse[] = [];
	gammeMaintenanceEquipementListe: GammeMaintenanceEquipement[] = null;
	emitter: any = {};
	emitterArticle: any = {};
	selectedGammeMaintenanceEquipement = 0;
	PeriodType: typeof PeriodType = PeriodType;
	// statutReccurente: typeof StatutReccurente = StatutReccurente;
	// typeTermineEnum: typeof TypeTermine = TypeTermine;
	gamme_maintenance_equipement_Selected: MaintenanceEquipement[] = [];
	gamme_maintenance_equipement_liste: GammeMaintenanceEquipement[] = [];
	gamme_maintenance_equipement_Selected_libelles: string[];
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	typeRepetitionEnum: typeof PeriodicityRecurringType = PeriodicityRecurringType;
	periodicityType: typeof PeriodicityType = PeriodicityType;
	periodicityEndingType: typeof PeriodicityEndingType = PeriodicityEndingType;
	result: ReccurenteModel = new ReccurenteModel();
	dataDialog: ReccurenteModel = null;
	/** ****************************************
	 * files logique
	 **************************************** */
	piecesJointes: Attachement[] = [];
	//#region reccurent
	type;
	recurrente;
	min = new Date();
	articles = [];
	dataArticles;
	minFinDate = new Date();
	remise = 0;
	typeRemise = TypeValue.Amount;
	puc = 0;
	prorata = 0;
	retenueGarantie = 0;
	delaiGarantie = null;
	partProrata = false;
	participationPuc = false;
	retenuGaranti = false;
	docType = '';
	document;
	id = '';
	subscriptions: Subscription[] = [];
	color: typeof Color = Color;
	selectedStatus = '';
	status = [];
	listMaterials = [];
	materials = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private dialog: MatDialog) {
		this.docType = localStorage.getItem(LocalElements.docType);
		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		this.min.setDate(this.min.getDate() + 1);

		this.creationForm = this.buildCreationForm();

		this.subscriptions.push(
			this.router.events.pipe(
				debounceTime(0),
				filter(e => e instanceof NavigationEnd))
				.subscribe(async () => {
					if (
						EditDocumentHelper.isAction(Action.AJOUTER) || EditDocumentHelper.isAction(Action.DUPLIQUER)) {
						await this.initializeCreationForm();
					}
					if (
						EditDocumentHelper.isAction(Action.MODIFIER) || EditDocumentHelper.isAction(Action.DUPLIQUER)) {
						this.id = this.route.snapshot.params.id;
						await this.getDocument();
						this.setData();

					}
				})
		);

	}

	async ngOnInit() {
		this.processing = true;
		this.selectLanguage();
		this.listClients = await this.getClients('');
		this.type = Object.keys(PeriodType);


		this.processing = false;
	}

	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER); }

	getDocument(): Promise<any> {
		this.processing = true;
		return new Promise((resolve, reject) => {
			if (this.id && this.id !== '') {

				this.service.getById(this.docType, this.id).subscribe(res => {
					this.document = res.value;
					this.selectedStatus = this.document.status;
					this.CheckStatus();
					resolve(res.value);
					this.processing = false;
				});
			}
		});
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients('');
				if (client) {
					this.listClients.unshift(client);
					this.creationForm.controls['idClient'].setValue(client);
					this.adresses = client.addresses;
				}
			}
		});
	}

	CheckStatus() {
		if (this.selectedStatus === StatutContratEntretien.Brouillon) {
			this.status = [StatutContratEntretien.Enattente, StatutContratEntretien.Brouillon];
		}
		if (this.selectedStatus === StatutContratEntretien.Annule) {
			this.status = [StatutContratEntretien.Enattente, StatutContratEntretien.Annule];
		}
		if (this.selectedStatus === StatutContratEntretien.Enattente) {
			this.status = [StatutContratEntretien.Enattente, StatutContratEntretien.Annule];
		}
		if (this.selectedStatus === StatutContratEntretien.Encours) {
			this.status = [StatutContratEntretien.Encours, StatutContratEntretien.Annule];
		}
	}


	getReccurente(id): Promise<void> {
		this.processing = true;
		return new Promise((reslove, reject) => {
			this.service.getById(ApiUrl.Recurring, id).subscribe(async res => {
				this.recurrente = res.value;
				this.processing = false;
			}, err => {
				this.processing = false;
			}, () => {
				reslove();
				this.processing = false;
			});
		});
	}


	addDay(data) {
		if (data !== null && data !== undefined) {
			const da = new Date(data);
			return da.setDate(da.getDate() + 1);

		} else {
			return data;
		}
	}

	get f() { return this.creationForm.controls; }
	setDate() {
		const date = this.creationForm.controls['date_validation'].value;
		const clonedDate = new Date(date.getTime());
		this.minFinDate = new Date(clonedDate.setDate(clonedDate.getDate() + 1));
	}


	async setData() {
		this.processing = true;
		var data = this.document;

		if (data !== {} && data !== undefined && data !== null && data !== []) {

			this.adresses = this.document.client.addresses;
			const site = data.site;
			this.adresses = this.document.client.addresses;
			const index = this.getIndexOfAdress(site);
			this.creationForm.controls['site'].setValue(this.adresses[index]);
			if (this.isModif()) {
				this.creationForm.controls['reference'].setValue(this.document.reference);
			}
			const client = await this.getClient(data.client);
			this.creationForm.controls['idClient'].setValue(client);
			this.creationForm.controls['dateDebut'].setValue(new Date(data.startDate));
			this.creationForm.controls['dateFin'].setValue(new Date(data.endDate));
			this.creationForm.controls['expiration'].setValue(data.expirationAlertEnabled);
			this.creationForm.controls['periode'].setValue(data.expirationAlertPeriod);
			this.creationForm.controls['typePeriode'].setValue(data.expirationAlertPeriodType);
			this.creationForm.controls['enableAutoRenewal'].setValue(data.enableAutoRenewal);
			this.gamme_maintenance_equipement_Selected = this.formatGamme_maintenance_equipement_Selected(this.document.equipmentMaintenance);
			this.gamme_maintenance_equipement_Selected_libelles = this.gamme_maintenance_equipement_Selected.map(x => {
				return x.equipmentName;
			});
			this.piecesJointes = data.attachments;
			this.articles = data.orderDetails !== null && data.orderDetails !== undefined ?
				data.orderDetails.lineItems : [];
			if (data['recurringDocumentId']) {
				await this.getReccurente(data['recurringDocumentId']);
			}

			this.listMaterials = client.materials;
			this.materials = client.materials;

		}
		this.setDataFactureRecurrent(data.factureReccurent);
		this.processing = false;
	}

	formatGamme_maintenance_equipement_Selected(equipementContrat) {
		if (equipementContrat !== undefined) {
			equipementContrat.forEach((elem, index) => {
				if (elem.installationDate !== undefined && elem.installationDate !== null) {
					equipementContrat[index].installationDate = new Date(elem.installationDate);
				}
				equipementContrat[index].maintenanceOperations = this.formaterPeriodiciet(elem.maintenanceOperations);
			});
			return equipementContrat;
		}
	}

	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}

	getIndexOfAdress(adresse: Adresse) {
		let index = null;
		this.adresses.forEach((item, i) => {
			if (_.isEqual(item, adresse)) {
				index = i;
			}
		});
		return index;
	}
	/**
	 * @description construire le formulaire de création
	 */

	addYears(n) {
		const now = new Date();
		return new Date(now.setFullYear(now.getFullYear() + n));
	}

	buildCreationForm(): FormGroup {
		const oneyears = this.addYears(1);
		return this.formBuilder.group({
			reference: [null, [Validators.required], this.CheckUniqueReference.bind(this)],
			idClient: [null, [Validators.required]],
			site: [null, [Validators.required]],
			dateDebut: [new Date(), [Validators.required]],
			dateFin: [(oneyears), [Validators.required]],
			expiration: [false],
			periode: [0],
			typePeriode: [PeriodType.Day],
			enableAutoRenewal: [true],
			nbr_repetition: [null, [Validators.required]],
			dateRepetition: [],
			type_termine: [null, [Validators.required]],
			termine: [],
			date_validation: [null, [Validators.required]],
			date_fin: [],
			nbr_occurence: [0],
		});
	}

	/** --------------------------------------------------
		 @description initialiser le formulaire de création
		 -------------------------------------------------- */
	async initializeCreationForm(): Promise<void> {
		const oneyears = this.addYears(1);
		await this.generateReference();
		this.creationForm.controls['dateDebut'].setValue(new Date);
		this.creationForm.controls['dateFin'].setValue((oneyears));
	}
	/** ---------------------------------
		 @description Get List des Clients
		 --------------------------------- */

	getClients(search: string): Promise<Client[]> {
		this.processing = true;
		if (this.listClients == null) {
			return new Promise((resolve, reject) => {
				this.service.getAll(ApiUrl.Client).subscribe(
					res => {
						resolve(res.value)
						this.clients = res.value;
						this.processing = false;
					}
				);
			})
		}
		this.processing = false;
	}

	async getClient(client) {
		if (!this.listClients || this.listClients.length === 0) { await this.getClients('') }
		if (client && this.listClients && this.listClients.length !== 0) {
			const elem = this.listClients.find(x => x.id === client.id);
			if (elem === undefined) { this.listClients.unshift(client) };
			return this.listClients.find(x => x.id === client.id);
		}
	}

	async loadAddreseClient(client) {
		const clientInfos = await this.getClient(client);
		this.adresses = [];
		if (clientInfos == null) {
			this.adresses = [];
			this.creationForm.controls['site'].setValue(null);
			return;
		} else {
			const adresses: Adresse[] = clientInfos.addresses as Adresse[];
			this.adresses = adresses == null ? [] : adresses;
		}

		this.listMaterials = client.materials;
		this.materials = client.materials;
	}
	/** --------------------------------------------------------
		 @description définir la langue utilisée dans le composant
		 --------------------------------------------------------*/
	selectLanguage(): void {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	async createBodyRequest(statut) {
		const formeValue = this.creationForm.value;
		var contratEntretien: ContratEntretienPut = new ContratEntretienPut();

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			contratEntretien = this.document;
		}

		// ajouter les info supplémentaire
		contratEntretien.status = statut;


		if (!EditDocumentHelper.isAction(Action.MODIFIER)) {
			if (statut) {
				contratEntretien['status'] = statut;
				contratEntretien['reference'] = formeValue.reference + '-' + AppSettings.generateStampHM() + '-Brouillon';

			} else {
				contratEntretien['status'] = this.statutContratEntretien.Enattente
			}
		} else {
			if (this.selectedStatus === 'draft' && !statut) {
				contratEntretien['status'] = this.statutContratEntretien.Enattente
				contratEntretien['reference'] = await this.generateReference();
			}
			if (this.selectedStatus === 'draft' && statut) {
				contratEntretien['status'] = statut;

			}
			if (this.selectedStatus !== 'draft' && !statut) {
				contratEntretien['status'] = this.selectedStatus;

			}
		}
		contratEntretien.clientId = formeValue.idClient.id;
		contratEntretien.client = formeValue.idClient;
		contratEntretien.reference = formeValue.reference;
		let site: any = formeValue.site;
		if (site.designation === undefined) {
			site = this.adresses[+site];
		} else {
			site = formeValue.site;
		}
		contratEntretien.site = site;
		contratEntretien.startDate = AppSettings.formaterDatetime(formeValue.dateDebut);
		contratEntretien.endDate = AppSettings.formaterDatetime(formeValue.dateFin);
		contratEntretien.expirationAlertEnabled = formeValue.expiration;
		contratEntretien.expirationAlertPeriod = formeValue.periode;
		contratEntretien.expirationAlertPeriodType = formeValue.typePeriode;
		contratEntretien.enableAutoRenewal = formeValue.enableAutoRenewal;
		this.gamme_maintenance_equipement_Selected.forEach((elem, index) => {
			this.gamme_maintenance_equipement_Selected[index].maintenanceOperations = this.getOperation(elem.maintenanceOperations);
		});
		contratEntretien.equipmentMaintenance = this.gamme_maintenance_equipement_Selected;
		contratEntretien.attachments = this.piecesJointes;

		if (this.dataArticles.prestation.length > 0) {
			const orderDetails = {
				'globalDiscount': {
					'value': this.dataArticles.remise,
					'type': this.dataArticles.typeRemise
				},
				'holdbackDetails': {
					'warrantyPeriod': this.dataArticles.delaiGarantie,
					'holdback': this.dataArticles.retenueGarantie,
					'warrantyExpirationDate': new Date(),
				},
				'globalVAT_Value': this.dataArticles.tvaGlobal,
				'puc': this.dataArticles.puc,
				'proportion': this.dataArticles.prorata,
				'productsDetailsType': OrderProductsDetailsType.List,
				'lineItems': this.dataArticles.prestation,
				'productsPricingDetails': {
					'totalHours': 0,
					'salesPrice': 0
				},
				'totalHT': this.dataArticles.totalHt,
				'totalTTC': this.dataArticles.totalTtc
			};
			contratEntretien['orderDetails'] = orderDetails;
		} else {
			contratEntretien['orderDetails'] = null;
		}
		return contratEntretien;
	}
	async add(statut) {
		this.processing = true;
		const compareDate = this.creationForm.value.dateFin == null || this.creationForm.value.dateFin == null ? true :
			AppSettings.compareDate(this.creationForm.value.dateFin, this.creationForm.value.dateDebut);
		if (!compareDate) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.datesIntervention, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}
		if (!this.creationForm.valid) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}
		let res = null;
		this.gamme_maintenance_equipement_Selected.forEach((elem, index) => {
			const ele = elem.maintenanceOperations.filter(e => e.periodicity.length === 0 && e.subOperations.length === 0);
			res += ele.length;
		});

		if (res > 0) {
			toastr.warning('Veuillez sélectionner au moin une periode', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		this.dataArticles = await this.getDataFromArticlesComponet();

		if (this.dataArticles.prestation.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		if (this.gamme_maintenance_equipement_Selected === undefined || this.gamme_maintenance_equipement_Selected.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.fillAllGammeMaintenance, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		const createBody = await this.createBodyRequest(statut);
		const reccurence = await this.createBodyRequestFacture(statut);

		await this.service.create(ApiUrl.Recurring + ACTION_API.create, reccurence).subscribe(async res => {
			if (res.isSuccess) {
				createBody['RecurringDocumentId'] = res.value.id;
				await this.createContract(createBody);
			}
		}, async err => {
			console.log(err);
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}, () => {
			this.processing = false;
		});

	}

	async createContract(createBody) {
		this.processing = true;
		await this.service.create(ApiUrl.MaintenanceContrat + ACTION_API.create, createBody).subscribe(async res => {
			if (res.isSuccess) {
				const translation = await this.getTranslationByKey('add');
				toastr.success(translation.msg, translation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.router.navigate(['/contratentretiens/detail', res.value.id]);
				this.processing = false;
			}
		}, async err => {
			console.log(err);
			if (err.error && err.error.messageCode === 103) {
				const reference = this.creationForm.value.reference;
				const message = "Il exixte un contrat d'entretien  avec cette reference :  " + reference + "   Vous voulez créer cet contrat d'entretien par la nouevelle reference :"
					+ await this.generateReference();
				swal({
					title: '',
					text: message,
					icon: 'warning',
					buttons: {
						cancel: {
							text: 'Annuler',
							value: null,
							visible: true,
							className: '',
							closeModal: true
						},
						confirm: {
							text: 'Valider',
							value: true,
							visible: true,
							className: '',
							closeModal: true
						}
					}
				}).then(async isConfirm => {
					if (isConfirm) {
						createBody['reference'] = this.creationForm.value.reference;
						this.createContract(createBody);
					} else {
					}
				});

			} else {
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
			}
			this.processing = false;
		}, () => {
			this.processing = false;
		});
	}
	/**
	* get translation By Key
	*/
	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation)
			});
		});
	}

	async select_gamme_maintenance_equipement(): Promise<void> {
		this.gamme_maintenance_equipement_liste = await this.getGammeMaintenanceEquipementListe();
		this.gamme_maintenance_equipement_liste.forEach((elem, index) => {
			this.gamme_maintenance_equipement_liste[index].maintenanceOperations = this.formaterPeriodiciet(elem.maintenanceOperations);
		});
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.data = null;
		dialogLotConfig.width = '1000px';
		dialogLotConfig.height = '500px';
		const gamme_maintenance_equipement_liste = [];

		this.gamme_maintenance_equipement_liste.forEach(element => {
			const result = this.gamme_maintenance_equipement_Selected.filter(x => x.id === element.id);
			if (result.length === 0) {
				gamme_maintenance_equipement_liste.unshift(element);
			}
		});
		dialogLotConfig.data = gamme_maintenance_equipement_liste;

		const dialogRef = this.dialog.open(ListeGammeMaintenanceEquipementComponent, dialogLotConfig);

		dialogRef.afterClosed().subscribe((data) => {

			if (data !== '' && data !== undefined) {
				this.gamme_maintenance_equipement_Selected =
					this.gamme_maintenance_equipement_Selected.concat(data.map((x: GammeMaintenanceEquipement) => {
						x.maintenanceOperations = x.maintenanceOperations;
						return x
					}));
				this.gamme_maintenance_equipement_Selected_libelles = this.gamme_maintenance_equipement_Selected.map(gamme => {
					return gamme.equipmentName;
				});
			}

		});
	}

	/**
	 * Suprimmer une gamme de maintenance d'equipement
	 */
	suprimmer_gamme_maintenance_equipement(index): void {
		this.gamme_maintenance_equipement_Selected.splice(index, 1);
	}

	/**
	 * @desriprion Récupererla liste des gammes Maintenance d'equipement
	 */
	getGammeMaintenanceEquipementListe(): Promise<any> {
		this.processing = true;
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.GME).subscribe(res => {
				resolve(res.value);
				this.processing = false;
			});
		});
		this.processing = false;
	}

	/**
	 * @description la fonctionnalité principale de cette méthode est le changement d'onglet
	 * @param index l'index d'equipement dans @property gamme_maintenance_equipement_Selected
	 */
	async changeTab(index: number): Promise<void> {

		// selectionner l'index d'esquipepemnt déja afichée
		const currentIndex: number = this.selectedGammeMaintenanceEquipement;

		// récuperer la traduction
		const translation = await this.getTranslationByKey('errors');

		if (this.gamme_maintenance_equipement_Selected.length === 0) {
			return;
		}
		// vérifier si le nom est saisi
		if (this.gamme_maintenance_equipement_Selected[currentIndex].equipmentName.length === 0) {
			toastr.warning(translation.equipement.minLengthNom, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			return;
		}

		// vérifier si le nom est supérieur à trois caractéres
		if (this.gamme_maintenance_equipement_Selected[currentIndex].equipmentName.length < 3) {
			toastr.warning(translation.equipement.required, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			return;
		}

		// récuperer la liste des operations à partir du composant "gamme-maintenance-equipement"
		const operations = this.emitter.getOperations;

		// vérifier si la listes des operation n'est pas vide
		if (operations.length === 0) {
			toastr.warning(translation.equipement.perationsRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			return;
		}

		// changer l'onglet
		this.selectedGammeMaintenanceEquipement = index;
	}

	removeEquipement(index) {
		this.translate.get('labels.removeEquipement').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true,
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true,
					},
				},
			}).then(isConfirm => {
				if (isConfirm) {

					this.gamme_maintenance_equipement_Selected_libelles.splice(index, 1);
					this.gamme_maintenance_equipement_Selected.splice(index, 1);
					this.selectedGammeMaintenanceEquipement = 0;
					this.changeTab(0);
					swal(text.success, '', 'success');
				} else {
					toastr.success(text.failed, text.title, {
						positionClass: 'toast-top-center',
						containerId: 'toast-top-center',
					});
				}
			});
		});
	}

	selectMaterial(e, gamme) {
		const gam = this.materials.find(x => x.id === e.value);
		gamme.idMaterial = gam.id;
		gamme.brand = gam.brand;
		gamme.model = gam.model;
		gamme.serialNumber = gam.serialNumber;
	}

	async saveNewAddress(adresses: Adresse[]) {
		const idClient = this.creationForm.value.idClient;
		const client = await this.getClient(idClient)
		if (idClient != null) {
			const oldAdresses: Adresse[] = client.addresses;
			const index = adresses.length - 1;
			const newAdresse = adresses[index];
			newAdresse.isDefault = false;
			oldAdresses.unshift(newAdresse);
			client.addresses = oldAdresses;
			this.service.update(ApiUrl.Client, client, idClient.id).subscribe(res => {
				if (res) {
					this.translate.get('adresse').subscribe(text => {
						toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						this.adresses = res.value.addresses;
						this.creationForm.patchValue({
							site: newAdresse,
						});
					});
				}
			}, err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			});
		}
	}

	getColor(status) { return this.color[status === 'finished' ? 'late' : status] };

	/**
	 * get the file from the user device
	 */
	startUpload(event: FileList): void {
		this.processing = true;
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new Attachement()
			pieceJoin.fileId = AppSettings.guid()
			pieceJoin.fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString();
			this.piecesJointes.push(pieceJoin);
		}
		this.processing = false;
	}

	downloadPieceJointe(index: number) {
		const pieceJointe = this.piecesJointes[index];
		AppSettings.downloadBase64(pieceJointe.content, pieceJointe.fileName,
			pieceJointe.content.substring('data:'.length, pieceJointe.content.indexOf(';base64')), pieceJointe.fileType)
	}

	removeFile(index: number) {
		this.piecesJointes.splice(index, 1);
	}

	selectTypeRecurrent(event) {
		if (event === this.periodicityType.Custom) {
			this.LoadPersonaliseReccurente();
		}
	}

	LoadPersonaliseReccurente() {

		let dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '500px';
		dialogLotConfig.data = { listes: this.result, readOnly: false };

		const dialogRef = this.dialog.open(PersonaliseReccurenteComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
			this.dataDialog = data;
		});
	}

	getPeriodiciTyOptions() {
		let res;
		if (this.dataDialog !== null) {
			res = {
				daysOfWeek: this.dataDialog.jourSemaine,
				dayOfMonth: this.dataDialog.jour !== undefined ? this.dataDialog.jour : 0,
				recurringType: this.dataDialog.typeRepetition,
				repeatEvery: this.dataDialog.nbrRepetition
			};
		} else {
			res = {
				daysOfWeek: this.recurrente !== undefined && this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.daysOfWeek : [],
				dayOfMonth: this.recurrente !== undefined && this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.dayOfMonth : 0,
				recurringType: this.recurrente !== undefined && this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.recurringType : 0,
				repeatEvery: this.recurrente !== undefined && this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.repeatEvery : 0
			};
		}
		return res;
	}

	//#region add
	async createBodyRequestFacture(status?) {
		const formValue = this.creationForm.value;
		const reccurent = {
			periodicityOptions: this.getPeriodiciTyOptions(),
			periodicityType: formValue.nbr_repetition,
			endingType: formValue.type_termine,
			occurringCount: formValue.nbr_occurence,
			endingDate: formValue.date_fin !== null && formValue.type_termine === this.periodicityEndingType.SpecificDate
				? AppSettings.formaterDatetime(this.formatDate(formValue.date_fin)) : null,
			startingDate: AppSettings.formaterDatetime(this.formatDate(formValue.date_validation)),
			status: status === this.statutContratEntretien.Brouillon ? this.statutContratEntretien.Brouillon : this.statutContratEntretien.Encours,
			documentType: TypeDocument.Invoice,
			document: await this.generateInvoice(status)
		}
		return reccurent;
	}

	getNameClient(id) {
		const res = this.listClients.find(client => client.id === id);
		return res.firstName + ' ' + res.lastName;
	}

	async generateInvoice(status) {
		const formValue = this.creationForm.value;
		const facture: Invoice = new Invoice();
		facture.reference = '';
		facture.workshopId = null;
		facture.clientId = formValue.idClient.id;
		facture['client'] = formValue.idClient;
		facture['clientName'] = this.getNameClient(formValue.idClient.id);
		facture.status = status;
		facture.typeInvoice = TypeFacture.None;
		facture.creationDate = AppSettings.formaterDatetime(formValue.dateDebut);
		facture.dueDate = AppSettings.formaterDatetime(formValue.dateFin);
		facture.purpose = '';
		facture.note = '';
		facture.paymentCondition = '';
		const dataArticles = await this.getDataFromArticlesComponet();
		const orderDetails: OrderDetails = {
			'globalDiscount': {
				'value': dataArticles.remise,
				'type': dataArticles.typeRemise
			},
			'holdbackDetails': {
				'warrantyPeriod': dataArticles.delaiGarantie,
				'holdback': dataArticles.retenueGarantie,
				'warrantyExpirationDate': new Date(),
			},
			'globalVAT_Value': dataArticles.tvaGlobal,
			'puc': dataArticles.puc,
			'proportion': dataArticles.prorata,
			'productsDetailsType': OrderProductsDetailsType.List,
			'lineItems': dataArticles.prestation,
			'productsPricingDetails': {
				'totalHours': 0,
				'salesPrice': 0
			},
			'totalHT': dataArticles.totalHt,
			'totalTTC': dataArticles.totalTtc
		};
		facture.operationSheetsIds = [];
		facture.orderDetails = orderDetails;
		facture.quoteId = null;
		facture.lotDetails = false;
		facture.OperationSheetMaintenanceId = null;
		facture.ContractId = null;
		let adresseIntervention = formValue.site;
		if (adresseIntervention === null || adresseIntervention.designation === undefined) {
			adresseIntervention = this.adresses[+adresseIntervention];
		} else {
			adresseIntervention = formValue.adresseIntervention;
		}
		facture.addressIntervention = adresseIntervention;
		return facture;
	}


	formatDate(date) {
		// tslint:disable-next-line:prefer-const
		let d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			// tslint:disable-next-line:prefer-const
			year = d.getFullYear();

		if (month.length < 2) { month = '0' + month; }
		if (day.length < 2) { day = '0' + day; }
		return [year, month, day].join('/');
	}

	getValid(res) {
		if (res !== null && res !== undefined) {
			return true
		}
		return false;
	}

	setDataFactureRecurrent(reccurente) {

		this.creationForm.controls['nbr_repetition'].setValue(this.recurrente.periodicityType);
		this.creationForm.controls['date_validation'].setValue(this.recurrente.startingDate !== null ? new Date(this.recurrente.startingDate) : null);
		this.creationForm.controls['type_termine'].setValue(this.recurrente.endingType);
		this.creationForm.controls['date_fin'].setValue(this.recurrente.endingDate !== null ? new Date(this.recurrente.endingDate) : null);
		this.creationForm.controls['nbr_occurence'].setValue(this.recurrente.occurringCount);

		const res: ReccurenteModel = new ReccurenteModel();
		if (this.recurrente.periodicityType === this.periodicityType.Custom) {
			res.typeRepetition = this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.recurringType : null;
			res.nbrRepetition = this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.repeatEvery : null;
			res.jour = this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.dayOfMonth : null;
			res.jourSemaine = this.getValid(this.recurrente.periodicityOptions) ? this.recurrente.periodicityOptions.daysOfWeek : null;
			this.result = res;
		} else {
			this.result = null;
		}
	}

	//#endregion

	//#region reference
	generateReference(): Promise<string> {
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.configReference + this.typeNumerotation.MaintenanceContract + '/reference').subscribe(
				res => {
					reslove(res);
					this.creationForm.controls['reference'].setValue(res);
				},
				err => { console.log(err); }
			);
		});
	}

	CheckUniqueReference(control: FormControl) {
		const promise = new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.MaintenanceContrat + ApiUrl.CheckReference, control.value).subscribe(res => {
				if (!res && this.isModif() && this.creationForm.value.reference !== this.document.reference) {
					resolve({ CheckUniqueReference: true });
				} else if (!res && !this.isModif()) {
					resolve({ CheckUniqueReference: true });
				} else {
					resolve(null);
				}
			});
		});
		return promise;
	}

	getOperation(result): OperationMaintenance[] {
		result.forEach((element, index) => {
			result[index].periodicity = this.filterPeriodicity(element.periodicity);
			result[index].subOperations.forEach((ele, i) => {
				result[index].subOperations[i].periodicity = this.filterPeriodicity(ele.periodicity);
			});
		});
		return result;
	}



	filterPeriodicity(periodicity) {
		if (periodicity !== undefined) {
			return periodicity.filter(x => x.value === true).map(y => y.mois);
		}
		return [];
	}

	formaterPeriodiciet(result) {
		result.forEach((elem, index) => {
			result[index].periodicity = result[index].periodicity.length !== 0 ? this.getPeriodicity(elem.periodicity) : [];
			result[index].subOperations.forEach((ele, i) => {
				result[index].subOperations[i].periodicity = result[index].subOperations[i].periodicity.length !== 0 ?
					this.getPeriodicity(ele.periodicity) : [];
			});
		});
		return result;
	}

	getPeriodicity(periode) {
		const periodicite = [];
		for (let i = 1; i <= 12; i++) {
			periodicite.push({ mois: i, value: periode.includes(i) })
		}
		return periodicite;
	}

	getDataFromArticlesComponet(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitterArticle.getDateToSave((res) => {
				resolve(res);
			})
		})
	}

	filterclient(e) {
		if (e !== '') {
			this.listClients = [];
			this.listClients = this.clients.filter(x => x.name.includes(e) || x.reference.includes(e));
		} else {
			this.listClients = this.clients;
		}

	}

	filtermaterial(e) {
		if (e !== '') {
			this.listMaterials = [];
			this.listMaterials = this.materials.filter(x => x.designation.includes(e));
		} else {
			this.listMaterials = this.materials;
		}

	}



	toast(msg) {
		return toastr.success(this.translate.instant('toast.' + msg), '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}

	close() {
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	submit(status?) {

		if (EditDocumentHelper.isAction(Action.AJOUTER) || EditDocumentHelper.isAction(Action.DUPLIQUER)) {
			this.add(status);
		}

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			this.update(status);
		}

	}

	async update(statut) {
		this.processing = true;
		const compareDate = this.creationForm.value.dateFin == null || this.creationForm.value.dateFin == null ?
			true : AppSettings.compareDate(this.creationForm.value.dateFin, this.creationForm.value.dateDebut)
		if (!compareDate) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.datesIntervention, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}
		let res = null;
		this.gamme_maintenance_equipement_Selected.forEach((elem, index) => {
			const ele = elem.maintenanceOperations.filter(e => e.periodicity.length === 0 && e.subOperations.length === 0);
			res += ele.length;
		});
		if (res > 0) {
			toastr.warning('Veuillez sélectionner au moin une periode', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		if (this.gamme_maintenance_equipement_Selected === undefined || this.gamme_maintenance_equipement_Selected.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.fillAllGammeMaintenance, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		this.dataArticles = await this.getDataFromArticlesComponet();

		if (this.dataArticles.prestation.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}

		// if (!this.creationForm.valid) {
		// 	const translation = await this.getTranslationByKey('errors');
		// 	toastr.warning(translation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	this.processing = false;
		// 	return;
		// }

		const createBody = await this.createBodyRequest(statut);

		const reccurence = await this.createBodyRequestFacture(statut);



		if (this.recurrente) {
			await this.service.update(ApiUrl.Recurring, reccurence, this.recurrente.id).subscribe(async res => {
				if (res.isSuccess) {
					createBody['RecurringDocumentId'] = res.value.id;
					await this.updateContract(createBody);
				}
			}, async err => {
				console.log(err);
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.processing = false;
			});
		} else {

			await this.service.create(ApiUrl.Recurring + ACTION_API.create, reccurence).subscribe(async res => {
				if (res.isSuccess) {
					createBody['RecurringDocumentId'] = res.value.id;
					await this.createContract(createBody);
				}
			}, async err => {
				console.log(err);
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.processing = false;
			});
		}
	}

	async updateContract(createBody) {
		await this.service.update(ApiUrl.MaintenanceContrat, createBody, this.id).subscribe(async res => {
			if (res) {
				const translation = await this.getTranslationByKey('update');
				toastr.success(translation.msg, translation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.router.navigate(['/contratentretiens/detail', this.document.id]);
			}
		}, async err => {
			console.log(err);
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}, () => {
			this.processing = false;
		})
	}

	//#endregion

}
