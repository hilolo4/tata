import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { PublishingContract } from '../../../../../Models/Entities/publishing-contract/publishing-contract';
import { IGenericRepository } from '../../../../../shared/repository/igeneric-repository';
import { ApiUrl } from '../../../../../Enums/Configuration/api-url.enum';
import { SortDirection } from '../../../../../Models/Model/filter-option';
declare var toastr: any;

@Component({
	selector: 'app-select-contract',
	templateUrl: './select-contract.component.html',
	styleUrls: ['./select-contact.component.scss']
})

export class SelectContractComponent implements OnInit {

	title = 'select-contract.title';
	searchControl = new FormControl();
	contracts: PublishingContract[] = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public modal: NgbActiveModal,
		private translate: TranslateService) { }

	ngOnInit() {
		this.getAllContract('');
	}

	getAllContract(searchQuery: string) {
		this.service.getAllPagination(ApiUrl.Publishing, {
			SearchQuery: '',
			OrderBy: 'title',
			Page: 1,
			SortDirection: SortDirection.Ascending,
			PageSize: 10
		}).subscribe(result => {
			if (result.isSuccess) {
				this.contracts = result.value;
			} else {
				toastr.error(this.translate.instant('errors.server'));
			}
		})
	}


	save() {
		const contractSelected = this.contracts.find(e => e['checked'] === true);
		if (contractSelected != null) {
			this.modal.close(contractSelected);
		} else {
			toastr.error(this.translate.instant('errors.select-contract'));
		}
	}

	changeChecked(contractIndex: number) {
		this.contracts.forEach((item, index) => {
			if (index !== contractIndex) {
				item['checked'] = false;
			}
		})
	}

}
