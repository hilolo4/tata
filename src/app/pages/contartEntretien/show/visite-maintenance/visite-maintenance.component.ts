import { Component, OnInit, Input, OnChanges, Output, EventEmitter, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
// import { VisiteMaintenance, VisiteMaintenanceViewModel } from 'app/Models/Entities/VisiteMaintenance';
import * as _ from 'lodash';
import { StatutVisiteMaintenance } from 'app/Enums/Statut/StatutVisiteMaintenance';
import { MenuItem } from 'app/custom-module/primeng/api';
// import { VisiteMaintenanceService } from 'app/services/visisteMaintenance/visite-maintenance.service';
// import { FicheInterventionMaintenance } from 'app/Models/Entities/FicheInterventionMaintenance';
// import { GammeVisiteMaintenanceEquipementModel } from 'app/Models/GammeVisiteMaintenanceEquipementModel';
// import { FicheInterventionMaintenanceService } from 'app/services/ficheInterventionMaintenance/fiche-intervention-maintenance.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CreateFicheInterventionMaintenance } from 'app/Enums/CreateFicheInterventionMaintenance.Enum';
import { ContratEntretien, VisiteMaintenance, VisiteMaintenanceViewModel } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { moisEnum } from 'app/Enums/Maintenance/mois.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
declare var toastr: any;

declare var jQuery: any;
declare var swal: any;
@Component({
	selector: 'app-visite-maintenance',
	templateUrl: './visite-maintenance.component.html',
	styleUrls: ['./visite-maintenance.component.scss']
})
export class VisiteMaintenanceComponent implements OnInit, OnChanges {
	// tslint:disable-next-line:no-output-rename
	@Output('OnRefresh') refresh = new EventEmitter();
	// tslint:disable-next-line:no-input-rename
	@Input('visiteMaintenance') visiteMaintenance: VisiteMaintenance;

	visitesMaintenances: VisiteMaintenanceViewModel[] = [];
	statutVisiteMaintenance: typeof StatutVisiteMaintenance = StatutVisiteMaintenance;
	moisEnum: typeof moisEnum = moisEnum;
	labels: any = null;
	actionsItems: MenuItem[] = [];
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private router: Router,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {

		this.translate.get('labels').subscribe(text => {
			this.labels = text;
		});
	}
	ngOnChanges() {
		if (this.visiteMaintenance !== undefined) {
			const states: VisiteMaintenanceViewModel = _.groupBy(this.visiteMaintenance, 'year')
			this.visitesMaintenances = [];
			for (const key in states) {
				this.visitesMaintenances.push({ year: +key, visitesMaintenances: states[key] });
			}
		}
	}

	voirDetails(visitesMaintenances: VisiteMaintenance[], mois: number) {
		if (visitesMaintenances === undefined) {
			return true;
		}
		this.navigateToDetailVisiteMaintenance(visitesMaintenances[0]);
	}


	getTransalteLocationRequest(statut: StatutVisiteMaintenance): string {
		const StatusLabel: string = this.statutVisiteMaintenance[statut].toLowerCase();
		return `changeStatutVisiteMaintenance.${StatusLabel}`;
	}

	getStatutByMonth(visitesMaintenances: VisiteMaintenance[], mois: number) {

		const visitesMaintenance = this.getVisiteMaintenance(visitesMaintenances, mois);
		if (visitesMaintenance != null) {
			return this.getLabelleByStatut(visitesMaintenance.status);
		} else {
			return null;
		}

	}

	getFicheInterventionByMonth(visitesMaintenances: VisiteMaintenance[], mois: number) {
		const visitesMaintenance = this.getVisiteMaintenance(visitesMaintenances, mois);
		if (visitesMaintenance != null) {
			return visitesMaintenance.maintenanceOperationSheetReference == null ? null : visitesMaintenance.maintenanceOperationSheetReference;
		}
		else {
			return null;
		}
	}

	getFactureByMonth(years, mois: number) {
		// if (this.contratEntretien.factureReccurent.facture.length > 0) {
		// 	const facture = this.contratEntretien.factureReccurent.facture.find(x => new Date(x.dateCreation).getFullYear() === years && new Date(x.dateCreation).getMonth() === mois)
		// 	return facture.reference;
		// } else {
		// 	return null;
		// }
	}
	conditionsDropdownMenu(visitesMaintenances: VisiteMaintenance[], mois: number) {
		const visitesMaintenance = this.getVisiteMaintenance(visitesMaintenances, mois);
		if (visitesMaintenance != null) {
			return this.getConditionByStatut(visitesMaintenance.status);
		} else {
			return null;
		}
	}
	getConditionByStatut(statut) {
		switch (statut) {
			case this.statutVisiteMaintenance.Unplanned:
				return true;
				break;
			case this.statutVisiteMaintenance.Planned:
				return false;
				break;
		}
	}
	getLabelleByStatut(statut) {

		if (this.labels == null) {
			return
		}
		switch (statut) {
			case this.statutVisiteMaintenance.Unplanned:
				return this.labels.aplanifier;
				break;
			case this.statutVisiteMaintenance.Planned:
				return this.labels.planifier;
				break;
		}
	}
	getVisiteMaintenance(visitesMaintenances: VisiteMaintenance[], mois: number) {
		const visiteMaintenance = visitesMaintenances.filter(x => x.month === mois);
		return visiteMaintenance.length === 0 ? null : visiteMaintenance[0];
	}

	navigateToDetailVisiteMaintenance(visite) {
		if (visite.status == StatutVisiteMaintenance.Unplanned && visite.contractId != undefined) {
			this.router.navigate([`/contratentretiens/detail/${visite.contractId}/visitemaintenance/${visite.contractId}/${visite.year}/${visite.month}`]);
		}
		if (visite.status == StatutVisiteMaintenance.Planned) {
			this.router.navigate([`/contratentretiens/detail/${visite.contractId}/visitemaintenance/${visite.id}`]);
		}
	}

}
