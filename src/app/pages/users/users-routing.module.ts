import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';

const routes: Routes = [{
	path: '',
	children: [
		{
			path: '',
			pathMatch: 'full',
			loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			data: {
				title: 'Utilisateur'
				}
		},
		{
			path: 'create',
			component: AddComponent,
			data: {
				title: 'Ajouter Utilisateur'
			}
		},
		{
			path: 'edit/:id',
			component: AddComponent,
		},
		{
			path: 'detail/:id',
			component: ShowComponent,
			data: {
				title: 'Détails Utilisateur'
			}
		}
	]
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class UtilisateurRoutingModule { }
