import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
	path: '',
	children: [
		{
			path: '',
			pathMatch: 'full',
			loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			data: {
					title: 'Lots'
			}
		}
	]
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LotsRoutingModule { }
