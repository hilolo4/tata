import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametragePdfComponent } from './parametrage-pdf.component';

describe('ParametragePdfComponent', () => {
  let component: ParametragePdfComponent;
  let fixture: ComponentFixture<ParametragePdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametragePdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametragePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
