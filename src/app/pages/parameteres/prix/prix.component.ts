import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DefaultValue } from 'app/Models/Entities/Parametres/DefaultValue';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
declare var toastr: any;

@Component({
	selector: 'app-numerotation-prefixe',
	templateUrl: './prix.component.html',
	styleUrls: ['./prix.component.scss']
})
export class PrixComponent implements OnInit {

	form: any;
	loading;
	data: DefaultValue;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private header: HeaderService,
		private fb: FormBuilder) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.form = this.fb.group({
			salesPrice: [''],
			buyingPrice: [''],
			cartCost: [''],
			displacementCost: [''],
			defaultVAT: ['']
		});

	}
	prepareBreadcrumb(): void {
		this.translate.get(`parametragePrix.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('parametragePrix.title'), route: '/parameteres/prix' }
			];
		});
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetParametragePrix();
	}



	GetParametragePrix() {
		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			this.data = JSON.parse(res);
			this.form.patchValue(this.data);
		});
	}
	update() {
		if (this.form.valid) {
			this.data.salesPrice = this.form.controls['salesPrice'].value;
			this.data.buyingPrice = this.form.controls['buyingPrice'].value;
			this.data.cartCost = this.form.controls['cartCost'].value;
			this.data.displacementCost = this.form.controls['displacementCost'].value;
			this.data.defaultVAT = this.form.controls['defaultVAT'].value;
			this.service.updateAll(ApiUrl.configDefaultValue, { 'value': JSON.stringify(this.data) }).subscribe(res => {
				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.GetParametragePrix();
			})
		}
	}
}