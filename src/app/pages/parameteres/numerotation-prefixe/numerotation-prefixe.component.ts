import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { FormBuilder, Validators } from '@angular/forms';
import { Numerotation } from 'app/Models/Entities/Parametres/Numerotation';
import { FormatDateParametrage } from 'app/Enums/FormatDateParametrage.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Constants } from 'app/shared/utils/constants';
import { NumerotationAutomatique } from 'app/shared/utils/NumerotationAutomatique';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
declare var toastr: any;
declare var jQuery: any;


@Component({
	selector: 'app-numerotation-prefixe',
	templateUrl: './numerotation-prefixe.component.html',
	styleUrls: ['./numerotation-prefixe.component.scss']
})
export class NumerotationPrefixeComponent implements OnInit {

	form;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	formatDateParametrage: typeof FormatDateParametrage = FormatDateParametrage
	numPrefixClientFournisseur;
	numPrefixDocument;
	modelName = '';
	numerotationSelected: Numerotation
	loading = false;
	numerotation: Numerotation[];
	panelOpenState = false;
	blockExpansion = true;

	isExpansionDisabled(): string {
		if (this.blockExpansion) {
			return 'disabled-pointer';
		}
		return '';
	}

	expansionEnable() {
		this.blockExpansion = !this.blockExpansion;
	}
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private header: HeaderService,
		private fb: FormBuilder) {
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	ngOnInit() {
		// this.translate.setDefaultLang(AppSettings.lang);
		// this.translate.use(AppSettings.lang);
		this.InitData();
		this.prepareBreadcrumb();

	}
	prepareBreadcrumb(): void {
		this.translate.get(`numerotationPrefixe.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('numerotationPrefixe.title'), route: '/parameteres/numerotationPrefixe' }
			];
		});
	}
	InitData() {
		this.loading = true;
		this.numPrefixClientFournisseur = [];
		this.numPrefixDocument = [];
		this.getCounter();
	}

	getNumPrefix(counter) {
		return {
			type: counter.documentType,
			name: 'labels.' + TypeNumerotation[counter.documentType],
			code: NumerotationAutomatique.composeNumbering(counter),
			counter: counter
		};
	}

	getCounter() {
		this.service.getAll(ApiUrl.configurationCounter)
			.subscribe(
				async response => {
					if (response) {
						this.numerotation = Constants.deserializeJSON(response);
						this.numerotation.forEach((counter: Numerotation) => {
							if (counter.documentType === this.typeNumerotation.Client ||
								counter.documentType === this.typeNumerotation.fournisseur || counter.documentType === this.typeNumerotation.agent) {
								this.numPrefixClientFournisseur.push(this.getNumPrefix(counter));
							} else {
								this.numPrefixDocument.push(this.getNumPrefix(counter));
							}
						});
					}
				});
	}

	selectNumerotationtoUpdate(i, type) {

		const item = (type === this.typeNumerotation.Client || type ===
			this.typeNumerotation.fournisseur || type === this.typeNumerotation.agent) ?
			this.numPrefixClientFournisseur[i] : this.numPrefixDocument[i];

		this.form = this.fb.group({
			'racine': ['', [Validators.required]],
			'compteur': ['', [Validators.required]],
			'formatDate': ['', [Validators.required]],
			'longeurCompteur': ['', [Validators.required]]
		})

		this.setData(item);
	}

	setData(item) {
		this.modelName = this.translate.instant(item.name) + (item.code ? ' : ' + item.code : '');
		this.numerotationSelected = item.counter;
		this.form.controls['racine'].setValue(this.numerotationSelected.prefix);
		this.form.controls['compteur'].setValue(this.numerotationSelected.counter);
		this.form.controls['longeurCompteur'].setValue(this.numerotationSelected.counterLength);
		this.form.controls['formatDate'].setValue(this.numerotationSelected.dateFormat);
	}

	update() {


		if (this.form.valid) {
			this.translate.get('labels').subscribe(text => {
				const values = this.form.value;
				this.numerotation.forEach((num, index) => {
					if (num.documentType === this.numerotationSelected.documentType) {
						this.numerotation[index].documentType = this.numerotationSelected.documentType;
						this.numerotation[index].dateFormat = values['formatDate'];
						this.numerotation[index].counter = values['compteur'];
						this.numerotation[index].prefix = values['racine'];
						this.numerotation[index].counterLength = values['longeurCompteur'];
					}
				});

				this.service.updateAll(ApiUrl.configurationCounter, { 'value': JSON.stringify(this.numerotation) }).subscribe(
					res => {
						toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						jQuery('#updateParamter').modal('hide');
						this.InitData();
					}, err => {
						toastr.error('', text.error, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					}
				)
			});
		}
	}

	GenerateExampleCode(): string {
		const compteur: string = this.form.get('compteur').value;
		const longeurCompteur: string = this.form.get('longeurCompteur').value;
		const racine: string = this.form.get('racine').value;
		const formatDateValue: any = this.form.get('formatDate') == null ? '' : this.form.get('formatDate').value;
		const formatDate: string = NumerotationAutomatique.getFormatDate(formatDateValue);
		const numberZeros: string = NumerotationAutomatique.calculateNumberOfComplementaryZeros(longeurCompteur, compteur);
		return racine + formatDate + numberZeros + compteur;
	}

	sortList(arr: any) {
		arr.sort((obj1, obj2) => {
			if (obj1.name > obj2.name) {
				return 1;
			}

			if (obj1.name < obj2.name) {
				return -1;
			}

			return 0;
		});
	}

}
