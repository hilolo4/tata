import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-parametrage-document',
	templateUrl: './parametrage-document.component.html',
	styleUrls: ['./parametrage-document.component.scss']
})

export class ParametrageDocumentComponent implements OnInit {

	form: any;
	loading;
	data;
	editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '8rem',
		translate: 'yes',
	};


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private header: HeaderService,
		private translate: TranslateService) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetParametrageDocument();
	}
	prepareBreadcrumb(): void {
		this.translate.get(`labels.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('labels.parametragedocument'), route: '/parameteres/parametrageDocument' }
			];
		});
	}
	GetParametrageDocument() {

		this.service.getAll(ApiUrl.configDocument).subscribe(res => {
			this.data = JSON.parse(res);
		});
	}

	getData(evenet) {
		this.data = evenet;
	}

	update() {
		this.service.updateAll(ApiUrl.configDocument, { value: JSON.stringify(this.data) }).subscribe(res => {
			this.translate.get('labels').subscribe(text => {
				toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.GetParametrageDocument();
		})
	}

}
