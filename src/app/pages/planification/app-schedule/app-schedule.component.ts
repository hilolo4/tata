import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { EventSettingsModel, GroupModel, ResourceDetails, ActionEventArgs, CellClickEventArgs, EventRenderedArgs } from '@syncfusion/ej2-schedule';
import { L10n, loadCldr, setCulture, addClass, remove, closest } from '@syncfusion/ej2-base';
import { ScheduleComponent, PopupOpenEventArgs, TimeScaleModel, View } from '@syncfusion/ej2-angular-schedule';
import { DragAndDropEventArgs } from '@syncfusion/ej2-navigations';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { AppSettings } from 'app/app-settings/app-settings';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { TranslateService } from '@ngx-translate/core';
import { DateTimePicker } from '@syncfusion/ej2-calendars';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { User } from 'app/Models/Entities/User';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { moisEnum } from 'app/Enums/Maintenance/mois.enum';
import { FicheInterventionMaintenance } from 'app/Models/Entities/Maintenance/FicheInterventionMaintenance';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';
import { VisiteMaintenance } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { StatutVisiteMaintenance } from 'app/Enums/Statut/StatutVisiteMaintenance';
import * as _ from 'lodash';
declare var toastr: any;
declare var swal: any;
import 'leaflet';
import { ShowVisiteComponent } from '../show-visite/show-visite.component';
import { LocalElements } from 'app/shared/utils/local-elements';
import { StatutFicheInterventionMaintenance } from 'app/Enums/Statut/StatutFicheInterventionMain.enum';
declare let require: Function;
setCulture('fr');
@Component({
  selector: 'app-app-schedule',
  templateUrl: './app-schedule.component.html',
  styleUrls: [
    './app-schedule.component.scss',
    './../../../../assets/scss/components/agenda.scss'
  ]
})
export class AppScheduleComponent implements OnInit {
  @ViewChild('scheduleObj') scheduleObj: ScheduleComponent;
  @ViewChild('treeObj') treeObj: TreeViewComponent;
  listeLisiteMaintenance: VisiteMaintenance[] = [];

  public currentView: View = 'Month';

  isTreeItemDropped = false;
  allowMultiple = false;
  allowDragAndDrop = true;
  field: Object;
  draggedItemId = '';
  selectedDate: Date = new Date();
  // public workWeekDays: number[] = [1, 2, 3, 4, 5, 0, 6];// is manday to sanday

  public workWeekDays: number[] = [1, 2, 3, 4, 5, 0, 6]; // is manday to sanday
  public showWeekend: Boolean = true;
  public showWeekNumber = true;
  public startWeek = 1; // start week day is monday
  //public eventSettings: EventSettingsModel = { dataSource: [] };
  techniciensDataSource: Object[] = [{ Text: 'Techniciens', Id: UserProfile.technicien, Color: '#b35676' }];
  listeTechniciens: TechniciensInterface[] = [];
  group: GroupModel = { enableCompactView: false, resources: ['Departments', 'Consultants'] };
  listeClient: Client[] = [];
  IdTechnicien = null;
  dataSourceLength = 0;
  newInterventionData = null;
  ficheInterventionMaintenanceList;
  idClient: [] = [];
  data: object[] = [];
  idClientFicheIntervention = null;
  statut = null;
  listMois: any = null;
  cellData: any = null;
  mois: [] = [];
  technicienId: [] = [];
  listStatus: any = [];
  statutFicheIntervention: typeof StatutFicheIntervention = StatutFicheIntervention;
  currentViewDates: Date = new Date();
  public text = 'Sélectionnez un élément';
  public timeScaleOptions: TimeScaleModel = { enable: false };
  visiteMaintenanceSelected: any = null;
  timeToggled: boolean = true;
  today: Date = new Date();

  listTechniciensFilter = [];
  filterlistTechnicien: ngSelectModel[] = [];
  userProfile: typeof UserProfile = UserProfile;
  dateDebutAgenda: any;
  DateFinAgenda: any;
  listTechniciens: User[] = [];
  listTechnicien = [];
  role = [];
  filter;
  page = 1;
  totalPage = 1;
  techniciens = [];
  search = '';
  listRole = [];
  listMoisSelected: any
  visiteMaintenanaceFilter: VisiteMaintenanaceFilterInterface = {
    listeAnnee: [2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026,
      2028, 2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039
      , 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049, 2050,
    ],
    listeClient: [],
    idClient: null,
    annee: null,
  };
  annee = null;
  filterlistClient: ngSelectModel[] = [];

  filterVisiteMaintenance
  public eventSettings: EventSettingsModel = { dataSource: [] };
  dataAgenda
  isFilterVisite = false;
  isFilterFicheIntervention = false;
  constructor(
    @Inject('IGenericRepository') private service: IGenericRepository<any>,
    private router: Router,
    private translate: TranslateService,
    private dialog: MatDialog,

  ) {
    this.translate.setDefaultLang(AppSettings.lang);
    this.translate.use(AppSettings.lang);
  }

  async ngOnInit() {
    this.traductionCalendarToFranch();
    await this.listStatut();
    this.getlistMois();
    this.getClients();
    await this.getRole();
    this.getTechniciens();
    await this.getVisiteMaintenance();

    // 	await this.getFicheInterventionChantierList();
    await this.getFicheInterventionMaintenanceList();
  }
  filtersOpened = false;
  onFiltersTriggered(): void {
    this.filtersOpened = !this.filtersOpened;
  }
  filtersOpenedFiche = false;
  onFiltersTriggeredFiche(): void {
    this.filtersOpenedFiche = !this.filtersOpenedFiche;
  }

  onBackdropClickedFiche() {
    this.idClientFicheIntervention = null;
    this.statut = null;
    this.refreshFicheIntervention();
  }
  onBackdropClicked() {
    this.idClient = [];
    this.mois = [];
    this.annee = null;
    this.getVisiteMaintenance();
  }
  getPlacholderClient() {
    const text = this.translate.instant('labels.client')
    return text;
  }
  getPlacholderMois() {
    const text = this.translate.instant('labels.mois')
    return text;
  }
  getPlacholderStatus() {
    const text = this.translate.instant('labels.status')
    return text;
  }

  getPlacholderAnnee() {
    const text = this.translate.instant('labels.annee')
    return text;
  }
  onValidate() {

  }
  getViewDate = () => this.scheduleObj && this.scheduleObj.selectedDate ? this.scheduleObj.selectedDate : '';
  getCurrentDate = () => new Date();
  getDateFormat = () => this.scheduleObj ? (this.scheduleObj.currentView === 'Month' ? 'MMMM yyyy' : 'dd MMMM yyyy') : '';

  onTechnicienAll(): void {
    this.technicienId = this.filterlistTechnicien.slice(0).map(e => e.id) as any;
    this.refreshFicheIntervention();
  }

  onTechnicienReset(e: MouseEvent): void {
    e.stopPropagation();
    this.technicienId = [];
    this.refreshFicheIntervention();
  }

  onAgendaPrevious(): void {
    if (this.scheduleObj && this.scheduleObj.element) {
      const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-prev button') as any;

      if (btn) {
        btn.click();
      }
    }
  }

  onAgendaNext(): void {
    if (this.scheduleObj && this.scheduleObj.element) {
      const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-next button') as any;

      if (btn) {
        btn.click();
      }
    }
  }

  onAgendaToday(): void {
    if (this.scheduleObj && this.scheduleObj.element) {
      const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-today button') as any;

      if (btn) {
        btn.click();
      }
    }
  }

  onAgendaDate(e: any): void {
    if (this.scheduleObj && e && e.value) {
      this.scheduleObj.selectedDate = e.value;
    }
  }

  onDisplayChanged(e: number): void {
    const keys = ['e-month', 'e-timeline-day', 'e-timeline-work-week'];

    if (this.scheduleObj && this.scheduleObj.element) {
      const btn = this.scheduleObj.element.querySelector(`.e-toolbar-item.e-views.${keys[e]} button`) as any;
      if (btn) {
        btn.click();
      }
    }
    this.group = { enableCompactView: false, resources: ['Departments', 'Consultants'] };
  }
  isMonthly = () => this.scheduleObj && this.scheduleObj.viewIndex === 2;


  async listStatut() {
    const transaltions = await this.getTranslationByKey('labels');
    this.listStatus.push({ value: this.statutFicheIntervention.Brouillon, name: transaltions.brouillon });
    this.listStatus.push({ value: this.statutFicheIntervention.Planifiee, name: transaltions.planifiee });
    this.listStatus.push({ value: this.statutFicheIntervention.Realisee, name: transaltions.realisee });
    this.listStatus.push({ value: this.statutFicheIntervention.Facturee, name: transaltions.facturee });
    this.listStatus.push({ value: this.statutFicheIntervention.Annulee, name: transaltions.annulee });
    this.listStatus.push({ value: this.statutFicheIntervention.Late, name: transaltions.late });
  }
  /**
   *  list Mois
   *  */
  async getlistMois() {

    if (this.listMois == null) {
      const transaltions = await this.getTranslationByKey('mois');
      this.listMois = [];
      this.listMois.push({ value: moisEnum.January, name: transaltions.janvier });
      this.listMois.push({ value: moisEnum.February, name: transaltions.fevrier });
      this.listMois.push({ value: moisEnum.March, name: transaltions.mars });
      this.listMois.push({ value: moisEnum.April, name: transaltions.avril });
      this.listMois.push({ value: moisEnum.May, name: transaltions.mai });
      this.listMois.push({ value: moisEnum.June, name: transaltions.juin });
      this.listMois.push({ value: moisEnum.July, name: transaltions.juillet });
      this.listMois.push({ value: moisEnum.August, name: transaltions.aout });
      this.listMois.push({ value: moisEnum.September, name: transaltions.septembre });
      this.listMois.push({ value: moisEnum.October, name: transaltions.octobre });
      this.listMois.push({ value: moisEnum.November, name: transaltions.novembre });
      this.listMois.push({ value: moisEnum.December, name: transaltions.decembre });
      // this.selecteMois.isOpen = true;
      this.listMoisSelected = this.formaterListMois(this.listMois);
    }
  }
  /**
   * format list mois for ng select multiple
   * @param list mois
   */
  formaterListMois(list): ngSelectModel[] {
    const listMois: ngSelectModel[] = [];
    list.forEach((element, index) => {
      listMois.push({ id: element.value, name: element.name, disabled: false });
    });
    return listMois;
  }

  getClients() {
    this.service.getAll(ApiUrl.Client).subscribe(res => {
      this.listeClient = res.value as Client[];
      this.filterlistClient = this.formaterClient(res.value)

    });
  }

  formaterClient(list: Client[]): ngSelectModel[] {
    const listClient: ngSelectModel[] = [];
    list.forEach((client, index) => {
      listClient.push({ id: client.id, name: client.name, disabled: false });
    });
    return listClient;
  }
  getTechniciens() {
    this.filter = {
      roleIds: this.listRole,
      SearchQuery: this.search,
      Page: this.page,
      PageSize: 10,
      OrderBy: 'firstName',
      SortDirection: 'Descending',
      ignorePagination: true
    };
    this.service.getAllPagination(ApiUrl.User, this.filter)
      .subscribe(res => {
        this.techniciens = res.value;
        this.filterlistTechnicien = this.formaterTechnicien(this.techniciens);
        this.listeTechniciens = res.value.map(element => {
          return { Text: element.lastName, Id: element.id, GroupId: UserProfile.technicien, Color: '', Designation: element.registrationNumber }
        });
      }, err => {
      });
  }

  formaterTechnicien(list: User[]): ngSelectModel[] {
    const listTechnicien: ngSelectModel[] = [];
    list.forEach((technicien, index) => {
      listTechnicien.push({ id: technicien.id, name: technicien.lastName, disabled: false });
    });
    return listTechnicien;
  }
  getRole(): Promise<void> {
    return new Promise((reslove, reject) => {
      this.service.getAll(ApiUrl.role).subscribe(async res => {
        this.role = res.value;
        this.listRole.push(this.role.find(x => x.type === this.userProfile.admin).id)
        const idUserTechnicien = this.role.find(x => x.type === this.userProfile.technicien).id;
        const idUserTechnicienMaintenance = this.role.find(x => x.type === this.userProfile.technicienMaintenance).id;
        if (idUserTechnicien !== undefined) {
          this.listRole.push(idUserTechnicien)
        }

        if (idUserTechnicienMaintenance !== undefined) {
          this.listRole.push(idUserTechnicienMaintenance)
        }
      }, err => {

      }, () => {
        reslove();
      });
    });
  }
  async getVisiteMaintenance() {
    this.filterVisiteMaintenance = {
      externalPartnerId: this.idClient.length === 0 ? [] : this.idClient,
      status: StatutVisiteMaintenance.Unplanned,
      year: (this.annee == null || this.annee === 0) ? null : +this.annee,
      month: this.mois.length === 0 ? [] : this.mois,
      SearchQuery: this.search,
      Page: this.page,
      PageSize: 10,
      OrderBy: 'year',
      SortDirection: 'Descending',
      ignorePagination: true
    };
    this.service.getAllPagination(ApiUrl.VisitMaintenance, this.filterVisiteMaintenance)
      .subscribe(async res => {
        // sort list with year and month
        const sortListVisiteMaintenance = _.sortBy(res.value, ['year'], ['month']);
        this.listeLisiteMaintenance = res.value;
        // les selecteur des mois dans le ficheir du traduction
        const selector = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
        // recupers la traduction des mois
        const translate = await this.getTranslationByKey('mois');
        // formater les donnée à afficher dans la liste des vistes du maintenanace
        const dataSource = sortListVisiteMaintenance.map(x => {
          return {
            Id: x.contractId + x.month + x.year,
            client: x.client,
            site: x.site, // (JSON.parse(x.contratEntretien.site) as Adresse) ,
            date: translate[selector[x.month - 1]] + ' ' + x.year,
          }
        });
        // affecter les données
        this.field = { dataSource, id: 'Id', text: 'Name' };
      });
  }




  refreshFicheIntervention() {
    this.getFicheInterventionMaintenanceList();
    this.scheduleObj.refreshEvents();
  }

  /**data source for event setting */
  dataSource(data) {


    this.eventSettings = {
      dataSource: data.length === 0 ? [] : data,
      // fields: {
      // 	id: 'EventID',
      // 	subject: { name: 'EventName' },
      // 	isAllDay: { name: 'IsAllDay' },
      // 	description: { name: 'EventDescription' },
      // 	startTime: { name: 'StartDateTime' },
      // 	endTime: { name: 'EndDateTime' }
      // }
    };
  }

  /**
    *
    * @param args get color for event
    */
  applyCategoryColor(args: EventRenderedArgs): void {
    const categoryColor: string = args.data.CategoryColor as string;
    if (!args.element || !categoryColor) {
      return;
    }
    if (this.scheduleObj.currentView === 'Agenda') {
      (args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
    } else {
      args.element.style.backgroundColor = categoryColor;
    }
  }
  onActionComplete(args): void {
    if (args.requestType === 'viewNavigate' || args.requestType === 'dateNavigate') {
      const currentViewDates = this.scheduleObj.getCurrentViewDates();
      const startDate = currentViewDates[0];
      const endDate = currentViewDates[currentViewDates.length - 1];
      this.dateDebutAgenda = AppSettings.formaterDatetime(this.convert(startDate));
      this.DateFinAgenda = AppSettings.formaterDatetime(this.convert(endDate));
      setTimeout(() => {
        // 	this.setClickEventsforMaps();
      }, 90)
    }
  }

  getTranslationByKey(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.translate.get(key).subscribe(translation => {
        resolve(translation);
      });
    });
  }

  public onCloseClick(): void {
    this.scheduleObj.quickPopup.quickPopupHide();
  }

  navigateToDetailFicheIntervention(data) {
    localStorage.setItem(LocalElements.docType, ApiUrl.MaintenanceOperationSheet);
    this.router.navigate(['/planification/detail', data.Id])

  }

  navigateToDetailVisiteMaintenance(idVisiteMaintenance) {
    const dialogVisiteConfig = new MatDialogConfig();
    dialogVisiteConfig.width = '850px';
    dialogVisiteConfig.height = '650px';
    // 	dialogVisiteConfig.data = idVisiteMaintenance;

    const visite = this.listeLisiteMaintenance.filter(x => x => x.contractId + x.month + x.year === idVisiteMaintenance)[0] as VisiteMaintenance;
    dialogVisiteConfig.data = { idContratVisite: visite.contractId, year: visite.year, month: visite.month };

    this.dialog.open(ShowVisiteComponent, dialogVisiteConfig);
  }

  //#region pop up Calendar
  onPopupOpen(args: PopupOpenEventArgs): void {
    if (args.type === 'QuickInfo') {
      // QuickInfo for event  vide
      if (args.target.classList.contains('e-work-cells') || args.target.classList.contains('e-header-cells')) {
        this.scheduleObj.quickPopup.quickPopupHide(true);
        args.cancel = true;
      } else if (args.target.classList.contains('e-appointment')) {
        // QuickInfo for event
        (<HTMLElement>args.element).style.boxShadow = `1px 2px 5px 0 ${(<HTMLElement>args.target).style.backgroundColor}`;
      }
    }
    if (args.type === 'Editor') {

      const dataArgs: any = args.data;
      if (args.target === undefined || (dataArgs.Name === undefined && args.target.classList.contains('e-appointment'))) {
        // edit  for visite maintenance (add new event )
        const clientElement: HTMLInputElement = args.element.querySelector('#Client') as HTMLInputElement;
        clientElement.value = this.visiteMaintenanceSelected[0]['client'];

        const siteElement: HTMLInputElement = args.element.querySelector('#Site') as HTMLInputElement;
        siteElement.value = this.visiteMaintenanceSelected[0]['site']['designation']; // designation

        const startElement: HTMLInputElement = args.element.querySelector('#DateDebut') as HTMLInputElement;
        if (!startElement.classList.contains('e-datetimepicker')) {
          new DateTimePicker({ value: this.cellData.startTime }, startElement);
        }

        const endElement: HTMLInputElement = args.element.querySelector('#DateFin') as HTMLInputElement;
        if (!endElement.classList.contains('e-datetimepicker')) {
          new DateTimePicker({ value: this.cellData.endTime }, endElement);
        }

        // document.getElementById('RecurrenceEditor').style.display = (this.scheduleObj.currentAction === 'EditOccurrence') ? 'none' : 'block';

      } else {
        this.scheduleObj.quickPopup.quickPopupHide(true);
        args.cancel = true;
      }
    }

  }

  onPopupClose(args) {
    this.newInterventionData = args.data;
    // count les fiche d'intervention
    this.dataSourceLength = (this.scheduleObj.eventSettings.dataSource as any).length;
    const interval = setInterval(async () => {
      const currentDataSourceLength = (this.scheduleObj.eventSettings.dataSource as any).length;
      if (currentDataSourceLength !== this.dataSourceLength) {

        this.service.create(ApiUrl.FicheInterventionMaintenance + '/Create/Maintenance', await this.createBodyRequest(args))
          .subscribe(async res => {
            if (res) {
              const translation = await this.getTranslationByKey('add');
              toastr.success(translation.msg, translation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
              const Id = this.scheduleObj.eventSettings.dataSource[currentDataSourceLength - 1].Id
              this.scheduleObj.eventSettings.dataSource[currentDataSourceLength - 1] = {
                Id: res.id,
                Subject: res.reference + "," + args.data.Client,
                Name: args.data.Client,
                StartTime: (args.data.DateDebut),
                EndTime: (args.data.DateFin),
                Description: res.purpose,
                DepartmentID: UserProfile.technicien,
                ConsultantID: +this.IdTechnicien,
                DepartmentName: args.data.Site,
                CategoryColor: '#00d2d3'// changecolorEvent

              };
              this.scheduleObj.refreshEvents();
              this.getFicheInterventionMaintenanceList();
              await this.getVisiteMaintenance();
              clearInterval(interval);
            }
          }
            , async err => {
              console.log(err);
              const translation = await this.getTranslationByKey('errors');
              toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }, () => {
              this.scheduleObj.refreshEvents();
              clearInterval(interval);
            }
          );
        this.dataSourceLength = (this.scheduleObj.eventSettings.dataSource as any).length;
      }
    }, 1000);
  }
  //#endregion


  //#region fonction server

  /**
   * @param args Create body for fiche intervention maintenance
   */
  async createBodyRequest(args) {

    const ficheInterventionMaintenance: FicheInterventionMaintenance = new FicheInterventionMaintenance();
    const visite = await this.visiteMaintenanceById(this.visiteMaintenanceSelected[0].Id);
    ficheInterventionMaintenance.year = visite.year;
    ficheInterventionMaintenance.month = visite.month;
    // 	ficheInterventionMaintenance.status = StatutFicheInterventionMaintenance.Planifiee;
    ficheInterventionMaintenance.maintenanceContractId = visite.contractId;
    ficheInterventionMaintenance.equipmentDetails = visite.equipmentDetails;
    ficheInterventionMaintenance.type = TypeInterventionMaintenance.Maintenance;
    ficheInterventionMaintenance.clientId = visite.clientId;
    ficheInterventionMaintenance.client = visite.client;
    const debut = AppSettings.formatDate(args.data.DateDebut);
    ficheInterventionMaintenance.startDate = AppSettings.formaterDatetime(args.data.DateDebut);
    ficheInterventionMaintenance.endDate = AppSettings.formaterDatetime(args.data.DateFin);
    ficheInterventionMaintenance.addressIntervention = visite.site;
    ficheInterventionMaintenance.technicianId = this.IdTechnicien.toString();
    ficheInterventionMaintenance.purpose = args.data.Object;
    ficheInterventionMaintenance.reference = await this.generateReference();
    ficheInterventionMaintenance.status = StatutFicheInterventionMaintenance.Planned;
    return ficheInterventionMaintenance;
  }


  visiteMaintenanceById(id): Promise<any> {
    const visite = this.listeLisiteMaintenance.filter(x => x.contractId + x.month + x.year === this.visiteMaintenanceSelected[0].Id)[0] as VisiteMaintenance;
    return new Promise((resolve, reject) => {
      this.service.getAll(ApiUrl.VisitMaintenance + '/contract/' + visite.contractId + '/' + visite.month + '/' + visite.year)
        .subscribe(res =>
          resolve(res.value), err => reject(err))
    });
  }

  getFicheInterventionMaintenanceList(): Promise<void> {
    this.filter = {
      techniciansId: this.technicienId,
      externalPartnerId: this.idClientFicheIntervention,
      workshopId: '',
      status: (this.statut == null || this.statut == "") ? [] : [this.statut],
      SearchQuery: this.search,
      Page: this.page,
      PageSize: 10,
      OrderBy: 'reference',
      SortDirection: 'Descending',
      ignorePagination: true
    };
    return new Promise((reslove, reject) => {
      this.service.getAllPagination(ApiUrl.FicheInterventionMaintenance, this.filter).subscribe(res => {
        this.ficheInterventionMaintenanceList = res.value as FicheInterventionMaintenance[];
        this.data = [];
        if (this.ficheInterventionMaintenanceList.length === 0) {
          this.dataSource(this.data);
        } else {

          this.ficheInterventionMaintenanceList.forEach(async element => {
            this.data.push({
              Id: element.id,
              Subject: element.reference + ',' + element.client,
              Name: element.client,
              StartTime: new Date(element.startDate),
              EndTime: new Date(element.endDate),
              Description: element.purpose, // objet
              DepartmentID: UserProfile.technicien,
              ConsultantID: element.technicianId,
              DepartmentName: element.addressIntervention, // this.jsonParse(element.adresseIntervention).designation,
              // RecurrenceRule: 'FREQ=WEEKLY;INTERVAL=1;BYDAY=FR',
              // CategoryColor: '#7fa900',
              //customClass: 'cust-appointment--interventionsMaintenance',

              CategoryColor: this.getColorByStatut(element.status)// changecolorEvent
            });
            this.dataSource(this.data);
            reslove();
          });
        }

      });

    });

  }


  //#endregion


  oncreated(): void {
    const currentViewDates = [this.selectedDate];
    const startDate: Date = currentViewDates[0] as Date;
    const endDate: Date = currentViewDates[currentViewDates.length - 1] as Date;
    this.dateDebutAgenda = AppSettings.formaterDatetime(this.convert(startDate));
    this.DateFinAgenda = AppSettings.formaterDatetime(this.convert(endDate));
    setTimeout(() => {
      // this.setClickEventsforMaps();
    }, 90)
  }
  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-');
  }


  onActionBegin(event: ActionEventArgs): void {
    if (event.requestType === 'eventCreate' && this.isTreeItemDropped) {
      const treeViewdata: { [key: string]: Object }[] = this.treeObj.fields.dataSource as { [key: string]: Object }[];
      const filteredPeople: { [key: string]: Object }[] =
        treeViewdata.filter((item: any) => item.Id !== parseInt(this.draggedItemId, 10));
      this.treeObj.fields.dataSource = filteredPeople;
      const elements: NodeListOf<HTMLElement> = document.querySelectorAll('.e-drag-item.treeview-external-drag');
      for (let i = 0; i < elements.length; i++) {
        remove(elements[i]);
      }
    }

    if (event.requestType === 'eventChange') {
      const { Id, StartTime, EndTime, ConsultantID } = event.changedRecords[0] as any;
      this.updateInterventionMAintenance(Id, StartTime, EndTime, ConsultantID);
    }
  }
  /**
   * Disabel cell click
   */
  onCellClick(args) {
    args.cancel = true;
  }

  oncellDoubleClick(args) {
    args.cancel = true;
  }
  updateInterventionMAintenance(Id, StartTime, EndTime, ConsultantID) {

    const fiche = this.ficheInterventionMaintenanceList.filter(x => x.id === Id)[0] as FicheInterventionMaintenance;
    if (fiche.status !== StatutFicheInterventionMaintenance.Realized && fiche.status !== StatutFicheInterventionMaintenance.Billed) {
      this.translate.get('file.update').subscribe(text => {
        swal({
          title: text.title,
          text: text.question,
          icon: 'error',
          buttons: {
            cancel: {
              text: text.cancel,
              value: null,
              visible: true,
              className: '',
              closeModal: true
            },
            confirm: {
              text: text.confirm,
              value: true,
              visible: true,
              className: '',
              closeModal: true
            }
          }
        }).then(isConfirm => {
          if (isConfirm) {
            Date.prototype.addHours = function (h) {
              this.setTime(this.getTime() + (h * 60 * 60 * 1000));
              return this;
            }
            fiche.startDate = fiche.startDate !== StartTime ? (StartTime as Date).addHours(1) : fiche.startDate;
            fiche.endDate = fiche.endDate !== EndTime ? (EndTime as Date).addHours(1) : fiche.endDate;
            fiche.technicianId = ConsultantID;
            this.service.update(ApiUrl.FicheInterventionMaintenance, fiche, Id).subscribe(res => {
              swal(text.success, '', 'success');
              // toastr.success(text.success, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            });
            //	(StartTime as Date).addHours(-1);
            //	(EndTime as Date).addHours(-1);
            this.scheduleObj.refreshEvents();
          } else {
            this.getFicheInterventionMaintenanceList();
            toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
          }
        });
      });
    } else {
      const msgErr = this.translate.instant('errors.statutRealiseFacture');
      // toastr.warning(msgErr, "", { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
      toastr.warning(msgErr, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
      this.getFicheInterventionMaintenanceList();

      this.scheduleObj.refreshEvents();

    }

  }

  onItemDrag(event: any): void {
    if (this.scheduleObj.isAdaptive) {
      const classElement: HTMLElement = this.scheduleObj.element.querySelector('.e-device-hover');
      if (classElement) {
        classElement.classList.remove('e-device-hover');
      }
      if (event.target.classList.contains('e-work-cells')) {
        addClass([event.target], 'e-device-hover');
      }
    }
    if (document.body.style.cursor === 'not-allowed') {
      document.body.style.cursor = '';
    }
    if (event.name === 'nodeDragging') {
      const dragElementIcon: NodeListOf<HTMLElement> =
        document.querySelectorAll('.e-drag-item.treeview-external-drag .e-icon-expandable');
      for (let i = 0; i < dragElementIcon.length; i++) {
        dragElementIcon[i].style.display = 'none';
      }
    }

  }

  onTreeDragStop(event: DragAndDropEventArgs): void {
    const treeElement: Element = <Element>closest(event.target, '.e-treeview');
    const classElement: HTMLElement = this.scheduleObj.element.querySelector('.e-device-hover');
    if (classElement) {
      classElement.classList.remove('e-device-hover');
    }
    if (!treeElement) {
      event.cancel = true;
      const scheduleElement: Element = <Element>closest(event.target, '.e-content-wrap');
      if (scheduleElement) {
        const treeviewData: { [key: string]: Object }[] =
          this.treeObj.fields.dataSource as { [key: string]: Object }[];
        if (event.target.classList.contains('e-work-cells')) {
          const filteredData: { [key: string]: Object }[] = treeviewData.filter((item: any) => item.Id === event.draggedNodeData.id as string);
          const cellData: CellClickEventArgs = this.scheduleObj.getCellDetails(event.target);
          const resourceDetails: ResourceDetails = this.scheduleObj.getResourcesByIndex(cellData.groupIndex);
          this.visiteMaintenanceSelected = filteredData;
          this.cellData = cellData;
          const eventData: { [key: string]: Object } = {
            Name: filteredData[0].Name,
            StartTime: cellData.startTime,
            EndTime: cellData.endTime,
            IsAllDay: cellData.isAllDay,
            Description: filteredData[0].Description,
            DepartmentID: resourceDetails.resourceData.GroupId,
            ConsultantID: resourceDetails.resourceData.Id
          };
          this.IdTechnicien = resourceDetails.resourceData.Id.toString();
          this.scheduleObj.openEditor(eventData, 'Add', true);
          this.isTreeItemDropped = true;
          this.draggedItemId = event.draggedNodeData.id as string;

        }
      }
    }
  }
  /**
   * return a reference for the new insertion
   */
  generateReference(): Promise<string> {
    return new Promise((reslove, reject) => {
      this.service.getAll(ApiUrl.configReference + TypeNumerotation.fiche_interventionMaintenance + '/reference').subscribe(
        res => {
          reslove(res);
        },
        err => { console.log(err); }
      );
    });
  }
  //#region color calendar
  getColorByStatut(statut: StatutFicheInterventionMaintenance) {
    switch (statut) {
      case StatutFicheInterventionMaintenance.Late:
        return '#ff0000';
        break;
      case StatutFicheInterventionMaintenance.Draft:
        return '#636e72';
        break;
      case StatutFicheInterventionMaintenance.Canceled:
        return '#fe0000';
        break;
      case StatutFicheInterventionMaintenance.Planned:
        return '#00d2d3'; // this.getColorStatutPlanifier(date)
        break;
      case StatutFicheInterventionMaintenance.Realized:
        return '#2ecc71';
        break;
      case StatutFicheInterventionMaintenance.Billed:
        return '#3498db';
        break;
      case StatutFicheInterventionMaintenance.UnDone:
        return '#eb9c42'; // this.getColorStatutPlanifier(date)
        break;
      //
    }
  }

  navigateToCreate() {
    localStorage.setItem(LocalElements.docType, ApiUrl.MaintenanceOperationSheet);
    this.router.navigate(['/planification/create'])

  }
  //#endregion
  traductionCalendarToFranch() {
    loadCldr(
      require('./traduction/numberingSystems.json'),
      require('./traduction/main/fr/ca-gregorian.json'),
      require('./traduction/main/fr/currencies.json'),
      require('./traduction/main/fr/numbers.json'),
      require('./traduction/main/fr/timeZoneNames.json')
    );
    L10n.load({
      'fr': {
        'schedule': {
          'day': 'journée',
          'week': 'La semaine',
          'workWeek': 'Semaine de travail',
          'month': 'Mois',
          'agenda': 'Ordre du jour',
          'weekAgenda': 'Agenda de la semaine',
          'workWeekAgenda': 'Agenda de la semaine de travail',
          'monthAgenda': 'Agenda du mois',
          'today': 'Aujourd\'hui',
          'noEvents': 'Pas d\'événements',
          'emptyContainer': 'Aucun événement n\'est prévu ce jour-là.',
          'allDay': 'Toute la journée',
          'start': 'Début',
          'end': 'Fin',
          'more': 'plus',
          'close': 'Fermer',
          'cancel': 'Annuler',
          'noTitle': '(Pas de titre)',
          'delete': 'Effacer',
          'deleteEvent': 'Supprimer un événement',
          'deleteMultipleEvent': 'Supprimer plusieurs événements',
          'selectedItems': 'Articles sélectionnés',
          'deleteSeries': 'Supprimer la série',
          'edit': 'modifier',
          'editSeries': 'Modifier la série',
          'editEvent': 'Modifier l\'événement',
          'createEvent': 'Créer',
          'subject': 'Assujettir',
          'addTitle': 'Ajouter un titre',
          'moreDetails': 'Plus de détails',
          'save': 'sauvegarder',
          'editContent': 'Voulez-vous modifier uniquement cet événement ou une série entière?',
          'deleteRecurrenceContent': 'Voulez-vous supprimer uniquement cet événement ou une série entière?',
          'deleteContent': 'Êtes-vous sûr de vouloir supprimer cet événement?',
          'deleteMultipleContent': 'Êtes-vous sûr de vouloir supprimer les événements sélectionnés?',
          'newEvent': 'Ajouter fiche d\'intervention maintenance',
          'title': 'Titre',
          'location': 'Emplacement',
          'description': 'La description',
          'timezone': 'Fuseau horaire',
          'startTimezone': 'Début du fuseau horaire',
          'endTimezone': 'Fin du fuseau horaire',
          'repeat': 'Répéter',
          'saveButton': 'sauvegarder',
          'cancelButton': 'Annuler',
          'deleteButton': 'Effacer',
          'recurrence': 'Récurrence',
          'wrongPattern': 'Le modèle de récurrence n\'est pas valide.',
          'seriesChangeAlert': 'Les modifications apportées à des instances spécifiques de cette série seront annulées et ces événements correspondront à nouveau à la série.',
          'createError': 'La durée de l\'événement doit être plus courte que sa fréquence. Raccourcissez la durée ou modifiez le modèle de récurrence dans l\'éditeur d\'événement de récurrence.',
          'recurrenceDateValidation': 'Certains mois ont moins que la date sélectionnée. Pour ces mois, l\'événement se produira à la dernière date du mois.',
          'sameDayAlert': 'Deux occurrences du même événement ne peuvent pas se produire le même jour.',
          'editRecurrence': 'Modifier la récurrence',
          'repeats': 'Répète',
          'alert': 'Alerte',
          'startEndError': 'La date de fin sélectionnée se produit avant la date de début.',
          'invalidDateError': 'La valeur de date saisie est invalide.',
          'ok': 'D\'accord',
          'occurrence': 'Occurrence',
          'series': 'Séries',
          'previous': 'précédent',
          'next': 'Prochain',
          'timelineDay': 'Journée',
          'timelineWeek': 'Semaine',
          'timelineWorkWeek': 'Semaine',
          'timelineMonth': 'Mois Chronologique'
        },
        'recurrenceeditor': {
          'none': 'Aucun',
          'daily': 'du quotidien',
          'weekly': 'Hebdomadaire',
          'monthly': 'Mensuel',
          'month': 'Mois',
          'yearly': 'Annuel',
          'never': 'Jamais',
          'until': 'Jusqu\'à',
          'count': 'Compter',
          'first': 'Premier',
          'second': 'Seconde',
          'third': 'Troisième',
          'fourth': 'Quatrième',
          'last': 'Dernier',
          'repeat': 'Répéter',
          'repeatEvery': 'Répéter tous les',
          'on': 'Répéter sur',
          'end': 'Fin',
          'onDay': 'journée',
          'days': 'Journées)',
          'weeks': 'Semaines)',
          'months': 'Mois)',
          'years': 'Années)',
          'every': 'chaque',
          'summaryTimes': 'fois)',
          'summaryOn': 'sur',
          'summaryUntil': 'jusqu\'à',
          'summaryRepeat': 'Répète',
          'summaryDay': 'journées)',
          'summaryWeek': 'semaines)',
          'summaryMonth': 'mois)',
          'summaryYear': 'années)',
          'monthWeek': 'Mois Semaine',
          'monthPosition': 'Position du mois',
          'monthExpander': 'Mois Expander',
          'yearExpander': 'Année Expander',
          'repeatInterval': 'Intervalle de répétition'

        },
        'dropdowns': {
          'noRecordsTemplate': 'Aucun enregistrement trouvé',
          'actionFailureTemplate': 'Modèle d\'échec d\'action',
          'overflowCountTemplate': '+${count} plus..',
          'totalCountTemplate': '${count} choisi'
        }
      }
    });
  }
}



interface TechniciensInterface {
  Text: string,
  Id: number,
  GroupId: number,
  Color: string,
  Designation: string
}

interface VisiteMaintenanaceFilterInterface {
  listeClient: Client[],
  listeAnnee: number[],
  annee: number,
  idClient: number
}

declare global {
  interface Date {
    addHours(hour?: number): any
  }


}

export interface addressMarker {
  adresse: any;
  ville: string;
  codePostal: string;
}

export class ngSelectModel {
  id: string;
  name: string;
  disabled: boolean;
}


