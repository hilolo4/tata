import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowVisiteComponent } from './show-visite.component';

describe('ShowVisiteComponent', () => {
  let component: ShowVisiteComponent;
  let fixture: ComponentFixture<ShowVisiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowVisiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowVisiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
