import { Component, OnInit, Inject } from '@angular/core';
import { MenuItem } from 'app/custom-module/primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { StatutReccurente } from 'app/Enums/Statut/StatutReccurente.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
import { GlobalInstances } from 'app/app.module';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var toastr: any;
declare var jQuery: any;
declare var swal: any;
@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowReccurenteComponent implements OnInit {
	id;
	reccurente = null;
	statutReccurente: typeof StatutReccurente = StatutReccurente;
	processIsStarting: boolean = false;
	actionsItems: MenuItem[] = [];
	client: Client;
	adresseFacturation: string;
	chantiers: Workshop[] = [];
	statuts: { id: number; label: string; color: string }[];
	articles = [];
	articlesInfo: any = {};
	clientInfo: Client = new Client();
	processing: boolean = false;
	selectedTabs = 'information';
	docType = '';
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private router: Router,
		private header?: HeaderService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
	}

	async ngOnInit() {
		this.processing = true;
		this.route.params.subscribe(async params => {
			this.id = params['id'];
			this.translate.get('statuts').subscribe((statuts: { id: number; label: string; color: string }[]) => this.statuts = statuts);
			await this.init();
			this.prepareBreadcrumb();

			this.processing = false;
		});

	}
	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}
	modifier() {
		return GlobalInstances.router.navigateByUrl(`/factureReccurente/modifier/${this.reccurente.id}`);
	}
	async init(): Promise<void> {
		await this.refresh();
		this.client = this.reccurente.document.client;

	}
	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{
					label: this.client ? this.client.reference : this.reccurente.document.clientName,
					route: ManageDataTable.getRouteDetails(this.reccurente)
				}
			];
			//		TranslateConfiguration.setCurrentLanguage(this.translate);

		});
	}
	// getclientById(idClient): Promise<Client> {
	// 	return new Promise((resolve, reject) => {
	// 		this.service.getById(ApiUrl.Client, idClient).subscribe(
	// 			res => resolve(res.value),
	// 			err => reject(err)
	// 		);
	// 	});
	// }


	// Refresh facture aprés l'ajout de paiement ou init component
	refresh(): Promise<void> {

		return new Promise((reslove, reject) => {
			this.service.getById(ApiUrl.Recurring, this.id).subscribe(async res => {
				this.reccurente = res.value;
				this.articles = this.reccurente.document.orderDetails.lineItems; this.actionsItems = [];

			}, err => {

			}, () => {
				reslove();
			});
		});
	}


	//dupliquer reccurence
	dupliquerReccurente() {

		// ReccurenteState.reccurente = this.reccurente;
		// this.router.navigate(['/reccurentes/ajouter', CreateReccurent.DUPLIQUER])
	}
}
