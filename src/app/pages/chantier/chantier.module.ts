import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { ChantierRoutingModule } from './chantier-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { CommonModules } from '../../common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, DialogModule, SplitButtonModule } from 'app/custom-module/primeng/primeng';

import { ChantierComponent } from './chantier-details/chantier-information/chantier-information.component';
import { ShowComponent } from './chantier-details/chantier-details.component';

import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { RebriqueModule } from './rebrique/rebrique.module';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { RecaptulatifFinancierComponent } from './recaptulatif-financier/recaptulatif-financier.component';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule, MatNativeDateModule, MatDialogModule, MatInputModule } from '@angular/material';
import { RetenueGarantieComponent } from './retenue-garantie/retenue-garantie.component'

import { OverlayModule } from '@angular/cdk/overlay';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';

import { ChartsModule } from 'ng2-charts';
import { MatMenuModule } from '@angular/material/menu';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { ContactsModule } from '../contacts/contacts.module';
import { DetailsDocumentModule } from '../documents/details-document/details-document.module';
// import { NgProgressHttpModule } from '@ngx-progressbar/http';
// import { NgProgressRouterModule } from '@ngx-progressbar/router';


import { ConfigDialogComponent } from '../../shared/config-dialog/config-dialog.component';
import { IndexComponent } from './chantier-index/chantier-index.component';
import { AddFactureClotureComponent } from '../../components/add-facture-cloture/add-facture-cloture.component';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';
import { MenuService } from 'app/services/menu/menu.service';
import { SliderModule } from 'app/common/slider/slider.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DynamicDialogModule } from 'app/custom-module/primeng/dynamicdialog';
import { EditRubriqueFormModule } from 'app/components/edit-rubrique-form/edit-rubrique-form.module';
export function HttpLoaderFactory(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' }
	]);
}


@NgModule({
	providers: [
		PreviousRouteService,
		MatDatepickerModule,
		MenuService
	],
	imports: [
		CommonModule,
		ChantierRoutingModule,
		NgSelectModule,
		MatMenuModule,
		MatTabsModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		ReactiveFormsModule,
		DataTablesModule,
		CommonModules,
		NgbTooltipModule,
		AngularEditorModule,
		NgxMatSelectSearchModule,
		OverlayModule,
		MatTooltipModule,
		MatSelectModule,
		MatDatepickerModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatCheckboxModule,
		TableArticleModule,
		RebriqueModule,
		CalendarModule,
		ChartsModule,
		MatIconModule,
		MatSliderModule,
		SplitButtonModule,
		ContactsModule,
		DetailsDocumentModule,
		AddChantierFormModule,
		MatDialogModule,
		SliderModule,
		CalendarModule,
		DynamicDialogModule,
		DialogModule,
		MatDatepickerModule,
		MatInputModule,
		MatSelectModule,
		EditRubriqueFormModule,
	],
	declarations: [
		ConfigDialogComponent,
		ChantierComponent,
		ShowComponent,
		RecaptulatifFinancierComponent,
		RetenueGarantieComponent,
		IndexComponent

	]
})
export class ChantiersModule { }
