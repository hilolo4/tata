import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { IFormType } from '../../../common/chantier-form/IFormType.enum';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { User } from 'app/Models/Entities/User';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Router } from '@angular/router';
import { Constants } from 'app/shared/utils/constants';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { WorkShopListModel } from 'app/Models/Model/ListModel';
import { DeleteModel } from 'app/Models/Model/deleteModel';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var jQuery: any;
declare var swal: any;
declare var toastr: any;

@Component({
	selector: 'app-chantier-index',
	templateUrl: './chantier-index.component.html',
	styleUrls: ['./chantier-index.component.scss']
})
export class IndexComponent implements OnInit {
	formType: typeof IFormType = IFormType;
	formConfig = {
		type: null,
		defaultData: null
	};


	serach = '';
	pageNumber = 1;
	pageSize = 10;
	pageSizes = [10, 25, 50, 100];
	statut = null;
	statutChantier: typeof StatutChantier = StatutChantier;
	chantiers: WorkShopListModel;
	statuts;
	isAdmin = true;
	client = null;
	processing = false;
	listClients: Client[] = null;
	filter;
	@ViewChild('selecteClient') selecteClient: NgSelectComponent;
	preventSingleClick = false;
	timer: any;
	delay: Number;

	filtersOpened = false;
	selectedChantier;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private router: Router,
		private header: HeaderService) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
	}

	async ngOnInit() {
		this.prepareBreadcrumb();
		this.getAll();
		this.getClients();
		this.getlistStatus();


	}
	prepareBreadcrumb(): void {
		this.translate.get(`title`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: translation, route: '/chantiers' }]
		});
	}
	async getlistStatus() {
		if (this.statuts == null) {
			const transaltions = await this.getTranslationByKey('statut');
			this.statuts = [];
			this.statuts.push({ value: '', name: transaltions.tous });
			this.statuts.push({ value: this.statutChantier.Accepte, name: transaltions.accepte });
			this.statuts.push({ value: this.statutChantier.EnEtude, name: transaltions.enetude });
			this.statuts.push({ value: this.statutChantier.NonAcceptee, name: transaltions.refuse });
			this.statuts.push({ value: this.statutChantier.Termine, name: transaltions.termine });
		}
	}

	doubleClick(chantier) {
		this.preventSingleClick = true;
		clearTimeout(this.timer);
		this.router.navigate(['/chantiers/detail', chantier.id]);
	}

	OnSave(chantier) {
		switch (this.formConfig.type) {
			case IFormType.add:
				this.add(chantier);
				break;
			case IFormType.update:
				this.update(chantier);
				break;
		}
	}
	setformConfig(defaultData, type) {
		this.formConfig.defaultData = defaultData;
		this.formConfig.type = type;
	}

	add(chantier) {
		this.service.create(ApiUrl.Chantier + ACTION_API.create, chantier).subscribe(res => {
			this.translate.get('addchantier').subscribe(text => {
				this.getAll();
				jQuery('#Model').modal('hide');
				toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			});
		});
	}

	update(chantier) {
		this.service.update(ApiUrl.Chantier, chantier, chantier.id).subscribe(res => {
			this.translate.get('update').subscribe(text => {
				this.getAll();
				jQuery('#Model').modal('hide');
				toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		}, err => {
			console.log(err);
			jQuery('#Model').modal('hide');
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});
	}


	getAll() {
		this.processing = true;
		this.filter = {
			clientId: this.client,
			status: (this.statut !== null && this.statut !== '') ? [this.statut] : [],
			SearchQuery: this.serach,
			Page: this.pageNumber,
			PageSize: this.pageSize,
			OrderBy: 'createdOn',
			SortDirection: 'Descending',
		};
		this.service.getAllPagination(ApiUrl.Chantier, this.filter)
			.subscribe((res: WorkShopListModel) => {
				if (res.isSuccess) {
					this.processing = false;
					this.chantiers = res;
				}
			}, err => this.translate.get('errors').subscribe(errors => {
				if (this.processing) {
					this.processing = false;
					toastr.warning(errors.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			}));
	}

	chagePageNumber(value) {
		this.pageNumber = value;
		this.getAll();
	}

	changePageSize(value) {
		this.pageSize = value;
		this.getAll();
	}

	OnDelete(id) {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.Chantier, id).subscribe((res: DeleteModel) => {
						if (res.isSuccess) {
							swal(text.success, '', 'success');
							this.getAll();
						} else {
							swal(text.ImpossibleDeSuppression, '', 'error');
						}
					}, err => {
						swal(text.ImpossibleDeSuppressionChnatier, '', 'error');
					});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}

	OnShow(chantier: Workshop) {
		this.setformConfig(chantier, this.formType.preview);
		jQuery('#Model').modal('show');
	}

	OnEdit(chantier: Workshop) {
		this.setformConfig(chantier, this.formType.update);
		jQuery('#Model').modal('show');
	}

	search() {
		this.getAll();
	}

	GetNameOfClient(idClient) {
		const result = (this.listClients as any[]).filter(c => c.id === idClient);
		return result.length > 0 ? result[0].nom : '';
	}


	getClients(): void {
		if (this.listClients == null) {
			this.processing = true;
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
			}, async err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				// this.selecteClient.isOpen = true;
				this.processing = false;
			});
		}
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}

	clearStatutFilter() {
		this.statut = null;
	}

	onFiltersTriggered(): void {
		this.filtersOpened = !this.filtersOpened;
	}

	onMenuOpened(chantier: any): void {
		this.selectedChantier = chantier;
	}

	onBackdropClicked(): void {
		this.filtersOpened = false;
	}
}
