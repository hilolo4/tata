
import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { EventSettingsModel, GroupModel, ActionEventArgs } from '@syncfusion/ej2-schedule';
import { L10n, loadCldr, setCulture, addClass, remove } from '@syncfusion/ej2-base';
import { ScheduleComponent, TimeScaleModel, PopupOpenEventArgs, EventRenderedArgs, View } from '@syncfusion/ej2-angular-schedule';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { User } from 'app/Models/Entities/User';
import { OperationSheet } from 'app/Models/Entities/Documents/OperationSheet';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FicheInterventionMaintenance } from 'app/Models/Entities/Maintenance/FicheInterventionMaintenance';
import { TypeFicheIntervention } from 'app/Enums/TypeFicheIntervention';
import { LocalElements } from 'app/shared/utils/local-elements';
import { AgendaTypeFilter } from 'app/Enums/AgendaTypeFIlter';
import { HeaderService } from 'app/services/header/header.service';
import { MatDatepickerInputEvent, MatDialog, MatOption } from '@angular/material';
import { DialogHelper } from 'app/libraries/dialog';
import { MissionKind } from 'app/Enums/missionKinds';
import { AgendaFormComponent } from 'app/shared/agenda-form/agenda-form/agenda-form.component';
import { TranslateConfiguration } from 'app/libraries/translation';
export declare var swal: any;
declare var toastr: any;
declare let require: Function;
setCulture('fr');

interface TechniciensInterface {
	Text: string,
	Id: string,
	GroupId: number,
	Color: string,
	Designation: string
}
export class ngSelectModel {
	id: string;
	name: string;
	disabled: boolean;
}

@Component({
	selector: 'app-agenda-global',
	templateUrl: './agenda-global.component.html',
	styleUrls: ['./agenda-global.component.scss']
})
export class AgendaGlobalComponent implements OnInit {

	@ViewChild('scheduleObj') scheduleObj: ScheduleComponent;
	@ViewChild('treeObj') treeObj: TreeViewComponent;
	@ViewChild('allSelected') allSelected: MatOption;
	public currentView: View = 'Month';
	isTreeItemDropped = false;
	allowMultiple = false;
	allowDragAndDrop = true;
	field: Object;
	draggedItemId = '';
	selectedDate: Date = new Date();
	public workWeekDays: number[] = [1, 2, 3, 4, 5, 0, 6]; // is manday to sanday
	public showWeekend: Boolean = true;
	public showWeekNumber = true;
	public starDaysOftWeek = 1; // start week day is monday
	public eventSettings: EventSettingsModel = { dataSource: [] };
	techniciensDataSource: Object[] = [{ Text: 'Techniciens', Id: UserProfile.technicien, Color: '#b35676' }];
	listeTechniciens: TechniciensInterface[] = [];
	group: GroupModel = null;
	listeClient: [];
	IdTechnicien = null;
	dataSourceLength = 0;
	newInterventionData = null;
	ficheInterventionMaintenanceList;
	idClient: [] = [];
	data: object[] = [];
	idClientFicheIntervention = null;
	statut = null;
	listMois: any = null;
	cellData: any = null;
	mois: [] = [];
	technicienId = [];
	listStatus: any = [];
	statutFicheIntervention: typeof StatutFicheIntervention = StatutFicheIntervention;
	typeFicheIntervention: typeof TypeFicheIntervention = TypeFicheIntervention;
	agendaTypeFilter: typeof AgendaTypeFilter = AgendaTypeFilter;
	currentViewDates: Date = new Date();
	public text = 'Sélectionnez un élément';
	public timeScaleOptions: TimeScaleModel = { enable: true };
	listTechniciensFilter = [];
	filterlistTechnicien: ngSelectModel[] = [];
	userProfile: typeof UserProfile = UserProfile;
	ficheType = null;
	listTechniciens: User[] = [];
	listTechnicien = [];
	role = [];
	filter;
	page = 1;
	totalPage = 1;
	techniciens = [];
	search = '';
	listRole = [];
	tasks = [];
	filterMission;
	processing = false;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private translate?: TranslateService,
		private header?: HeaderService,
		public dialog?: MatDialog
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.ficheType =
			[this.agendaTypeFilter.InterventionsMaintenance, this.agendaTypeFilter.InterventionsChantier,
			this.agendaTypeFilter.Taches, this.agendaTypeFilter.Evenements, this.agendaTypeFilter.Rappels];

	}

	prepareBreadcrumb(): void {
		this.translate.get(`title`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('title'), route: '/agendaglobal' }
			];
		});

	}
	async ngOnInit() {
		this.processing = true;
		this.prepareBreadcrumb();
		this.traductionCalendarToFranch();
		this.getTechniciens();
		this.getFicheInterventionMaintenanceList();
		this.getFicheInterventionChantierList();
		this.getTaches();
		if (this.scheduleObj && this.scheduleObj.element) {
			// this.scheduleObj.refreshEvents();
		}
		this.processing = false;
	}

	getViewDate = () => this.scheduleObj && this.scheduleObj.selectedDate ? this.scheduleObj.selectedDate : '';
	getCurrentDate = () => new Date();
	getDateFormat = () => this.scheduleObj ? (this.scheduleObj.currentView === 'Month' ? 'MMMM yyyy' : 'dd MMMM yyyy') : '';

	onAgendaPrevious(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-prev button') as any;
			if (btn) {
				btn.click();
			}
		}
	}

	onAgendaNext(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-next button') as any;
			if (btn) {
				btn.click();
			}
		}
	}

	onAgendaToday(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-today button') as any;
			if (btn) {
				btn.click();
			}
		}
	}

	onAgendaDate(e: any): void {
		if (this.scheduleObj && e && e.value) {
			this.scheduleObj.selectedDate = e.value;
		}
	}

	onDisplayChanged(e: number): void {
		const keys = ['e-month', 'e-day', 'e-week'];
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector(`.e-toolbar-item.e-views.${keys[e]} button`) as any;
			if (btn) {
				btn.click();
			}
		}
		this.group = this.isMonthly()
			? null
			: { enableCompactView: false, resources: ['Departments', 'Consultants'] };
	}

	getTechniciens() {
		this.processing = true;
		this.filter = {
			roleIds: this.listRole,
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 10,
			OrderBy: 'firstName',
			SortDirection: 'Descending',
			ignorePagination: true
		};
		this.service.getAllPagination(ApiUrl.User, this.filter)
			.subscribe(res => {
				this.processing = false;
				this.techniciens = res.value;
				this.filterlistTechnicien = this.formaterTechnicien(this.techniciens);
				this.technicienId = this.filterlistTechnicien.map(element => element.id);
				this.technicienId.push(0);
				this.listeTechniciens = res.value.map(element => {
					return { Text: element.lastName, Id: element.id, GroupId: UserProfile.technicien, Color: '', Designation: element.registrationNumber }
				});
			}, err => {
			});
	}

	formaterTechnicien(list: User[]): ngSelectModel[] {
		const listTechnicien: ngSelectModel[] = [];
		list.forEach((technicien, index) => {
			listTechnicien.push({ id: technicien.id, name: technicien.lastName, disabled: false });
		});
		return listTechnicien;
	}
	getRole(): Promise<void> {
		this.processing = true;
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.role).subscribe(async res => {
				this.processing = false
				this.role = res.value;
				const idUserTechnicien = this.role.filter(x => x.type === this.userProfile.technicien)[0].id;
				const idUserTechnicienChantier = this.role.filter(x => x.type === this.userProfile.technicienChantier)[0].id;
				const idUserTechnicienMaintenance = this.role.filter(x => x.type === this.userProfile.technicienMaintenance)[0].id;
				if (idUserTechnicien !== undefined) {
					this.listRole.push(idUserTechnicien)
				}
				if (idUserTechnicienChantier !== undefined) {
					this.listRole.push(idUserTechnicienChantier)
				}
				if (idUserTechnicienMaintenance !== undefined) {
					this.listRole.push(idUserTechnicienMaintenance)
				}
			}, err => {

			}, () => {
				reslove();
			});
		});
	}
	getFilters() {
		const list = this.technicienId.filter(x => x !== 0);
		return {
			'externalPartnerId': '',
			'techniciansId': this.technicienId.length === 0 || this.technicienId == null ? [] : list,
			'workshopId': '',
			'status': [StatutFicheIntervention.Realisee, StatutFicheIntervention.Facturee, StatutFicheIntervention.Planifiee],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference',
			'ignorePagination': true

		};
	}

	getFiltersMission() {
		const list = this.technicienId.filter(x => x !== 0);
		return {
			'technicianIds': this.technicienId.length === 0 || this.technicienId == null ? [] : list,
			'clientId': '',
			'types': [],
			'status': [],
			'kinds': [],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference',
			'ignorePagination': true

		};
	}

	addDays(date, days) {
		const copy = new Date(Number(date))
		copy.setDate(date.getDate() + days)
		return copy
	}

	// generateAvatar = () => this.user ? `${this.user.lastName[0]}${this.user.firstName[0]}` : '';
	genereteNameTechnicien(user) {
		return `${user.lastName[0]}${user.firstName[0]}`
	}
	async getTaches(kind?) {
		this.processing = true;
		this.filterMission = !kind ? this.getFiltersMission() : this.getFilterKindOption(kind);
		this.service.getAllPagination(ApiUrl.Mission, this.filterMission).subscribe(res => {
			this.tasks = res.value;
			if (this.tasks.length === 0) {
				this.dataSource(this.data);
				this.processing = false;
			} else {
				this.tasks.forEach(async element => {
					const adress = element.client == null || element.client.address == null || element.client.address.city === null ? '' : ' (' + element.client.address.city + ')';
					const clientNam = element.client == null || element.client.name == null ? '' : element.client.name;
					this.data.push({
						Id: element.id,
						Subject: clientNam + adress + element.object + ' ,' + `${element.technician.lastName} ${element.technician.firstName}`,
						Name: element.object,
						StartTime: new Date(element.startingDate),
						EndTime: this.addDays(new Date(element.startingDate), 0),
						Description: element.object, // objet
						DepartmentID: null,
						ConsultantID: null,
						DepartmentName: '',
						customClass: this.getMisionKind(element.missionKind),
						CategoryColor: this.getMisionKindColor(element.missionKind),

					});
					this.dataSource(this.data);
				});
				this.processing = false;
			}
		});
	}

	getFilterKindOption(kind) {
		const res = this.getFiltersMission();
		res.kinds.push(kind);
		return res;
	}

	getMisionKind(type) {
		if (type === MissionKind.Appointment) { return 'cust-appointment--evenements'; }
		if (type === MissionKind.Call) { return 'cust-appointment--rappels'; }
		if (type === MissionKind.TechnicianTask) { return 'cust-appointment--taches'; }
	}

	getMisionKindColor(type) {
		if (type === MissionKind.Appointment) { return ' #38b475'; }
		if (type === MissionKind.Call) { return '#feb969'; }
		if (type === MissionKind.TechnicianTask) { return '#47a2c1'; }
	}

	ifupdate(customClass) {
		return customClass === 'cust-appointment--evenements' ||
			customClass === 'cust-appointment--rappels' || customClass === 'cust-appointment--taches'
	}


	getFicheInterventionMaintenanceList(): Promise<void> {
		const list = this.technicienId.filter(x => x !== 0)
		this.processing = true;
		this.filter = {
			techniciansId: this.technicienId.length === 0 || this.technicienId == null ? [] : list,
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 10,
			OrderBy: 'reference',
			SortDirection: 'Descending',
			ignorePagination: true
		};
		return new Promise((reslove, reject) => {
			this.service.getAllPagination(ApiUrl.FicheInterventionMaintenance, this.filter).subscribe(res => {
				this.ficheInterventionMaintenanceList = res.value as FicheInterventionMaintenance[];
				// this.data = [];
				if (this.ficheInterventionMaintenanceList.length === 0) {
					this.dataSource(this.data);
					this.processing = false;
				} else {
					this.ficheInterventionMaintenanceList.forEach(async element => {
						this.data.push({
							Id: element.id,
							Subject: element.client + ' ' + (!element.clientCity || element.clientCity === null ? '' : '(' + element.clientCity + ') , ') + element.technician,
							Name: element.client,
							StartTime: new Date(element.startDate),
							EndTime: new Date(element.endDate),
							Description: element.purpose, // objet
							DepartmentID: null,//UserProfile.technicien,
							ConsultantID: null,//element.technicianId,
							DepartmentName: element.addressIntervention, // this.jsonParse(element.adresseIntervention).designation,
							customClass: 'cust-appointment--interventionsMaintenance',
							// RecurrenceRule: 'FREQ=WEEKLY;INTERVAL=1;BYDAY=FR',
							CategoryColor: '#05658d',
							// CategoryColor: '#05658d'// changecolorEvent
						});
						this.dataSource(this.data);
						reslove();
					});
					this.processing = false;
				}
			});
		});
	}
	getFicheInterventionChantierList(): Promise<void> {
		this.processing = true;
		return new Promise((reslove, reject) => {
			this.service.getAllPagination(ApiUrl.FicheIntervention, this.getFilters()).subscribe(res => {
				this.ficheInterventionMaintenanceList = res.value as OperationSheet[];
				// this.data = [];
				if (this.ficheInterventionMaintenanceList.length === 0) {
					this.dataSource(this.data);
					this.processing = false;
				} else {
					this.ficheInterventionMaintenanceList.forEach(async element => {
						let listTechnicienName = '';
						element.technicians.forEach(technicien => {
							listTechnicienName += ' ,' + this.genereteNameTechnicien(technicien);
						});
						this.data.push({
							Id: element.id,
							Subject: element.client + ' ' + (!element.clientCity || element.clientCity === null ? '' : '(' + element.clientCity + ') , ') + listTechnicienName,
							Name: element.client, // client name
							StartTime: new Date(element.startDate),
							EndTime: new Date(element.endDate),
							Description: element.purpose,
							DepartmentID: null,
							ConsultantID: null, // element.idTechnicien,
							customClass: 'cust-appointment--interventionsChantier',
							CategoryColor: '#f14f63',

						});
						this.dataSource(this.data);
					});
					this.processing = false;
				}
				reslove();
			});
		});
	}

	onTechnicienReset(e: MouseEvent): void {
		e.stopPropagation();
		this.technicienId = this.filterlistTechnicien.map(e => e.id) as any;
		this.refreshAgenda();
	}

	onTechnicienAll(): void {
		this.technicienId = this.filterlistTechnicien.slice(0).map(e => e.id) as any;
		this.refreshAgenda();
	}

	tosslePerOne(all) {
		if (this.allSelected.selected) {
			this.allSelected.deselect();
			this.refreshAgenda();
			return false;
		} else if (this.technicienId.length === this.filterlistTechnicien.length) {
			this.allSelected.select();
			this.refreshAgenda();
		} else {
			this.refreshAgenda();
		}
	}

	toggleAllSelection() {
		if (this.allSelected.selected) {
			this.technicienId = [...this.filterlistTechnicien.map(item => item.id), 0];
			this.refreshAgenda();
		} else {
			this.technicienId = [];
			this.data = [];
			this.dataSource(this.data);

		}
	}

	refreshAgenda() {
		this.data = [];
		if ((this.ficheType == null || this.ficheType.length === 0) || this.technicienId.length === 0) {
			this.dataSource(this.data);
		} else if (this.ficheType != null && this.ficheType.length > 0 && this.technicienId.length > 0) {
			if (this.ficheType.includes(this.agendaTypeFilter.InterventionsMaintenance)) {
				this.getFicheInterventionMaintenanceList();
			}
			if (this.ficheType.includes(this.agendaTypeFilter.InterventionsChantier)) {
				this.getFicheInterventionChantierList();
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Taches)) {
				this.getTaches(MissionKind.TechnicianTask);
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Evenements)) {
				this.getTaches(MissionKind.Appointment);
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Rappels)) {
				this.getTaches(MissionKind.Call);
			}
		} else {
			this.getFicheInterventionMaintenanceList();
			this.getFicheInterventionChantierList();
			this.getTaches();
		}
		if (this.scheduleObj) {
			// this.scheduleObj.refreshEvents();
		}
	}

	/**data source for event setting */
	dataSource(data) {
		if (data.length === 0) {
			this.scheduleObj.eventSettings.dataSource = [];
		} else {
			this.scheduleObj.eventSettings.dataSource = data;
		}
	}
	//#region calendar
	//#endregion
	onItemDrag(event: any): void {
		if (this.scheduleObj.isAdaptive) {
			const classElement: HTMLElement = this.scheduleObj.element.querySelector('.e-device-hover');
			if (classElement) {
				classElement.classList.remove('e-device-hover');
			}
			if (event.target.classList.contains('e-work-cells')) {
				addClass([event.target], 'e-device-hover');
			}
		}
		if (document.body.style.cursor === 'not-allowed') {
			document.body.style.cursor = '';
		}
		if (event.name === 'nodeDragging') {
			const dragElementIcon: NodeListOf<HTMLElement> =
				document.querySelectorAll('.e-drag-item.treeview-external-drag .e-icon-expandable');
			for (let i = 0; i < dragElementIcon.length; i++) {
				dragElementIcon[i].style.display = 'none';
			}
		}
	}
	onActionBegin(event: ActionEventArgs): void {

		if (event.requestType === 'eventCreate' && this.isTreeItemDropped) {
			const treeViewdata: { [key: string]: Object }[] = this.treeObj.fields.dataSource as { [key: string]: Object }[];
			const filteredPeople: { [key: string]: Object }[] =
				treeViewdata.filter((item: any) => item.Id !== parseInt(this.draggedItemId, 10));
			this.treeObj.fields.dataSource = filteredPeople;
			const elements: NodeListOf<HTMLElement> = document.querySelectorAll('.e-drag-item.treeview-external-drag');
			for (let i = 0; i < elements.length; i++) {
				remove(elements[i]);
			}
		}
	}

	onDataBound(): void {
		this.scheduleObj.scrollTo('7:00');
	}


	/**
	  *
	  * @param args get color for event
	  */
	applyCategoryColor(args: EventRenderedArgs): void {
		const categoryColor: string = args.data.CategoryColor as string;
		if (!args.element || !categoryColor) {
			return;
		}
		if (this.scheduleObj.currentView === 'Agenda') {
			(args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
		} else {
			args.element.style.backgroundColor = categoryColor;
		}
	}
	onPopupOpen(args: PopupOpenEventArgs): void {
		if (args.type === 'QuickInfo') {
			// QuickInfo for event  vide
			if (args.target.classList.contains('e-work-cells') || args.target.classList.contains('e-header-cells')) {
				this.scheduleObj.quickPopup.quickPopupHide(true);
				args.cancel = true;
			} else if (args.target.classList.contains('e-appointment')) {
				// QuickInfo for event
				(<HTMLElement>args.element).style.boxShadow = `1px 2px 5px 0 ${(<HTMLElement>args.target).style.backgroundColor}`;
			}
		}
		if (args.type === 'Editor') {
			const dataArgs: any = args.data;
			if (args.target === undefined || (dataArgs.Name === undefined && args.target.classList.contains('e-appointment'))) {
			} else {
				this.scheduleObj.quickPopup.quickPopupHide(true);
				args.cancel = true;
			}
		}
	}

	onEventClick(args) {
	}
	/**
	 * Disabel cell click
	 */
	onCellClick(args) {
		if (!args.event.target.classList.contains('e-more-indicator')) {
			args.cancel = true;
			DialogHelper.openDialog(
				this.dialog,
				AgendaFormComponent,
				'',
				{ hasBackdrop: true, dateSelectd: args.startTime }
			).subscribe(async response => {
				if (response) {
					this.router.navigate(['/agendaglobal', { refresh: (new Date).getTime() }]);
					this.refreshAgenda();
				}
			});
		}

	}

	update(data) {
		this.service.getById(ApiUrl.Mission, data.Id).subscribe(response => {
			if (response) {
				const res = response.value;
				DialogHelper.openDialog(
					this.dialog,
					AgendaFormComponent,
					'',
					{ hasBackdrop: true, res: res }
				).subscribe(async response => {
					if (response) {
						this.router.navigate(['/agendaglobal', { refresh: (new Date).getTime() }]);
						this.refreshAgenda();
					}
				});
			}
		});
	}


	DeleteMission(data) {
		let text = null;
		if (data.customClass === 'cust-appointment--evenements') {
			text = this.translate.instant('list.deleteEvenements')
		}
		if (data.customClass === 'cust-appointment--rappels') {
			text = this.translate.instant('list.deleteRappels')

		}
		if (data.customClass === 'cust-appointment--taches') {
			text = this.translate.instant('list.deleteTache')

		}

		swal({
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		}).then(isConfirm => {
			if (isConfirm) {
				//
				///api/Mission/{id}/Delete
				//	await this.service.update(ApiUrl.Mission, this.data.res, this.data.res.id).toPromise();

				this.service.delete(ApiUrl.Mission, data.Id).subscribe(res => {
					if (res) {
						swal(text.success, '', 'success');
						// this.FicheInterventions.value = this.FicheInterventions.value.filter(x => x.id !== id);
						// this.refreshFicheIntervention();
						// this.rerender();
						this.refreshAgenda();
					} else {
						swal(text.error, '', 'error');
					}
				});
			} else {
				toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		});

	}

	isTask(data) {
		return data.customClasstrue === 'cust-appointment--taches';
	}

	oncellDoubleClick(args) {
		args.cancel = true;
	}
	public onCloseClick(): void {
		this.scheduleObj.quickPopup.quickPopupHide();
	}

	navigateToDetailFicheIntervention(data) {
		if (data.customClass === 'cust-appointment--interventionsMaintenance') {
			localStorage.setItem(LocalElements.docType, ApiUrl.MaintenanceOperationSheet);
			this.router.navigate(['/planification/detail', data.Id])
		}
		if (data.CategoryColor === '#ED217C') {
			this.router.navigate(['/agendaglobal/detail', data.Id])
		}
		if (data.customClass === 'cust-appointment--interventionsChantier') {
			localStorage.setItem(LocalElements.docType, ApiUrl.FicheIntervention)
			this.router.navigate(['/ficheintervention/detail', data.Id])
		}
		if (data.customClass === 'cust-appointment--taches') {
			this.dateils(data)
		}
		if (data.customClass === 'cust-appointment--evenements') {
			this.dateils(data)
		}
		if (data.customClass === 'cust-appointment--rappels') {
			this.dateils(data)
		}
	}


	dateils(data) {
		this.service.getById(ApiUrl.Mission, data.Id).subscribe(response => {
			if (response) {
				const res = response.value;
				DialogHelper.openDialog(
					this.dialog,
					AgendaFormComponent,
					'',
					{ hasBackdrop: false, res: res, readOnly: true }
				).subscribe(async response => {
					if (response) {
						this.router.navigate(['/agendaglobal', { refresh: (new Date).getTime() }]);
						this.refreshAgenda();
					}
				});
			}
		});
	}

	isMonthly = () => this.scheduleObj && this.scheduleObj.viewIndex === 2;

	traductionCalendarToFranch() {
		loadCldr(
			require('./traduction/numberingSystems.json'),
			require('./traduction/main/fr/ca-gregorian.json'),
			require('./traduction/main/fr/currencies.json'),
			require('./traduction/main/fr/numbers.json'),
			require('./traduction/main/fr/timeZoneNames.json')
		);
		L10n.load({
			'fr': {
				'schedule': {
					'day': 'journée',
					'week': 'La semaine',
					'workWeek': 'Semaine de travail',
					'month': 'Mois',
					'agenda': 'Ordre du jour',
					'weekAgenda': 'Agenda de la semaine',
					'workWeekAgenda': 'Agenda de la semaine de travail',
					'monthAgenda': 'Agenda du mois',
					'today': 'Aujourd\'hui',
					'noEvents': 'Pas d\'événements',
					'emptyContainer': 'Aucun événement n\'est prévu ce jour-là.',
					'allDay': 'Toute la journée',
					'start': 'Début',
					'end': 'Fin',
					'more': 'plus',
					'close': 'Fermer',
					'cancel': 'Annuler',
					'noTitle': '(Pas de titre)',
					'delete': 'Effacer',
					'deleteEvent': 'Supprimer un événement',
					'deleteMultipleEvent': 'Supprimer plusieurs événements',
					'selectedItems': 'Articles sélectionnés',
					'deleteSeries': 'Supprimer la série',
					'edit': 'modifier',
					'editSeries': 'Modifier la série',
					'editEvent': 'Modifier l\'événement',
					'createEvent': 'Créer',
					'subject': 'Assujettir',
					'addTitle': 'Ajouter un titre',
					'moreDetails': 'Plus de détails',
					'save': 'sauvegarder',
					'editContent': 'Voulez-vous modifier uniquement cet événement ou une série entière?',
					'deleteRecurrenceContent': 'Voulez-vous supprimer uniquement cet événement ou une série entière?',
					'deleteContent': 'Êtes-vous sûr de vouloir supprimer cet événement?',
					'deleteMultipleContent': 'Êtes-vous sûr de vouloir supprimer les événements sélectionnés?',
					'newEvent': 'Ajouter fiche d\'intervention maintenance',
					'title': 'Titre',
					'location': 'Emplacement',
					'description': 'La description',
					'timezone': 'Fuseau horaire',
					'startTimezone': 'Début du fuseau horaire',
					'endTimezone': 'Fin du fuseau horaire',
					'repeat': 'Répéter',
					'saveButton': 'sauvegarder',
					'cancelButton': 'Annuler',
					'deleteButton': 'Effacer',
					'recurrence': 'Récurrence',
					'wrongPattern': 'Le modèle de récurrence n\'est pas valide.',
					'seriesChangeAlert': 'Les modifications apportées à des instances spécifiques de cette série seront annulées et ces événements correspondront à nouveau à la série.',
					'createError': 'La durée de l\'événement doit être plus courte que sa fréquence. Raccourcissez la durée ou modifiez le modèle de récurrence dans l\'éditeur d\'événement de récurrence.',
					'recurrenceDateValidation': 'Certains mois ont moins que la date sélectionnée. Pour ces mois, l\'événement se produira à la dernière date du mois.',
					'sameDayAlert': 'Deux occurrences du même événement ne peuvent pas se produire le même jour.',
					'editRecurrence': 'Modifier la récurrence',
					'repeats': 'Répète',
					'alert': 'Alerte',
					'startEndError': 'La date de fin sélectionnée se produit avant la date de début.',
					'invalidDateError': 'La valeur de date saisie est invalide.',
					'ok': 'D\'accord',
					'occurrence': 'Occurrence',
					'series': 'Séries',
					'previous': 'précédent',
					'next': 'Prochain',
					'timelineDay': 'Journée',
					'timelineWeek': 'Semaine',
					'timelineWorkWeek': 'Semaine',
					'timelineMonth': 'Mois Chronologique'
				},
				'recurrenceeditor': {
					'none': 'Aucun',
					'daily': 'du quotidien',
					'weekly': 'Hebdomadaire',
					'monthly': 'Mensuel',
					'month': 'Mois',
					'yearly': 'Annuel',
					'never': 'Jamais',
					'until': 'Jusqu\'à',
					'count': 'Compter',
					'first': 'Premier',
					'second': 'Seconde',
					'third': 'Troisième',
					'fourth': 'Quatrième',
					'last': 'Dernier',
					'repeat': 'Répéter',
					'repeatEvery': 'Répéter tous les',
					'on': 'Répéter sur',
					'end': 'Fin',
					'onDay': 'journée',
					'days': 'Journées)',
					'weeks': 'Semaines)',
					'months': 'Mois)',
					'years': 'Années)',
					'every': 'chaque',
					'summaryTimes': 'fois)',
					'summaryOn': 'sur',
					'summaryUntil': 'jusqu\'à',
					'summaryRepeat': 'Répète',
					'summaryDay': 'journées)',
					'summaryWeek': 'semaines)',
					'summaryMonth': 'mois)',
					'summaryYear': 'années)',
					'monthWeek': 'Mois Semaine',
					'monthPosition': 'Position du mois',
					'monthExpander': 'Mois Expander',
					'yearExpander': 'Année Expander',
					'repeatInterval': 'Intervalle de répétition'

				},
				'dropdowns': {
					'noRecordsTemplate': 'Aucun enregistrement trouvé',
					'actionFailureTemplate': 'Modèle d\'échec d\'action',
					'overflowCountTemplate': '+${count} plus..',
					'totalCountTemplate': '${count} choisi'
				}
			}
		});
	}
}
