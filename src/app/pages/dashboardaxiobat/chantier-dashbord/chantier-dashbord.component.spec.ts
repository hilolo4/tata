import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChantierDashbordComponent } from './chantier-dashbord.component';

describe('ChantierDashbordComponent', () => {
  let component: ChantierDashbordComponent;
  let fixture: ComponentFixture<ChantierDashbordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChantierDashbordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChantierDashbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
