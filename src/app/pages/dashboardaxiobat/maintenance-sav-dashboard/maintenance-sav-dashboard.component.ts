import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import * as Chart from 'chart.js';
import { ngSelectModel } from 'app/pages/planification/app-schedule/app-schedule.component';
import { NgSelectComponent } from '@ng-select/ng-select';

import { User } from 'app/Models/Entities/User';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { StatutContratEntretien } from 'app/Enums/Statut/StatutContratEntretien.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { DashbordContratEntretien, ContratParStatut, FicheInterventionParType, ContratParDate, TechnicienParHeure, ContratDetail } from 'app/Models/Entities/Maintenance/DashboardMaintenanceSav';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';
declare var toastr: any;

@Component({
	selector: 'app-maintenance-sav-dashboard',
	templateUrl: './maintenance-sav-dashboard.component.html',
	styleUrls: ['./maintenance-sav-dashboard.component.scss']
})
export class MaintenanceSavDashboardComponent implements OnInit {
	@ViewChild('baseChart') baseChart: ElementRef;
	// tslint:disable-next-line:no-input-rename
	@Input('dataMaintenance') dataMaintenance = false;
	idClient: [] = [];
	idTechnicien: [] = [];
	idGroupe: [] = [];
	dureEcheance = 1;
	nbrContratParStatut = 0;
	nbrContratActive = 0;
	nbrContratTermine = 0;
	dashboardContrat: DashbordContratEntretien = null;
	contratParStatut: ContratParStatut[] = [];
	ficheInterventionParType: FicheInterventionParType[] = [];
	contratParDate: ContratParDate = null;
	technicienParHeure: TechnicienParHeure[] = [];
	top10ContratParCA: ContratDetail[] = [];
	nbrContratParDate = 0;
	nbrficheinterventionType = 0;
	nbrficheinterventionsav = 0;
	nbrficheinterventionentretien = 0;
	periode = PeriodType.None;
	PeriodType: typeof PeriodType = PeriodType;
	userProfil: typeof UserProfile = UserProfile;
	dateMinimal
	dateMaximal
	dateLang
	listClients: Client[] = null;
	listGroupes: Groupe[] = null;
	listTechniciens: User[] = null;
	listTechnicien = [];
	filterlistGroupe: ngSelectModel[] = [];
	@ViewChild('selecteGroupes') selecteGroupes: NgSelectComponent;

	//#region chart horizontalBar

	data: { labels: string, datasets: number }[] = [];

	chart: any;
	/**
  * @description récuperer la liste des techniciens de type 'technicien' & 'technicien maintenance'
  */
	listTechniciensFilter = [];
	@ViewChild('selecteTechniciens') selecteTechniciens: NgSelectComponent;

	filterlistTechnicien: ngSelectModel[] = [];

	filterlistClient: ngSelectModel[] = [];

	@ViewChild('selecteClients') selecteClients: NgSelectComponent;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	ngOnInit() {
		this.dashboardMaintenanceSav();
	}

	filter() {
		this.dashboardMaintenanceSav();
	}

	dashboardMaintenanceSav() {

		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;

		const res = {
			// this.idTechnicien, this.idGroupe, this.dureEcheance,
			externalPartnerId: this.idClient,
			dateStart: this.dateMinimal,
			dateEnd: this.dateMaximal,
			periodType: this.periode,
		}

			this.service.create(ApiUrl.analytic + '/Purchase', res).subscribe(res => {
				this.dashboardContrat = res.value;
				this.contratParStatut = this.dashboardContrat.contratParStatut;
				this.contratParDate = res.contratParDate;
				this.nbrContratParDate = res.contratParDate.nbrContrat;
				this.ficheInterventionParType = res.ficheInterventionParType;
				this.nbrficheinterventionsav = res.ficheInterventionParType.
				filter(x => x.type === TypeInterventionMaintenance.AfterSalesService)[0].nbrIntervention;
				this.nbrficheinterventionentretien = res.ficheInterventionParType.
				filter(x => x.type === TypeInterventionMaintenance.Maintenance)[0].nbrIntervention;
				this.nbrficheinterventionType = this.nbrficheinterventionsav + this.nbrficheinterventionentretien;
				this.top10ContratParCA = res.top10ContratParCA;
				this.contraParStatut(this.contratParStatut);
				this.technicienParHeure = res.technicienParHeure;
				try {
					this.dataChart(this.technicienParHeure).then((val) => {
						this.data = val as any;
						setTimeout(() => {
							this.showChart();
						}, 0);
					});
				} finally {
					// this.loader.hide();
				}

			})

	}


	contraParStatut(contratParStatut: ContratParStatut[]) {
		this.nbrContratActive = contratParStatut.filter(x => x.status === StatutContratEntretien.Encours)[0].nbrContrat;
		this.nbrContratTermine = contratParStatut.filter(x => x.status === StatutContratEntretien.Termine)[0].nbrContrat;
		this.nbrContratParStatut = this.nbrContratActive + this.nbrContratTermine != null ? this.nbrContratActive + this.nbrContratTermine : 0;
	}


	async dataChart(technicienParHeure: TechnicienParHeure[]) {
		return new Promise(async (resolve) => {
			const data = [];
			technicienParHeure.forEach(element => {
				data.push({ labels: element.detailsTechnicien.nom, datasets: element.nbrHeure });
			});
			// return classement result
			resolve(data.sort((a, b) => a.datasets > b.datasets ? -1 : 1));
		});
	}

	showChart() {
		if (this.chart) {
			this.chart.destroy();
		}

		this.chart = new Chart(this.baseChart.nativeElement, {
			type: 'horizontalBar',
			data: {
				labels: this.data.map(e => e.labels), // your labels array
				datasets: [
					{
						// label: this.labels,
						label: 'Technicien/Heures',
						data: this.data.map(e => e.datasets), // your data array
						backgroundColor: [
							'#8ce89e',
							'#ade2ff',
							'#99e4e4',
							'#74e5cc',
							'#5cc5c5',
							'#97d9ff',
							'#93a9cf',
							'#d19392',
							'#b9cd96',
							'#91c3d5'

						],
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					}
				]
			},
			options: {
				responsive: true,
				title: {
					// text: 'Technicien/Heures',
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	getTechniciens() {

		if (this.listTechniciens == null) {

			const filter = {
				roleIds: this.listTechnicien,
				SearchQuery: '',
				Page: 1,
				PageSize: 500,
				OrderBy: 'firstName',
				SortDirection: 'Descending',
				ignorePagination: true
			};

			//  this.processing = true;
			this.service.getAllPagination(ApiUrl.User, filter).subscribe(res => {
				this.listTechniciens = res.value;
				this.filterlistTechnicien = this.formaterTechnicien(this.listTechniciens);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				// this.processing = false;
				this.selecteTechniciens.isOpen = true;
			});
		}
	}




	getRole() {
		this.service.getAll(ApiUrl.role).subscribe(res => {
			const role = res.value;
			this.listTechnicien = [role.filter(x => x.type === this.userProfil.technicien)[0].id, role.filter(x => x.type ===
				this.userProfil.technicienMaintenance)[0].id];
		});
	}

	formaterTechnicien(list: User[]): ngSelectModel[] {
		const listTechnicien: ngSelectModel[] = [];
		list.forEach((technicien, index) => {
			listTechnicien.push({ id: technicien.id, name: technicien.firstName, disabled: false });
		});
		return listTechnicien;
	}
	getClients() {

		if (this.listClients == null) {
			//  this.processing = true;
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
				this.filterlistClient = this.formaterClient(this.listClients);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				// this.processing = false;
				this.selecteClients.isOpen = true;
			});
		}
	}

	formaterClient(list: Client[]) {
		const listClient: ngSelectModel[] = [];
		list.forEach((client, index) => {
			listClient.push({ id: client.id, name: client.firstName, disabled: false });
		});
		return listClient;
	}

	getGroupes() {

		if (this.listGroupes == null) {
			//  this.processing = true;
			this.service.getAll(ApiUrl.Groupe).subscribe(res => {
				this.listGroupes = res.value;
				this.filterlistGroupe = this.formaterGroupe(this.listGroupes);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteGroupes.isOpen = true;
			});
		}
	}

	formaterGroupe(list: Groupe[]) {
		const listGroupe = [];
		list.forEach((groupe, index) => {
			listGroupe.push({ id: groupe.id, name: groupe.name, disabled: false });
		});
		return listGroupe;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}
}
