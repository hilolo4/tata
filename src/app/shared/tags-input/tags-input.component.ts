import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'tags-input',
	templateUrl: './tags-input.component.html',
	styleUrls: ['./tags-input.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TagsInputComponent),
			multi: true
		}
	]
})
export class TagsInputComponent implements OnInit, ControlValueAccessor {

	//#region Properties

	@Input()  tags = [];
	input = '';

	@Input() readonly = false;

	//#region endregion

	//#region Lifecycle

	constructor() { }

	ngOnInit(): void { }

	//#endregion

	//#region Events

	onEnter(e: KeyboardEvent): void {
		if (e.code.toLocaleLowerCase() === 'backspace' && this.input.trim().length === 0) {
			this.tags.pop();
		} else if (['enter', 'numpadenter'].includes(e.code.toLocaleLowerCase())) {
			if (this.input.trim().length > 0) {
				this.addTag(this.input)
			}
		}
	}

	onDelete(index: number): void {
		if (!this.tags || !Array.isArray(this.tags)) {
			this.tags = [];
		}

		this.tags.splice(index, 1);
	}

	writeValue(obj: any): void {
		this.tags = obj;
	}

	registerOnChange(fn: any): void {
		this.propagateChange = fn;
	}

	registerOnTouched(fn: any): void { }

	setDisabledState?(isDisabled: boolean): void { }

	//#endregion

	//#region Methods

	addTag(tag: string): void {
		if (!this.tags || !Array.isArray(this.tags)) {
			this.tags = [];
		}

		this.input = '';
		this.tags.push(tag);
		this.propagateChange(this.tags);
	}

	propagateChange = (tags: string[]) => { };

	//#endregion
}
