import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagsInputComponent } from './tags-input.component';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material';

@NgModule({
	declarations: [TagsInputComponent],
	imports: [
		CommonModule,
		FormsModule,
		MatIconModule
	],
	exports: [TagsInputComponent]
})
export class TagsInputModule { }
