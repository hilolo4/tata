import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'config-dialog',
	templateUrl: './config-dialog.component.html'
})
export class ConfigDialogComponent implements OnInit {

	title: string;
	message: string;
	id: string;

	constructor(
		public dialogRef: MatDialogRef<ConfigDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data
	) {
		// Update view with given values
		this.title = data.title;
		this.message = data.message;
		this.id = data.id;
	}

	ngOnInit() { }

}
