import { User } from 'app/Models/Entities/User';
import { LocalElements } from './local-elements';
import { FormControl } from '@angular/forms';


export class Constants {

	public static getUser(): User {
		return localStorage.getItem(LocalElements.PDB_USER) != null ? JSON.parse(localStorage.getItem(LocalElements.PDB_USER)) as User : null
	}

	public static deserializeJSON(jsonObj) {
		return jsonObj === '' || jsonObj == null ? [] : JSON.parse(jsonObj) as any[];
	}

	public static phoneNumberValidator(control: FormControl): { [key: string]: any } | null {
		const valid = /^\d+$/.test(control.value)
		return (control.value.length === 0 || valid) ? null : { invalidNumber: { valid: false, value: control.value } }
	}

	static getContentType(Extension): { Extension: string, Mime: string } | null {
		const FileTypes: { Extension: string, Mime: string }[] = [
			{ Extension: 'aac', Mime: 'audio/aac' },
			{ Extension: 'abw', Mime: 'application/x-abiword' },
			{ Extension: 'arc', Mime: 'application/octet-stream' },
			{ Extension: 'avi', Mime: 'video/x-msvideo' },
			{ Extension: 'azw', Mime: 'application/vnd.amazon.ebook' },
			{ Extension: 'bin', Mime: 'application/octet-stream' },
			{ Extension: 'bz', Mime: 'application/x-bzip' },
			{ Extension: 'bz2', Mime: 'application/x-bzip2' },
			{ Extension: 'csh', Mime: 'application/x-csh' },
			{ Extension: 'css', Mime: 'text/css' },
			{ Extension: 'csv', Mime: 'text/csv' },
			{ Extension: 'doc', Mime: 'application/msword' },
			{ Extension: 'docx', Mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' },
			{ Extension: 'eot', Mime: 'application/vnd.ms-fontobject' },
			{ Extension: 'epub', Mime: 'application/epub+zip' },
			{ Extension: 'gif', Mime: 'image/gif' },
			{ Extension: 'htm', Mime: 'text/html' },
			{ Extension: 'html', Mime: 'text/html' },
			{ Extension: 'ico', Mime: 'image/x-icon' },
			{ Extension: 'ics', Mime: 'text/calendar' },
			{ Extension: 'jar', Mime: 'application/java-archive' },
			{ Extension: 'jpeg', Mime: 'image/jpeg' },
			{ Extension: 'jpg', Mime: 'image/jpeg' },
			{ Extension: 'js', Mime: 'application/javascript' },
			{ Extension: 'json', Mime: 'application/json' },
			{ Extension: 'mid', Mime: 'audio/midi' },
			{ Extension: 'midi', Mime: 'audio/midi' },
			{ Extension: 'mpeg', Mime: 'video/mpeg' },
			{ Extension: 'mpkg', Mime: 'application/vnd.apple.installer+xml' },
			{ Extension: 'odp', Mime: 'application/vnd.oasis.opendocument.presentation' },
			{ Extension: 'ods', Mime: 'application/vnd.oasis.opendocument.spreadsheet' },
			{ Extension: 'odt', Mime: 'application/vnd.oasis.opendocument.text' },
			{ Extension: 'oga', Mime: 'audio/ogg' },
			{ Extension: 'ogv', Mime: 'video/ogg' },
			{ Extension: 'ogx', Mime: 'application/ogg' },
			{ Extension: 'otf', Mime: 'font/otf' },
			{ Extension: 'png', Mime: 'image/png' },
			{ Extension: 'pdf', Mime: 'application/pdf' },
			{ Extension: 'ppt', Mime: 'application/vnd.ms-powerpoint' },
			{ Extension: 'pptx', Mime: 'application/vnd.openxmlformats-officedocument.presentationml.presentation' },
			{ Extension: 'rar', Mime: 'application/x-rar-compressed' },
			{ Extension: 'rtf', Mime: 'application/rtf' },
			{ Extension: 'sh', Mime: 'application/x-sh' },
			{ Extension: 'svg', Mime: 'image/svg+xml' },
			{ Extension: 'swf', Mime: 'application/x-shockwave-flash' },
			{ Extension: 'tar', Mime: 'application/x-tar' },
			{ Extension: 'tif', Mime: 'image/tiff' },
			{ Extension: 'tiff', Mime: 'image/tiff' },
			{ Extension: 'ts', Mime: 'application/typescript' },
			{ Extension: 'ttf', Mime: 'font/ttf' },
			{ Extension: 'txt', Mime: 'text/plain' },
			{ Extension: 'vsd', Mime: 'application/vnd.visio' },
			{ Extension: 'wav', Mime: 'audio/x-wav' },
			{ Extension: 'weba', Mime: 'audio/webm' },
			{ Extension: 'webm', Mime: 'video/webm' },
			{ Extension: 'webp', Mime: 'image/webp' },
			{ Extension: 'woff', Mime: 'font/woff' },
			{ Extension: 'woff2', Mime: 'font/woff2' },
			{ Extension: 'xhtml', Mime: 'application/xhtml+xml' },
			{ Extension: 'xls', Mime: 'application/vnd.ms-excel' },
			{ Extension: 'xlsx', Mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' },
			{ Extension: 'xml', Mime: 'application/xml' },
			{ Extension: 'xul', Mime: 'application/vnd.mozilla.xul+xml' },
			{ Extension: 'zip', Mime: 'application/zip' },
			{ Extension: '3gp', Mime: 'video/3gpp' },
		];
		const result = FileTypes.filter(F => F.Extension.toLowerCase() === Extension.toLowerCase());
		return (result.length === 0) ? null : FileTypes.find(F => F.Extension.toLowerCase() === Extension.toLowerCase());
	}


}
