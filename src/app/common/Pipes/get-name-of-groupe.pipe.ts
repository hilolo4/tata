
import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

@Pipe({
	name: 'GetNameOfGroupe',
	pure: false
})
export class GetNameOfGroupePipe implements PipeTransform {

	private groupeName: any = null;
	private cachedIdGroupe = null;

	constructor(@Inject('IGenericRepository') private service: IGenericRepository<any>,
	) { }

	transform(groupeId: any, args?: any): any {

		if (groupeId !== this.cachedIdGroupe) {
			this.groupeName = null;
			this.cachedIdGroupe = groupeId;

			this.service.getById(ApiUrl.Groupe, groupeId).subscribe(groupe => {
				this.groupeName = groupe.value.name
			});
		}

		return this.groupeName;
	}

}
