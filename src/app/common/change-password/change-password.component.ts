import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var toastr: any;
declare var jQuery: any;
@Component({
	selector: 'change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

	changePasswordForm = this.formBuilder.group({
		password: ['', [Validators.required, Validators.minLength(5)]],
		confirmPassword: ['']
	}, { validator: this.checkPasswords })

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<ChangePasswordComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { userId: string },
		private formBuilder: FormBuilder, private translate: TranslateService) { }

	ngOnInit() {
	}

	checkPasswords(group: FormGroup) {
		const password = group.controls.password.value;
		const confirmPassword = group.controls.confirmPassword.value;
		return password === confirmPassword ? null : { notSame: true }
	}

	change() {
		if (this.changePasswordForm.valid) {
			const res = {
				'userId': this.data.userId,
				'password': this.changePasswordForm.value.password
			}
			this.service.updateAll(ApiUrl.User + '/Update/Password', res).subscribe(res => {
				if (res) {
					this.translate.get('changePassword').subscribe(text => {
						toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					});
					this.dialogRef.close();
				}
			});
		}
	}


	rest() {
		this.changePasswordForm.controls['password'].setValue(null);
		this.changePasswordForm.controls['confirmPassword'].setValue(null);
		this.dialogRef.close();
	}

}
