import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';

declare var jQuery: any;

@Component({
	selector: 'choix-chantier-popup',
	templateUrl: './choix-chantier.component.html',
	styleUrls: ['./choix-chantier.component.scss']
})
export class ChoixChantierComponent implements OnInit {

	idChantier
	idFournisseur
	@Input("chantiers") chantiers: Workshop[] = [];

	@Input("fournisseurs") fournisseurs: Supplier[] = null;
	@Output("onSearch") onSearch = new EventEmitter();
	@Input("id") idPop = "choixChantier"
	@Input("choixfournisseur") choixfournisseur: boolean = false

	constructor() { }

	ngOnInit() { }

	// Choisir chantier
	choisirChantier() {

		jQuery("#choixChantier").modal("hide");
		this.onSearch.emit({ idChantier: this.idChantier, idFournisseur: this.idFournisseur })

		this.idChantier = null;
		this.idFournisseur = null;
	}

}
