import { Component, OnInit, OnChanges, Input, Output, EventEmitter, Inject, ViewChild, ElementRef } from '@angular/core';
import { ArticleType } from 'app/Models/Entities/Commun/article-type';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { SortDirection } from '../../../Models/Model/filter-option';
import { fromEvent, pipe } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

declare var toastr: any;
declare var jQuery: any;
@Component({
	selector: 'select-produit-fournisseur',
	templateUrl: './select-produit.component.html',
	styleUrls: ['./select-produit.component.scss']
})
export class SelectProduitByFournisseurComponent implements OnChanges, OnInit {

	search = '';
	page = 1;
	totalPage = 1;
	produits: any[] = [];
	loadingFinished = false;
	selected: { product: any, quantity: number, remise: number, type: number }[] = [];
	articleType: ArticleType = new ArticleType();
	@ViewChild('searchInput') searchRef: ElementRef;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<SelectProduitByFournisseurComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { idFournisseur: string }) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getProduits();
	}

	ngOnChanges(): void { }

	
	ngAfterViewInit() {
		fromEvent(this.searchRef.nativeElement, 'keyup')
		.pipe(
			map((event: CustomEvent) => event.target['value'] ? event.target['value'].toLowerCase().trim() : ''),
			debounceTime(400),
			distinctUntilChanged()
		).subscribe(res => {
			this.searchProduit();
		})
	}

	getProduits() {
		this.selected = [];
		this.loadingFinished = false;
		this.service.getAllPagination(ApiUrl.Produit, {
			// ClassificationPrentId: this.Categorie,
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 10,
			OrderBy: 'reference',
			supplierId : this.data.idFournisseur,
			SortDirection: SortDirection.Descending
		}).subscribe(res => {
			this.produits = [...this.produits, ...res.value];
			this.produits = this.produits.filter((l) => !this.selected.some(e => e.product.id === l.id));
			this.loadingFinished = true;
			this.totalPage = res.pagesCount;
		});
	}

	searchProduit() {

		this.page = 1;
		this.produits = [];
		this.getProduits();
	}

	onScroll() {
		if (this.totalPage !== this.page) {
			this.page++;
			this.getProduits();
		}
	}

	checkElement(index, value) {
		this.selected.unshift({
			product: this.produits[index],
			quantity: value != null ? parseInt(value, 0) : 1,
			type: this.articleType.produit,
			remise: 0
		});
		this.produits.splice(index, 1);
	}

	IncheckElement(index) {
		const produitSelected = this.selected[index].product;
		this.produits.unshift(produitSelected as any);
		this.selected.splice(index, 1);
	}

	getCtaegoriId() {
		// this.selected.map(x => x.product['categorieId'] = x.product.category.id);
		this.selected.map(x => x.product['category'] = null);
	}
	addTmpArticle(index, value) {
		if (value != '') {
			this.selected.unshift({
				product: this.produits[index],
				quantity: value != null ? parseInt(value) : 1,
				remise: 0,
				type: 0,
			});
			this.produits.splice(index, 1);
		}
	}


	save() {
		if (this.selected.length == 0) {
			toastr.warning('text.serveur', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		} else {
			this.getCtaegoriId();
			this.selected.map(x => x.product.discount == (null || undefined) ? { 'value': 0, 'type': TypeValue.Amount } : x.product.discount);
			this.dialogRef.close(this.selected);
		}
	}



	changeQuantite(index, value, changed) {
		let produit = this.selected[index];
		if (value != null) { produit.quantity += value; }
		if (changed != null && changed != '') { produit.quantity = parseInt(changed); }
		if (produit.quantity <= 0) { produit.quantity = 1; }
	}

	removeTmpArticle(index) {
		this.produits.unshift(this.selected[index].product);
		this.selected.splice(index, 1);
	}

	initialization() {
		this.selected = [];
		this.search = '';
	}

	qte(produit) {
		return produit.quantity == undefined ? 0 : produit.quantity
	}


}
