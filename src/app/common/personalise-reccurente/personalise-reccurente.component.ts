import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MatDialogRef, MAT_DIALOG_DATA, MAT_DATE_FORMATS } from '@angular/material';
import { PeriodicityRecurringType, TypeTermine, DayOfWeek } from 'app/Enums/TypeRepetition.Enum';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/dateAdapter/date.adapter';
import { ReccurenteModel } from 'app/Models/Model/ReccurenteModel';


@Component({
	selector: 'app-personalise-reccurente',
	templateUrl: './personalise-reccurente.component.html',
	styleUrls: ['./personalise-reccurente.component.scss'],
	providers: [
		{
			provide: DateAdapter, useClass: AppDateAdapter
		},
		{
			provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
		}
	]
})
export class PersonaliseReccurenteComponent implements OnInit {

	get f() { return this.creationForm.controls; }


	typeRepetitionenum: typeof PeriodicityRecurringType = PeriodicityRecurringType;
	typeTermineenum: typeof TypeTermine = TypeTermine;
	typeTermine = 1;
	creationForm: any = null;
	typeRepetition = 1;
	serializedDate = new FormControl((new Date()).toISOString());
	typetermine = 1;
	matDatepicker;
	date;


	daysCheckBox: { label: any, value: Boolean, type: any }[] = [
		{ type: DayOfWeek.Sunday, label: 'D', value: false },
		{ type: DayOfWeek.Monday, label: 'L', value: false },
		{ type: DayOfWeek.Tuesday, label: 'M', value: false },
		{ type: DayOfWeek.Wednesday, label: 'M', value: false },
		{ type: DayOfWeek.Thursday, label: 'J', value: false },
		{ type: DayOfWeek.Friday, label: 'V', value: false },
		{ type: DayOfWeek.Saturday, label: 'S', value: false }
	];

	constructor(private dateAdapter: DateAdapter<Date>,
		public dialogRef: MatDialogRef<PersonaliseReccurenteComponent>,
		private formBuilder: FormBuilder,
		@Inject(MAT_DIALOG_DATA) public data: { listes: ReccurenteModel, readOnly: boolean }
	) {
		this.dateAdapter.setLocale('fr');
	}

	ngOnInit() {
		this.date = new Date();
		this.creationForm = this.buildCreationForm();
		if (this.data.listes != null) {
			this.setData();
		}
	}

	buildCreationForm(): FormGroup {
		return this.formBuilder.group({
			nbr_repetition: [1],
			type_repetition: [1, [Validators.required]],
			dateRepetition: [],
			jour: [12],
		});
	}

	setData() {
		if (this.data.listes.typeRepetition === this.typeRepetitionenum.EveryWeek) {
			this.daysCheckBox = [
				{ type: DayOfWeek.Sunday, label: 'D', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Sunday).length !== 0 },
				{ type: DayOfWeek.Monday, label: 'L', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Monday).length !== 0 },
				{ type: DayOfWeek.Tuesday, label: 'M', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Tuesday).length !== 0 },
				{ type: DayOfWeek.Wednesday, label: 'M', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Wednesday).length !== 0 },
				{ type: DayOfWeek.Thursday, label: 'J', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Thursday).length !== 0 },
				{ type: DayOfWeek.Friday, label: 'V', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Friday).length !== 0 },
				{ type: DayOfWeek.Saturday, label: 'S', value: this.data.listes.jourSemaine.filter(x => x === DayOfWeek.Saturday).length !== 0 }
			];
		}
		this.creationForm.controls['nbr_repetition'].setValue(this.data.listes.nbrRepetition);
		this.creationForm.controls['type_repetition'].setValue(this.data.listes.typeRepetition);
		this.creationForm.controls['jour'].setValue(this.data.listes.jour);

	}

	disableClick(value) {
		return value;
	}

	changer($event) {
		this.matDatepicker = $event;
	}
	close() {
		this.dialogRef.close(null);

	}
	save() {
		const formValue = this.creationForm.value;
		const result: ReccurenteModel = new ReccurenteModel();
		result.typeRepetition = +(formValue.type_repetition);
		result.nbrRepetition = formValue.nbr_repetition;
		if (+(formValue.type_repetition) === this.typeRepetitionenum.EveryWeek) {
			result.jourSemaine = this.getDaysSelected();
		} else {
			result.jourSemaine = [];
		}
		if (+(formValue.type_repetition) === this.typeRepetitionenum.EveryMonth) {
			result.jour = formValue.jour;
		}
		this.dialogRef.close(result);

	}

	selectDayCheckBox(index) {
		if (this.data.readOnly === true) { return false; }
		this.daysCheckBox[index].value = !this.daysCheckBox[index].value;

	}

	getDaysSelected() {

		const result = this.daysCheckBox.map(day => {
			if (day.value) {
				return day.type;
			}
		}).filter(x => x != null);

		return result;
	}

}
