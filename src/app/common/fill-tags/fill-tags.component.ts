import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-fill-tags',
	templateUrl: './fill-tags.component.html'
})
export class FillTagsComponent implements OnInit {

	@Input() tags: string[] = [];
	@Input() defaultValue: any;

	/**
	* the form group
	*/
	form: FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		public modal: NgbActiveModal,
	) {
		this.form = this.formBuilder.group({})
	}

	ngOnInit() {
		this.tags.forEach(tag => this.addFormControlItem(tag));
		this.fillDefaultValue(this.defaultValue);
	}

	/**
	* add form control item
	*/
	addFormControlItem = (tag: string): void => this.form.addControl(tag, new FormControl(null))

	/**
	* save data
	*/
	save () { this.modal.close(this.form.value); }

	getSignature(tags) {
		return tags === '#signature#';
	}

	/**
	 * set default data to tags
	 * @param data the date to set
	 */
	fillDefaultValue(data: any) {
		if (data != null) {
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					const control = this.form.get(key);
					if (control != null) { control.setValue(data[key]); }
				}
			}
		}
	}

}
