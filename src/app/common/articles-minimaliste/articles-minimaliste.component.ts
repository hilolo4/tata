import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { DelaiGaranties } from 'app/Enums/DelaiGaranties.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
declare var toastr: any;
declare var swal: any;
@Component({
	selector: 'articles-minimaliste',
	templateUrl: './articles-minimaliste.component.html',
	styleUrls: ['./articles-minimaliste.component.scss']
})
export class ArticlesMinimalisteComponent implements OnInit, OnChanges {

	@Input('data') data = null;
	@Input('load') load: { getData };
	@Input('readOnly') readOnly = false;
	@Input('retenueGarantieValue') retenueGarantieValue = 0;
	@Input('delaiGarantie') delaiGarantie = 12;

	delaiGarantiesEnum: typeof DelaiGaranties = DelaiGaranties;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	public form = null;
	totalTtc;
	retenueGarantie = false;
	totalHt;
	fileName;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder) {
	}

	ngOnInit() {
		this.createForm();
		this.getCouteVenteFromParamerage();
		this.getRetenueParamerage();
		if (this.data && this.isValid(this.data.orderDetails.holdbackDetails)) {
			this.retenueGarantieValue = this.data.orderDetails.holdbackDetails.holdback;
			this.delaiGarantie = this.data.orderDetails.holdbackDetails.warrantyPeriod;
			this.retenueGarantie = (this.retenueGarantieValue == null || this.retenueGarantieValue === 0) ? false : true;
		}
	}

	createForm() {
		this.form = this.fb.group({
			puc: [
				(this.data == null ? 0 : this.data.orderDetails.puc),
				[Validators.required]
			],
			prorata: [
				(this.data == null ? 0 : this.data.orderDetails.proportion),
				[Validators.required]
			],
			tva: [
				(this.data == null ? 0 : this.data.orderDetails.globalVAT_Value),
				[Validators.required]
			],
			nomber_heure: [
				(this.data == null ? 0 : this.data.orderDetails.productsPricingDetails.totalHours),
				[Validators.required]
			],
			cout_vente: [
				(this.data == null ? 0 : this.data.orderDetails.productsPricingDetails.hourlyCost),
				[Validators.required]
			],
			cout_materiel: [
				(this.data == null ? 0 : this.data.orderDetails.productsPricingDetails.materialCost),
				[Validators.required]
			],
			achat_materiel: [
				(this.data == null ? 0 : this.data.orderDetails.productsPricingDetails.achatsMateriels),
				[Validators.required]
			],
			devisExel: [
				(this.data == null ? null : this.data.orderDetails.productsDetails_File),
				[Validators.required]
			]
		});
		this.fileName = this.data == null ? '' : this.data.orderDetails.productsDetails_File.fileName;

		if (this.data !== null) {
			this.data['totalHt'] = this.TotalHt(this.data.orderDetails.productsPricingDetails.totalHours
				, this.data.orderDetails.productsPricingDetails.hourlyCost,
				this.data.orderDetails.productsPricingDetails.materialCost
			);
			this.data['totalTTC'] = this.TotalTtc(this.data.orderDetails.productsPricingDetails.totalHours
				, this.data.orderDetails.productsPricingDetails.hourlyCost,
				this.data.orderDetails.productsPricingDetails.materialCost,
				this.data.orderDetails.globalVAT_Value
			);
		}

	}

	isValid(data) { return data && data !== null }

	ngOnChanges() {
		if (this.load === undefined) { return; }
		this.load.getData = this.returnData.bind(this);
	}


	startUpload(event: FileList) {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString();
			this.form.controls['devisExel'].setValue(pieceJoin);
			this.fileName = file.name;
		}
	}

	get f() { return this.form.controls; }

	returnData(callBack) {
		if (this.form.valid) {
			callBack({
				data: this.form.value,
				retenueGarantie: this.retenueGarantie ? this.retenueGarantieValue : 0,
				totalHt: this.totalHt,
				totalTtc: this.totalTtc,
				delaiGarantie: this.delaiGarantie,
			}
			);
		} else {
			callBack(false);
		}
	}

	removeFile() {
		this.translate.get('file.delete').subscribe(text => {
			swal({
				title: 'Supprimer',
				text: '',
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.form.controls['devisExel'].setValue(null);
					swal(text.success, '', 'success');
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}

	downloadFile() {
		const pieceJointe = this.data.orderDetails.productsDetails_File;
		AppSettings.downloadBase64(pieceJointe.fileId, pieceJointe.fileName,
			pieceJointe.fileId.substring('data:'.length, pieceJointe.fileId.indexOf(';base64')), pieceJointe.fileType)
	}

	getRetenueParamerage() {
		this.service.getAll(ApiUrl.configDocument).subscribe(res => {
			const parametrage = JSON.parse(res);
			const defaultsValues = parametrage.filter(x => x.documentType === this.typeNumerotation.devis)[0];
			if (!this.readOnly && !this.retenueGarantie) {
				this.retenueGarantieValue = defaultsValues.Holdback;
			}
		});
	}


	getCouteVenteFromParamerage() {
		this.service.getAll(ApiUrl.configDefaultValue).subscribe(
			(res) => {
				const PrixParDefault = JSON.parse(res);
				this.form.controls['cout_vente'].setValue(PrixParDefault.salesPrice);
			});
	}

	TotalHt(numberHeure, cout_vente, cout_materiel) {
		const coutVente = this.data != null ? this.data.orderDetails.productsPricingDetails.hourlyCost : cout_vente;
		const totalHt = ((numberHeure * coutVente) + cout_materiel).toFixed(2)
		this.totalHt = totalHt;
		return totalHt;
	}

	TotalTtc(numberHeure, cout_vente, cout_materiel, tva) {
		const coutVente = this.data != null ? this.data.orderDetails.productsPricingDetails.hourlyCost : cout_vente;
		const totalHt = ((numberHeure * coutVente) + cout_materiel)
		this.totalTtc = ((totalHt * ((tva / 100) + 1))).toFixed(2);
		return this.totalTtc;
	}

}