import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RubriqueTagsComponent } from './rubrique-tags.component';

describe('RubriqueTagsComponent', () => {
  let component: RubriqueTagsComponent;
  let fixture: ComponentFixture<RubriqueTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RubriqueTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RubriqueTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
