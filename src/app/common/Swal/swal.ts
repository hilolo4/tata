declare var swal;

interface SwalTitles {
	title: string;
	question: string;
	cancel: string;
	confirm: string;
}

export enum SwalTypePopUp {
	error = 'error'
}

export class SwalUtils {

	/**
     * show confirm popup
     * @param titles the titles of popup
     * @param callbackConfirm the callback of confirm action
     */
	static confirm(titles: SwalTitles, callbackConfirm) {
		swal({
			title: titles.title,
			text: titles.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: titles.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: titles.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		}).then(isConfirm => { if (isConfirm) { callbackConfirm(); } });
	}

	/**
     * show popup information
     * @param title the title of popup
     * @param subtitle the subtitle of popup
     * @param type the type of pop
     */
	static popUpInformation(title: string, subtitle: string, type: SwalTypePopUp): void {
		swal(title, subtitle, type);
	}

}
