import { Client } from '../Contacts/Client';
import { Historique } from '../Commun/Historique';
import { AssociatedDoc } from '../Commun/Commun';
import { OrderDetails } from '../Commun/OrderDetails';
import { Workshop } from './Workshop';

export class Credit {
	dueDate: Date;
	creationDate: Date;
	client: Client;
	orderDetails: OrderDetails;
	memos: [];
	id: string;
	reference: string;
	status: string;
	note: string;
	purpose: string;
	paymentCondition: string;
	workshop: Workshop;
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[];
}
