import { OrderDetails } from '../Commun/OrderDetails';
import { Memo } from '../Commun/Memo';
import { Historique } from '../Commun/Historique';
import { AssociatedDoc } from '../Commun/Commun';
import { Workshop } from './Workshop';

export class SupplierOrder {
	dueDate: Date;
	creationDate: Date;
	supplier: any;
	orderDetails: OrderDetails;
	memos: Memo[];
	expenses: [];
	id: string;
	reference: string;
	status: string;
	note: string;
	purpose: string;
	paymentCondition: string;
	workshop: Workshop;
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[]
}
