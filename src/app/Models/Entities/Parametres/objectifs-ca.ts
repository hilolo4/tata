
export class ObjectifsCa {
	id: number;
	years: number;
	chiffres: string;
	ca: number;
}

export class ObjectifsCaModel {
	id: number;
	goal: number;
	monthlyGoals: ChiffreCAMonthModel[];
}

export class ChiffreCAMonthModel {
	month: number;
	goal: number;
}


