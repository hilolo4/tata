export class SendMailParams {

	Subject: string;
	content?: string;
	body: string;
	path: string;
	messageTo?: string[];
	To: string[];
	Bcc: string[];
	Cc: string[];
	attachments: Attachments[];
}

export class Attachments {
	FileId: string;
	content: string;
	FileName: string;
	FileType: string;
}
