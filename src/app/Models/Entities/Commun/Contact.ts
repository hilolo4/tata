export class Contact {
	civility: string;
	email: string ;
	lastName: string;
	phoneNumber: string;
	firstName: string ;
	comment: string;
	function: string;
	landline: string;
}

