import { PieceJoin } from './Commun';

export class Memo {
	id: string;
	createdOn: Date;
	comment: string;
	user: {
		id: string;
		userName: string
	};
	attachments: PieceJoin[] = [];
}


