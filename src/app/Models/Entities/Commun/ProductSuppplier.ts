import { Product } from '../Biobiotheque/Product';
import { Supplier } from '../Contacts/Supplier';

export class ProductSupplier {

	productId: string;

	supplierId: string;

	price: string;

	isDefault: boolean;

	product: Product;

	supplier: Supplier;

}
