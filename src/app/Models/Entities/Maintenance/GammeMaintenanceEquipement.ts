import { moisEnum } from 'app/Enums/Maintenance/mois.enum';

export class GammeMaintenanceEquipement {
	id: string;
	equipmentName: string;
	maintenanceOperations: OperationMaintenance[];
}

export class OperationMaintenance 	{
	id?: string;
	name: string;
	periodicity?: moisEnum[];
	subOperations: OperationMaintenance[];
}


