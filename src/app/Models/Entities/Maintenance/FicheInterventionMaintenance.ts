import { OrderDetails, LineItems, GlobalDiscount } from '../Commun/OrderDetails';
import { Adresse } from '../Commun/Adresse';
import { Client } from '../Contacts/Client';
import { ClientSignature, AssociatedDoc, Technicians } from '../Commun/Commun';
import { Historique } from '../Commun/Historique';
import { MaintenanceEquipement } from './ContratEntretien';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';

export class FicheInterventionMaintenance {
	id: string;
	reference: string;
	status: string;
	orderDetails: OrderDetails;
	addressIntervention: Adresse;
	startDate: any;
	endDate: any;
	report: string;
	purpose: string;
	visitsCount: number;
	totalBasketConsumption: number;
	type: TypeInterventionMaintenance;
	year: number;
	month: number;
	quoteRequest: string;
	clientId: string;
	client: Client;
	clientSignature: ClientSignature;
	technicianSignature: ClientSignature;
	memos: [];
	technicianId: string;
	maintenanceVisitId: string;
	technician: Technicians;
	changesHistory: Historique[];
	// associatedDocuments: AssociatedDoc[];
	maintenanceContractId: string;
	maintenanceContract: any; // contrat
	maintenanceVisit: any; // visite maintenance
	emails: [];
	equipmentDetails: MaintenanceEquipement[]; // gamme equipement;
	observations: observation[];
	UnDoneMotif: [];
}

export class observation {
	id: string;
	name: string;
	observation: string;
	status: string;
}


export class FicheInterventionMaintenancePut {
	reference: string;
	clientId: string;
	client: any;
	technicianId: string;
	status: string;
	startDate: any;
	endDate: any;
	report: string;
	purpose: string;
	addressIntervention: Adresse;
	orderDetails: OrderDetailsMaintenance
}

export class FicheInterventionMaiSAVPut extends FicheInterventionMaintenancePut {
	id: string;
	visitsCount: number;
	totalBasketConsumption: number;
}

export class OrderDetailsMaintenance {
	totalHT: number;
	totalTTC: number;
	globalDiscount: GlobalDiscount;
	lineItems: LineItems[]
}

