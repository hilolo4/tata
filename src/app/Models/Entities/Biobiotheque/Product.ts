import { Historique } from '../Commun/Historique';
import { Supplier } from '../Contacts/Supplier';
import { Memo } from '../Commun/Memo';

export class Product {
	id: string;
	reference: string;
	designation: string;
	totalHours: number;
	materialCost: number;
	hourlyCost: number;
	vat: number;
	unite: string;
	categoryId: string;
	category: any;
	memo: Memo[];
	labels: any[];
	lots: any[];
	suppliers: Supplier[];
	totalHT: number;
	taxValue: number;
	totalTTC: number;
	productSuppliers: [];
	name: string;
	description: string;
	changesHistory: Historique[];
}
