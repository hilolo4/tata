export enum StatutFicheIntervention {

	Brouillon = 'draft',
	Planifiee = 'planned',
	Realisee = 'realized',
	Annulee = 'canceled',
	Facturee = 'billed',
	Areplanifiee = 'un_done',
	Late = 'late',

}



