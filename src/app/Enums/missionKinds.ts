

export enum MissionKind {

	ALL = '[]',

	TechnicianTask = 'technician_task',

	Appointment = 'appointment',

	Call = 'call',

}

