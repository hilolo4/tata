export enum TypeValue {
	None = 0,
	Percentage = 1,
	Amount = 2,
}

export enum TypeArticle {
	produit = 1,
	lot = 2,
	ligne = 3,
};

export enum EtatIntervention {
	OK = '1',
	NOK = '2',
}

export enum ActionPdf {
	print = 'print',
	download = 'download',
	base64 = 'base64',
}